package com.odigeo.membership.member.nosql;

import java.util.Optional;

public interface MembershipNoSQLManager {

    Optional<Long> getBookingIdFromCache(long membershipId);

    void storeInCache(String key, String value, int secondsToExpire);
}
