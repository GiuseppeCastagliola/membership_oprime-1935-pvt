package com.odigeo.membership.propertiesconfig;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.exception.DataNotFoundException;

import javax.sql.DataSource;
import java.sql.SQLException;

import static java.lang.String.format;

@Singleton
public class PropertiesConfigurationManager {

    private static final String SEND_IDS_TO_KAFKA_KEY = "SEND_IDS_TO_KAFKA";
    private final PropertiesConfigurationStore propertiesConfigurationStore;

    @Inject
    public PropertiesConfigurationManager(final PropertiesConfigurationStore propertiesConfigurationStore) {
        this.propertiesConfigurationStore = propertiesConfigurationStore;
    }

    public boolean isSendingIdsToKafkaActive(final DataSource dataSource) throws SQLException, DataNotFoundException {
        return propertiesConfigurationStore.getConfigValueByKey(dataSource, SEND_IDS_TO_KAFKA_KEY);
    }

    public boolean updatePropertyValue(final DataSource dataSource, final String key, final boolean value) throws DataAccessException {
        try {
            return propertiesConfigurationStore.updateConfigValueByKey(dataSource, key, value);
        } catch (SQLException e) {
            throw new DataAccessException(format("Cannot update the property %s with the value %s.", key, value), e);
        }
    }
}
