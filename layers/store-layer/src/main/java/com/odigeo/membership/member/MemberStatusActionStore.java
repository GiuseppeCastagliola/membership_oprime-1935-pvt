package com.odigeo.membership.member;

import com.google.inject.Singleton;
import com.odigeo.db.DbUtils;
import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.StatusAction;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Singleton
public class MemberStatusActionStore {

    private static final String SELECT_NEXT_ACTION_ID = "SELECT SEQ_GE_MEMBER_STATUS_ACTION_ID.nextval FROM dual ";
    private static final String ADD_MEMBER_STATUS_ACTION_SQL = "INSERT INTO GE_MEMBER_STATUS_ACTION(ID, MEMBER_ID, ACTION_TYPE) VALUES (?, ?, ?) ";
    private static final String SELECT_LAST_STATUS_ACTION_BY_MEMBER_ID = "SELECT msa.ID, msa.MEMBER_ID, msa.ACTION_TYPE, msa.ACTION_DATE FROM GE_MEMBER_STATUS_ACTION msa WHERE msa.MEMBER_ID = ? ORDER BY msa.ACTION_DATE DESC ";
    private static final String COLUMN_ID = "ID";
    private static final String COLUMN_MEMBER_ID = "MEMBER_ID";
    private static final String COLUMN_ACTION_TYPE = "ACTION_TYPE";
    private static final String COLUMN_ACTION_DATE = "ACTION_DATE";

    public void createMemberStatusAction(DataSource dataSource, long memberId, StatusAction statusAction) throws SQLException {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ADD_MEMBER_STATUS_ACTION_SQL)) {

            final long actionId = DbUtils.nextSequence(connection.prepareStatement(SELECT_NEXT_ACTION_ID));
            prepareAndExecuteCreationStatement(memberId, statusAction, preparedStatement, actionId);
        }
    }

    void prepareAndExecuteCreationStatement(final long memberId, final StatusAction statusAction,
                                            final PreparedStatement preparedStatement, final long actionId)
            throws SQLException {
        preparedStatement.setString(1, String.valueOf(actionId));
        preparedStatement.setString(2, String.valueOf(memberId));
        preparedStatement.setString(3, statusAction.toString());
        preparedStatement.execute();
    }

    public Optional<MemberStatusAction> selectLastStatusActionByMembershipId(final DataSource dataSource, final Long membershipId) throws SQLException {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_LAST_STATUS_ACTION_BY_MEMBER_ID)) {
            preparedStatement.setString(1, String.valueOf(membershipId));
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildMemberStatusAction(rs));
                }
                return Optional.empty();
            }
        }
    }

    private MemberStatusAction buildMemberStatusAction(final ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(COLUMN_ID);
        long memberId = resultSet.getLong(COLUMN_MEMBER_ID);
        Date date = resultSet.getDate(COLUMN_ACTION_DATE);
        String status = resultSet.getString(COLUMN_ACTION_TYPE);
        StatusAction statusAction = StatusAction.contains(status) ? StatusAction.valueOf(status) : StatusAction.UNKNOWN;
        return new MemberStatusAction(id, memberId, statusAction, date);
    }
}
