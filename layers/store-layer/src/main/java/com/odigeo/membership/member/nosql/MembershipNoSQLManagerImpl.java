package com.odigeo.membership.member.nosql;

import com.edreams.base.DataAccessException;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.log4j.Logger;

import java.util.Optional;

import static java.util.Objects.nonNull;

@Singleton
public class MembershipNoSQLManagerImpl implements MembershipNoSQLManager {

    private static final Logger LOGGER = Logger.getLogger(MembershipNoSQLManagerImpl.class);

    private final MembershipNoSQLRepository membershipNoSQLRepository;

    @Inject
    MembershipNoSQLManagerImpl(final MembershipNoSQLRepository membershipNoSQLRepository) {
        this.membershipNoSQLRepository = membershipNoSQLRepository;
    }

    @Override
    public Optional<Long> getBookingIdFromCache(long membershipId) {
        Optional<Long> bookingId = Optional.empty();
        try {
            String value = membershipNoSQLRepository.get(String.valueOf(membershipId));
            if (nonNull(value)) {
                bookingId =  Optional.of(Long.valueOf(value));
            }
        } catch (DataAccessException e) {
            LOGGER.error("Error retrieving BookingId from the Redis cache with membershipId =" + membershipId);
        }
        return bookingId;
    }

    @Override
    public void storeInCache(String key, String value, int secondsToExpire) {
        try {
            membershipNoSQLRepository.store(key, value, secondsToExpire);
        } catch (DataAccessException e) {
            LOGGER.error("Error storing BookingId from the Redis cache with key =" + key);
        }
    }
}
