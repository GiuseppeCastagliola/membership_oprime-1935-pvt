package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.MemberAccount;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Singleton
public class MemberAccountEntityBuilder {

    private static final String ACCOUNT_ID = "ACCOUNT_ID";
    private static final String USER_ID = "USER_ID";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String TIMESTAMP = "TIMESTAMP";

    public MemberAccount build(ResultSet rs, String idColumnLabel) throws SQLException {
        long id = rs.getLong(idColumnLabel);
        long userId = rs.getLong(USER_ID);
        String name = rs.getString(FIRST_NAME);
        String lastNames = rs.getString(LAST_NAME);
        Timestamp timestamp = rs.getTimestamp(TIMESTAMP);
        return new MemberAccount(id, userId, name, lastNames).setTimestamp(timestamp.toLocalDateTime());
    }

    public List<MemberAccount> buildWithMembership(ResultSet rs) throws SQLException {
        Map<Long, MemberAccount> memberAccountMap = new LinkedHashMap<>();
        while (rs.next()) {
            MemberAccount memberAccount = build(rs, ACCOUNT_ID);
            memberAccountMap.putIfAbsent(memberAccount.getId(), memberAccount);
            memberAccountMap.get(memberAccount.getId()).getMemberships()
                    .add(getMembershipEntityBuilder().build(rs, false));
        }
        return new ArrayList<>(memberAccountMap.values());
    }

    private MembershipEntityBuilder getMembershipEntityBuilder() {
        return ConfigurationEngine.getInstance(MembershipEntityBuilder.class);
    }
}
