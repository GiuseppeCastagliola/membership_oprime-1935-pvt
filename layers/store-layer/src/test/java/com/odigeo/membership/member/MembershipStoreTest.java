package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.v12.InvalidParametersException;
import com.odigeo.membership.AutoRenewalOperation;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MemberStatusAction;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.search.MembershipSearch;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.mockito.AdditionalMatchers.and;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class MembershipStoreTest {

    private static final int ZERO = 0;
    private static final int ONCE = 1;
    private static final int TWO = 2;
    private static final long TEST_MEMBERSHIP_ID_ONE = 123L;
    private static final long TEST_MEMBERSHIP_ID_TWO = 456L;
    private static final Long TEST_USER_ID = 707L;
    private static final long TEST_MEMBER_ACCOUNT_ID_ONE = 111L;
    private static final long TEST_MEMBER_ACCOUNT_ID_TWO = 222L;
    private static final LocalDate TEST_ACTIVATION_DATE = LocalDate.now().plusYears(1L);
    private static final LocalDate TEST_RENEWAL_DATE = TEST_ACTIVATION_DATE.plusYears(1L);
    private static final int OK = 1;
    private static final int KO = 0;
    private static final String WEBSITE = "ES";
    private static final BigDecimal TEST_PRICE = BigDecimal.TEN;
    private static final SourceType SOURCE_TYPE = SourceType.FUNNEL_BOOKING;
    private static final BigDecimal TOTAL_PRICE = BigDecimal.valueOf(34.02);
    private static final String CURRENCY_CODE = "EUR";
    private static final MembershipType MEMBERSHIP_TYPE_BASIC = MembershipType.BASIC;
    private static final MemberStatusAction MEMBER_STATUS_ACTION = new MemberStatusAction(555L, TEST_MEMBERSHIP_ID_TWO,
            StatusAction.CREATION, new java.util.Date());
    private static final LocalDateTime NOW_DATETIME = LocalDateTime.now();
    private static final String TEST_NAME = "Mario";
    private static final String TEST_LAST_NAME = "Gomez";
    private static final InvalidParametersException MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION = new InvalidParametersException(
            "Don't confuse membershipId with memberAccountId");
    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(TEST_MEMBER_ACCOUNT_ID_ONE, TEST_USER_ID, "test", "test");
    private Membership membership = new MembershipBuilder().setId(TEST_MEMBERSHIP_ID_ONE).setWebsite(WEBSITE)
            .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED)
            .setActivationDate(NOW_DATETIME).setExpirationDate(NOW_DATETIME.plusYears(1L))
            .setMemberAccountId(TEST_MEMBER_ACCOUNT_ID_ONE).setMembershipType(MEMBERSHIP_TYPE_BASIC)
            .setTotalPrice(TOTAL_PRICE).setCurrencyCode(CURRENCY_CODE).setProductStatus(ProductStatus.CONTRACT)
            .setTimestamp(NOW_DATETIME).setSourceType(SOURCE_TYPE).build();
    private Membership membershipWithAccount = new MembershipBuilder().setId(TEST_MEMBERSHIP_ID_ONE).setWebsite(WEBSITE)
            .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED)
            .setActivationDate(NOW_DATETIME).setExpirationDate(NOW_DATETIME.plusYears(1L))
            .setMemberAccountId(TEST_MEMBER_ACCOUNT_ID_ONE).setMembershipType(MEMBERSHIP_TYPE_BASIC)
            .setTotalPrice(TOTAL_PRICE).setCurrencyCode(CURRENCY_CODE).setProductStatus(ProductStatus.CONTRACT)
            .setTimestamp(NOW_DATETIME).setSourceType(SOURCE_TYPE).setMemberAccount(MEMBER_ACCOUNT).build();

    @Spy
    private MembershipStore store;
    @Mock
    private ResultSet resultSet;
    @Mock
    private PreparedStatement preparedStatement;
    @Mock
    private Connection connection;
    @Mock
    private DataSource dataSource;

    @BeforeMethod
    public void before() throws Exception {
        ConfigurationEngine.init();
        store = ConfigurationEngine.getInstance(MembershipStore.class);
        MockitoAnnotations.initMocks(this);
        mockDatabase();
        screenCorrectUseOfArgs();
    }

    @Test
    public void testFetchMembershipByMembershipId() throws Exception {
        testFetchSimpleClientFromDatabase();
        Membership actualMember = store.fetchMembershipById(dataSource, TEST_MEMBERSHIP_ID_ONE);
        assertEqualsMembership(actualMember, membership);
    }

    @Test
    public void testFetchMembershipByMemberAccountId() throws Exception {
        testFetchSimpleClientFromDatabase();
        List<Membership> actualMember = store.fetchMembershipByMemberAccountId(dataSource, TEST_MEMBER_ACCOUNT_ID_ONE);
        assertEqualsMembership(actualMember.get(0), membership);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testFetchMembershipByMemberAccountIdNoGettingResults() throws Exception {
        resultSetWithNoResults();
        store.fetchMembershipByMemberAccountId(dataSource, TEST_MEMBER_ACCOUNT_ID_ONE);
        fail();
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testFetchMemberNotGettingResults() throws Exception {
        resultSetWithNoResults();
        store.fetchMembershipById(dataSource, TEST_MEMBERSHIP_ID_ONE);
    }

    @Test
    public void testReactivateMember() throws Exception {
        setReactivateMember();
        assertTrue(store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.ACTIVATED));
    }

    @Test
    public void testReactivateMemberNoEffects() throws Exception {
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(KO);
        assertFalse(store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.ACTIVATED));
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testReactivateMemberSQLException() throws Exception {
        setActivateOrReactivateMemberException();
        assertTrue(store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.ACTIVATED));
    }

    @Test
    public void testDisableMembership() throws Exception {
        store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.DEACTIVATED);
        verify(preparedStatement).setString(anyInt(), eq(String.valueOf(TEST_MEMBERSHIP_ID_ONE)));
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testDisableMembershipNoEffects() throws Exception {
        when(this.preparedStatement.executeUpdate()).thenReturn(KO);
        assertFalse(store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.DEACTIVATED));
        verify(preparedStatement).setString(anyInt(), eq(String.valueOf(TEST_MEMBERSHIP_ID_ONE)));
        verify(preparedStatement).executeUpdate();
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testDisableMembershipThrowsEx() throws Exception {
        when(this.preparedStatement.executeUpdate()).thenThrow(new SQLException());
        store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.DEACTIVATED);
    }

    @Test
    public void testExpireMembership() throws Exception {
        store.updateStatus(dataSource, TEST_MEMBERSHIP_ID_ONE, MemberStatus.EXPIRED);
        verify(preparedStatement).setString(2, String.valueOf(TEST_MEMBERSHIP_ID_ONE));
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testDisableAutoRenewal() throws Exception {
        store.updateAutoRenewal(dataSource, TEST_MEMBERSHIP_ID_ONE, AutoRenewalOperation.DISABLE_AUTO_RENEW);
        verify(preparedStatement).setString(anyInt(), eq(String.valueOf(TEST_MEMBERSHIP_ID_ONE)));
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testDisableAutoRenewalNoEffects() throws Exception {
        when(this.preparedStatement.executeUpdate()).thenReturn(KO);
        store.updateAutoRenewal(dataSource, TEST_MEMBERSHIP_ID_ONE, AutoRenewalOperation.DISABLE_AUTO_RENEW);
        verify(preparedStatement).setString(anyInt(), eq(String.valueOf(TEST_MEMBERSHIP_ID_ONE)));
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testEnableAutoRenewal() throws Exception {
        store.updateAutoRenewal(dataSource, TEST_MEMBERSHIP_ID_TWO, AutoRenewalOperation.ENABLE_AUTO_RENEW);
        verify(preparedStatement).setString(2, String.valueOf(TEST_MEMBERSHIP_ID_TWO));
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testUpdateBalance() throws Exception {
        when(preparedStatement.executeUpdate()).thenReturn(OK);

        assertTrue(store.updateMembershipBalance(dataSource, TEST_MEMBERSHIP_ID_TWO, BigDecimal.TEN),
                String.format("balance of MembershipId %s not updated", TEST_MEMBERSHIP_ID_TWO));
        verify(preparedStatement).setBigDecimal(1, BigDecimal.TEN);
        verify(preparedStatement).setLong(2, TEST_MEMBERSHIP_ID_TWO);
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testUpdateBalanceMembershipNotFound() throws Exception {
        when(preparedStatement.executeUpdate()).thenReturn(KO);

        assertFalse(store.updateMembershipBalance(dataSource, TEST_MEMBERSHIP_ID_TWO, BigDecimal.TEN),
                String.format("balance of MembershipId %s was updated", TEST_MEMBERSHIP_ID_TWO));
        verify(preparedStatement).setBigDecimal(1, BigDecimal.TEN);
        verify(preparedStatement).setLong(2, TEST_MEMBERSHIP_ID_TWO);
        verify(preparedStatement).executeUpdate();
    }

    @Test
    public void testFetchMembersById() throws Exception {
        testFetchSimpleClientFromDatabase();
        Membership actualMember = store.fetchMembershipById(dataSource, TEST_MEMBERSHIP_ID_ONE);
        assertEqualsMembership(actualMember, membership);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testFetchMembersByIdNotGettingResults() throws Exception {
        resultSetWithNoResults();
        store.fetchMembershipById(dataSource, TEST_MEMBERSHIP_ID_ONE);
    }

    @Test
    public void testCreateMember() throws Exception {
        testFetchSimpleClientFromDatabase();
        assertEquals(store.createMember(dataSource, getMembershipCreation()), TEST_MEMBERSHIP_ID_ONE);
        verify(preparedStatement).setString(4, MemberStatus.PENDING_TO_ACTIVATE.toString());
        verify(preparedStatement).setString(5, MembershipRenewal.ENABLED.toString());
    }

    @Test
    public void testCreateMemberWithNullDate() throws Exception {
        testFetchSimpleClientFromDatabase();
        assertEquals(store.createMember(dataSource, getMembershipCreation()), TEST_MEMBERSHIP_ID_ONE);
        verify(preparedStatement).setNull(6, Types.DATE);
    }

    @Test
    public void testCreateMemberActivatedWithDate() throws Exception {
        testFetchSimpleClientFromDatabase();
        MembershipCreation membershipCreation = getMembershipCreation();
        membershipCreation.setActivationDate(LocalDate.now());
        membershipCreation.setExpirationDate(LocalDate.now().plusYears(1));
        membershipCreation.setMemberStatus(MemberStatus.ACTIVATED);
        assertEquals(store.createMember(dataSource, membershipCreation), TEST_MEMBERSHIP_ID_ONE);
        verify(preparedStatement).setString(4, MemberStatus.ACTIVATED.toString());
        verify(preparedStatement).setDate(6, Date.valueOf(membershipCreation.getActivationDate()));
        verify(preparedStatement).setDate(7, Date.valueOf(membershipCreation.getExpirationDate()));
    }

    @Test
    void testActivateMembership() throws SQLException {
        mockConnectionAndUpdateStatement(ONCE);
        assertTrue(store.activateMember(dataSource, TEST_MEMBERSHIP_ID_ONE, TEST_ACTIVATION_DATE, TEST_RENEWAL_DATE, TEST_PRICE));
        validateActivateMembership();
    }

    @Test
    void testNotActivateMembership() throws SQLException {
        mockConnectionAndUpdateStatement(ZERO);
        assertFalse(store.activateMember(dataSource, TEST_MEMBERSHIP_ID_ONE, TEST_ACTIVATION_DATE, TEST_RENEWAL_DATE, TEST_PRICE));
        validateActivateMembership();
    }

    @Test(expectedExceptions = SQLException.class)
    public void testActivateMemberSQLException() throws SQLException {
        setActivateOrReactivateMemberException();
        assertTrue(store.activateMember(dataSource, TEST_MEMBERSHIP_ID_ONE, TEST_ACTIVATION_DATE, TEST_RENEWAL_DATE, TEST_PRICE));
    }

    @Test
    public void testFetchMembersByIdBlocking() throws SQLException, DataNotFoundException {
        testFetchSimpleClientFromDatabase();
        Membership membership = store.fetchMembersByIdBlocking(dataSource, TEST_MEMBERSHIP_ID_ONE);
        assertEqualsMembership(membership, this.membership);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testFetchMembersByIdBlockingNotGettingResults() throws Exception {
        resultSetWithNoResults();
        store.fetchMembersByIdBlocking(dataSource, TEST_MEMBERSHIP_ID_ONE);
    }

    @Test
    public void testFetchMembershipByIdWithMemberAccount() throws SQLException, DataNotFoundException {
        setMocksForCreatingMemberFromDatabase();
        testFetchMembershipWithMemberAccountFromDatabase();
        Membership membership = store.fetchMembershipByIdWithMemberAccount(dataSource, TEST_MEMBERSHIP_ID_ONE);
        assertEqualsMembership(membership, this.membershipWithAccount);
    }

    @Test(expectedExceptions = DataNotFoundException.class)
    public void testFetchMembersByIdWithMemberAccountNotGettingResults() throws Exception {
        resultSetWithNoResults();
        store.fetchMembershipByIdWithMemberAccount(dataSource, TEST_MEMBERSHIP_ID_ONE);
    }

    @Test
    public void testSimpleSearchMembership() throws SQLException {
        testFetchSimpleClientFromDatabase();
        MembershipSearch membershipSearch = new MembershipSearch.Builder().website(WEBSITE).build();
        List<Membership> memberships = store.searchMemberships(dataSource, membershipSearch);
        Membership membership = memberships.get(0);
        assertTrue(membership.getMemberStatusActions().isEmpty());
        assertNull(membership.getMemberAccount());
    }

    @Test
    public void testSearchWithStatusActionMembership() throws SQLException {
        testFetchSimpleClientFromDatabase();
        MembershipSearch membershipSearch = new MembershipSearch.Builder().website(WEBSITE).withStatusActions(true).build();
        List<Membership> memberships = store.searchMemberships(dataSource, membershipSearch);
        Membership membership = memberships.get(0);
        assertFalse(membership.getMemberStatusActions().isEmpty());
        assertNull(membership.getMemberAccount());
    }

    @Test
    public void testSearchWithStatusActionAndAccountMembership() throws SQLException {
        testFetchMembershipWithMemberAccountFromDatabase();
        MembershipSearch membershipSearch = new MembershipSearch.Builder().website(WEBSITE).withStatusActions(true).withMemberAccount(true).build();
        List<Membership> memberships = store.searchMemberships(dataSource, membershipSearch);
        Membership membership = memberships.get(0);
        assertFalse(membership.getMemberStatusActions().isEmpty());
        assertNotNull(membership.getMemberAccount());
    }

    @Test
    public void testSearchWithAccountMembership() throws SQLException {
        testFetchMembershipWithMemberAccountFromDatabase();
        MembershipSearch membershipSearch = new MembershipSearch.Builder().website(WEBSITE).withMemberAccount(true).build();
        List<Membership> memberships = store.searchMemberships(dataSource, membershipSearch);
        Membership membership = memberships.get(0);
        assertTrue(membership.getMemberStatusActions().isEmpty());
        assertNotNull(membership.getMemberAccount());
    }

    @Test
    public void testSearchWithNoResults() throws SQLException {
        resultSetWithNoResults();
        MembershipSearch membershipSearch = new MembershipSearch.Builder().website(WEBSITE).withMemberAccount(true).build();
        List<Membership> memberships = store.searchMemberships(dataSource, membershipSearch);
        assertTrue(memberships.isEmpty());
    }

    @Test
    public void testActivatePendingToCollect() throws SQLException {
        mockConnectionAndUpdateStatement(ONCE);
        final Membership membershipForPtcActivation = new MembershipBuilder()
                .setTotalPrice(BigDecimal.TEN)
                .setId(TEST_MEMBERSHIP_ID_ONE)
                .build();
        assertTrue(store.activatePendingToCollect(dataSource, membershipForPtcActivation));
        verify(preparedStatement).setBigDecimal(1, membershipForPtcActivation.getTotalPrice());
        verify(preparedStatement).setDate(anyInt(), any(Date.class));
        verify(preparedStatement).setString(3, String.valueOf(membershipForPtcActivation.getId()));
    }

    private void testFetchSimpleClientFromDatabase() throws SQLException {
        resultSetWithOneResult();
        setMocksForCreatingMemberFromDatabase();
    }

    private void testFetchMembershipWithMemberAccountFromDatabase() throws SQLException {
        resultSetWithOneResult();
        setMocksForCreatingMembershipWithAccountFromDatabase();
    }

    private void setMocksForCreatingMemberFromDatabase() throws SQLException {
        when(resultSet.getString("ID")).thenReturn(Long.toString(TEST_MEMBERSHIP_ID_ONE));
        when(resultSet.getLong("ID")).thenReturn(TEST_MEMBERSHIP_ID_ONE);
        when(resultSet.getString("WEBSITE")).thenReturn(WEBSITE);
        when(resultSet.getString("STATUS")).thenReturn(MemberStatus.ACTIVATED.toString());
        when(resultSet.getString("AUTO_RENEWAL")).thenReturn(MembershipRenewal.ENABLED.toString());
        when(resultSet.getTimestamp("EXPIRATION_DATE")).thenReturn(Timestamp.valueOf(NOW_DATETIME.plusYears(1L)));
        when(resultSet.getTimestamp("ACTIVATION_DATE")).thenReturn(Timestamp.valueOf(NOW_DATETIME));
        when(resultSet.getString("MEMBERSHIP_TYPE")).thenReturn(MEMBERSHIP_TYPE_BASIC.toString());
        when(resultSet.getString("SOURCE_TYPE")).thenReturn(SOURCE_TYPE.name());
        when(resultSet.getString("CURRENCY_CODE")).thenReturn(CURRENCY_CODE);
        when(resultSet.getBigDecimal("TOTAL_PRICE")).thenReturn(TOTAL_PRICE);
        when(resultSet.getTimestamp("MEMBERSHIP_TIMESTAMP")).thenReturn(Timestamp.valueOf(NOW_DATETIME));
        when(resultSet.getLong("MEMBER_ACCOUNT_ID")).thenReturn(TEST_MEMBER_ACCOUNT_ID_ONE);
        when(resultSet.getString("PRODUCT_STATUS")).thenReturn(ProductStatus.CONTRACT.name());
        when(resultSet.getLong("MEMBER_STATUS_ACTION_ID")).thenReturn(MEMBER_STATUS_ACTION.getId());
        when(resultSet.getString("ACTION_TYPE")).thenReturn(MEMBER_STATUS_ACTION.getAction().name());
        when(resultSet.getDate("ACTION_DATE")).thenReturn(new java.sql.Date(MEMBER_STATUS_ACTION.getTimestamp().getTime()));
    }

    private void setMocksForCreatingMembershipWithAccountFromDatabase() throws SQLException {
        setMocksForCreatingMemberFromDatabase();
        when(resultSet.getLong("USER_ID")).thenReturn(TEST_USER_ID);
        when(resultSet.getString("FIRST_NAME")).thenReturn(TEST_NAME);
        when(resultSet.getString("LAST_NAME")).thenReturn(TEST_LAST_NAME);
        when(resultSet.getTimestamp("TIMESTAMP")).thenReturn(Timestamp.valueOf(NOW_DATETIME));
    }

    private void resultSetWithOneResult() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
    }

    private void resultSetWithNoResults() throws SQLException {
        when(resultSet.next()).thenReturn(false);
    }

    private void mockDatabase() throws SQLException {
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.getLong(1)).thenReturn(TEST_MEMBERSHIP_ID_ONE);
    }

    private void screenCorrectUseOfArgs() throws DataAccessException, SQLException {
        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store)
                .activateMember(eq(dataSource), and(not(eq(TEST_MEMBERSHIP_ID_ONE)), not(eq(TEST_MEMBERSHIP_ID_TWO))),
                        any(), any(), any());
        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store)
                .updateStatus(eq(dataSource), and(not(eq(TEST_MEMBERSHIP_ID_ONE)), not(eq(TEST_MEMBERSHIP_ID_TWO))),
                        any());
        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store).updateAutoRenewal(eq(dataSource),
                and(not(eq(TEST_MEMBERSHIP_ID_ONE)), not(eq(TEST_MEMBERSHIP_ID_TWO))), any());
        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store).fetchMembershipById(eq(dataSource),
                and(not(eq(TEST_MEMBERSHIP_ID_ONE)), not(eq(TEST_MEMBERSHIP_ID_TWO))));
        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store).fetchMembersByIdBlocking(eq(dataSource),
                and(not(eq(TEST_MEMBERSHIP_ID_ONE)), not(eq(TEST_MEMBERSHIP_ID_TWO))));
        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store).updateMembershipBalance(eq(dataSource),
                and(not(eq(TEST_MEMBERSHIP_ID_ONE)), not(eq(TEST_MEMBERSHIP_ID_TWO))), any());

        doThrow(MEMBERSHIP_VS_MEMBER_ACC_ID_EXCEPTION).when(store).fetchMembershipByMemberAccountId(eq(dataSource),
                and(not(eq(TEST_MEMBER_ACCOUNT_ID_ONE)), not(eq(TEST_MEMBER_ACCOUNT_ID_TWO))));
    }

    private void mockConnectionAndUpdateStatement(final int isUpdated) throws SQLException {
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(isUpdated);
    }

    private void validateActivateMembership() throws SQLException {
        verify(preparedStatement, times(TWO)).setDate(anyInt(), any(Date.class));
        verify(preparedStatement).setString(anyInt(), anyString());
    }

    private void setActivateOrReactivateMemberException() throws SQLException {
        when(preparedStatement.executeUpdate()).thenThrow(new SQLException());
    }

    private void setReactivateMember() throws SQLException {
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(OK);
    }

    private MembershipCreation getMembershipCreation() {
        return new MembershipCreationBuilder()
                .withUserId(TEST_MEMBERSHIP_ID_ONE)
                .withName("test")
                .withLastNames("test")
                .withWebsite(WEBSITE)
                .withMembershipType(MEMBERSHIP_TYPE_BASIC)
                .withSourceType(SOURCE_TYPE)
                .withSubscriptionPrice(BigDecimal.TEN)
                .withMemberAccountId(TEST_MEMBER_ACCOUNT_ID_ONE)
                .withExpirationDate(LocalDate.now().plusYears(1))
                .build();
    }

    private void assertEqualsMembership(final Membership actualMembership, final Membership expectedMembership) {
        assertEqualsMembershipWithExclusions(actualMembership, expectedMembership);
        assertEquals(actualMembership.getExpirationDate(), expectedMembership.getExpirationDate());
    }

    private void assertEqualsMembershipWithExclusions(final Membership actualMembership, final Membership expectedMembership) {
        assertEquals(actualMembership, expectedMembership);
        assertEquals(actualMembership.getId(), expectedMembership.getId());
        assertEquals(actualMembership.getWebsite(), expectedMembership.getWebsite());
        assertEquals(actualMembership.getStatus(), expectedMembership.getStatus());
        assertEquals(actualMembership.getAutoRenewal(), expectedMembership.getAutoRenewal());
        assertEquals(actualMembership.getActivationDate(), expectedMembership.getActivationDate());
        assertEquals(actualMembership.getTimestamp(), expectedMembership.getTimestamp());
        assertEquals(actualMembership.getBookingLimitReached(), expectedMembership.getBookingLimitReached());
        assertEquals(actualMembership.getMemberAccountId(), expectedMembership.getMemberAccountId());
        assertEquals(actualMembership.getBalance(), expectedMembership.getBalance());
        assertEquals(actualMembership.getMonthsDuration(), expectedMembership.getMonthsDuration());
        assertEquals(actualMembership.getMembershipType(), expectedMembership.getMembershipType());
        assertEquals(actualMembership.getSourceType(), expectedMembership.getSourceType());
        assertEquals(actualMembership.getProductStatus(), expectedMembership.getProductStatus());
        assertEquals(actualMembership.getTotalPrice(), expectedMembership.getTotalPrice());
        assertEquals(actualMembership.getCurrencyCode(), expectedMembership.getCurrencyCode());
        assertEquals(actualMembership.getRecurringId(), expectedMembership.getRecurringId());
    }
}
