package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.Membership;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

public class MemberAccountEntityBuilderTest {

    private static final long ID_1 = 111L;
    private static final long ID_2 = 222L;
    private static final long USER_ID_1 = 123L;
    private static final long USER_ID_2 = 456L;
    private static final String NAME = "name_test";
    private static final String LAST_NAMES = "lastName_test";
    private static final Date utilDate = new Date();
    private static final Timestamp timestamp = new Timestamp(utilDate.getTime());
    private static final String ID_COLUMN_LABEL = "ACCOUNT_ID";

    @Mock
    private ResultSet resultSet;
    @Mock
    private Membership membership;
    @Mock
    private MembershipEntityBuilder membershipEntityBuilder;

    private MemberAccountEntityBuilder memberAccountEntityBuilder;

    @BeforeMethod
    public void before() {
        initMocks(this);
        ConfigurationEngine.init((binder) -> binder.bind(MembershipEntityBuilder.class).toInstance(membershipEntityBuilder));
        memberAccountEntityBuilder = new MemberAccountEntityBuilder();
    }

    @Test
    public void testBuild() throws SQLException {
        // Given
        mockResultSet();
        // When
        MemberAccount memberAccount = memberAccountEntityBuilder.build(resultSet, ID_COLUMN_LABEL);
        // Then
        assertEquals(memberAccount.getId(), ID_1);
        assertEquals(memberAccount.getUserId(), USER_ID_1);
        assertEquals(memberAccount.getName(), NAME);
        assertEquals(memberAccount.getLastNames(), LAST_NAMES);
        assertEquals(memberAccount.getTimestamp(), timestamp.toLocalDateTime());
    }

    @Test
    public void testBuildWithMembership() throws SQLException {
        // Given
        mockResultSet();
        when(membershipEntityBuilder.build(resultSet, false)).thenReturn(membership);
        // When
        List<MemberAccount> memberAccountList = memberAccountEntityBuilder.buildWithMembership(resultSet);
        // Then
        assertEquals(memberAccountList.size(), 2);
        assertNotEquals(memberAccountList.get(0).getUserId(), memberAccountList.get(1).getUserId());
        assertNotEquals(memberAccountList.get(0).getId(), memberAccountList.get(1).getId());
        assertEquals(memberAccountList.get(0).getName(), memberAccountList.get(1).getName());
        assertEquals(memberAccountList.get(0).getLastNames(), memberAccountList.get(1).getLastNames());
        assertEquals(memberAccountList.get(0).getTimestamp(), memberAccountList.get(1).getTimestamp());
    }

    private void mockResultSet() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getLong(ID_COLUMN_LABEL)).thenReturn(ID_1).thenReturn(ID_2);
        when(resultSet.getLong("USER_ID")).thenReturn(USER_ID_1).thenReturn(USER_ID_2);
        when(resultSet.getString("FIRST_NAME")).thenReturn(NAME);
        when(resultSet.getString("LAST_NAME")).thenReturn(LAST_NAMES);
        when(resultSet.getTimestamp("TIMESTAMP")).thenReturn(timestamp);
    }
}