package com.odigeo.membership;

import org.testng.annotations.Test;

import java.util.Date;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

public class MemberAccountBookingTest {

    private static final MemberAccount MEMBER_ACCOUNT = new MemberAccount(1L, 2L, "test name", "test last name");
    private static final Long BOOKING_ID = 843L;
    private static final Date DATE_NOW = new Date();

    @Test
    public void testConstructor() {
        MemberAccountBooking memberAccountBooking = new MemberAccountBooking(MEMBER_ACCOUNT, BOOKING_ID, DATE_NOW);
        assertEquals(memberAccountBooking.getMemberAccount(), memberAccountBooking.getMemberAccount());
        assertEquals(memberAccountBooking.getBookingDetailSubscriptionId(), BOOKING_ID);
        assertEquals(memberAccountBooking.getLastStatusModificationDate(), DATE_NOW);
    }

    @Test
    public void testConstructorWithNullDate() {
        MemberAccountBooking memberAccountBooking = new MemberAccountBooking(MEMBER_ACCOUNT, BOOKING_ID, null);
        assertNull(memberAccountBooking.getLastStatusModificationDate());
    }
}
