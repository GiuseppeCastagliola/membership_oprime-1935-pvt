package com.odigeo.membership.recurring;

import bean.test.BeanTest;

public class RecurringSelectionResponseTest extends BeanTest<RecurringSelectionResponse> {

    private static final long MEMBERSHIP_ID = 1L;
    private static final String RECURRING_ID = "ABC";

    @Override
    protected RecurringSelectionResponse getBean() {
        RecurringSelectionResponse selectionResponse = new RecurringSelectionResponse();
        selectionResponse.setMembershipId(MEMBERSHIP_ID);
        selectionResponse.setRecurringId(RECURRING_ID);
        return selectionResponse;
    }
}