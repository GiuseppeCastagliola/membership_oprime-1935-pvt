package com.odigeo.membership.activation.validation;

import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MemberHasMembershipInactiveValidationTest {

    private static final Long MEMBER_ACCOUNT_ID = 1L;

    @Test
    public void testValidation() {

        Membership member = new MembershipBuilder().setStatus(MemberStatus.PENDING_TO_ACTIVATE).setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.INIT).build();
        MemberHasMembershipInactiveValidation memberHasMembershipInactiveValidation = new MemberHasMembershipInactiveValidation(member);
        assertTrue(memberHasMembershipInactiveValidation.validate());

        Membership member2 = new MembershipBuilder().setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
        MemberHasMembershipInactiveValidation memberHasMembershipInactiveValidation2 = new MemberHasMembershipInactiveValidation(member2);
        assertFalse(memberHasMembershipInactiveValidation2.validate());

        Membership member4 = new MembershipBuilder().setStatus(MemberStatus.DEACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
        MemberHasMembershipInactiveValidation memberHasMembershipInactiveValidation4 = new MemberHasMembershipInactiveValidation(member4);
        assertFalse(memberHasMembershipInactiveValidation4.validate());
    }

}
