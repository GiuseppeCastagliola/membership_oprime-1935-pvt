package com.odigeo.membership;

import bean.test.BeanTest;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BookingTrackingTest extends BeanTest<BookingTracking> {

    private static final long MEMBERSHIP_ID = 1L;

    @Override
    protected BookingTracking getBean() {
        BookingTracking bookingTracking = new BookingTracking();
        bookingTracking.setPerksAmount(BigDecimal.ONE);
        bookingTracking.setCostFeeAmount(BigDecimal.ONE);
        bookingTracking.setAvoidFeeAmount(BigDecimal.ONE);
        bookingTracking.setMembershipId(MEMBERSHIP_ID);
        bookingTracking.setBookingDate(LocalDateTime.now());
        return bookingTracking;
    }
}