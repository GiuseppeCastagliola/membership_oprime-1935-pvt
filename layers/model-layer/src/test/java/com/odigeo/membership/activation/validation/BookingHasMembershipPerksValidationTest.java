package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.MembershipPerks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class BookingHasMembershipPerksValidationTest {

    @Mock
    private BookingDetail bookingDetail;

    @Mock
    private MembershipPerks membershipPerks;

    @BeforeMethod
    public void before() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testMembershipPerksAbsentValidation() {
        when(bookingDetail.getMembershipPerks()).thenReturn(null);
        BookingHasMembershipPerksValidation validation = new BookingHasMembershipPerksValidation(bookingDetail);
        assertFalse(validation.validate());
    }

    @Test
    public void testMembershipPerksPresentValidation() {
        when(bookingDetail.getMembershipPerks()).thenReturn(membershipPerks);
        BookingHasMembershipPerksValidation validation = new BookingHasMembershipPerksValidation(bookingDetail);
        assertTrue(validation.validate());
    }
}
