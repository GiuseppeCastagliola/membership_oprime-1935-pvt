package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import junit.framework.Assert;
import org.testng.annotations.Test;

/**
 * Created by roc.arajol on 29-Sep-17.
 */
public class BookingDoesNotHaveDidNotBuyStatusValidationTest {

    @Test
    public void testNullStatusValidation() {
        BookingDetail bookingDetail = new BookingDetail();
        BookingDoesNotHaveDidNotBuyStatusValidation validation = new BookingDoesNotHaveDidNotBuyStatusValidation(bookingDetail);

        Assert.assertTrue(validation.validate());
    }
    @Test
    public void testOtherStatusValidation() {
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setBookingStatus("FOO");
        BookingDoesNotHaveDidNotBuyStatusValidation validation = new BookingDoesNotHaveDidNotBuyStatusValidation(bookingDetail);

        Assert.assertTrue(validation.validate());
    }
    @Test
    public void testDidNotBuyStatusValidation() {
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setBookingStatus("DIDNOTBUY");
        BookingDoesNotHaveDidNotBuyStatusValidation validation = new BookingDoesNotHaveDidNotBuyStatusValidation(bookingDetail);

        Assert.assertFalse(validation.validate());
    }
}
