package com.odigeo.membership.cookies;

import bean.test.BeanTest;

public class CookiesContextTest extends BeanTest<CookiesContext> {

    private static final String OUTER_DOMAIN = "http//:wwww.membership.edreamsodigeo";

    @Override
    protected CookiesContext getBean() {
        return assembleCookiesContext();
    }

    private CookiesContext assembleCookiesContext() {
        CookiesContext cookiesContext = new CookiesContext(OUTER_DOMAIN);
        return cookiesContext;
    }
}