package com.odigeo.membership.mapper;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class GeneralMapperCreatorTest {

    private GeneralMapperCreator generalMapperCreator;

    @BeforeMethod
    public void setUp() throws Exception {
        this.generalMapperCreator = new GeneralMapperCreator();
    }

    @Test
    public void test() throws Exception {
        assertNotNull(this.generalMapperCreator.getMapperFactory());
        assertNotNull(this.generalMapperCreator.getMapper());
    }
}
