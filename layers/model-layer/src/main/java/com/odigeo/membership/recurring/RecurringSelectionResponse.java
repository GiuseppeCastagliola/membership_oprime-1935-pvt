package com.odigeo.membership.recurring;

public class RecurringSelectionResponse {
    private long membershipId;
    private String recurringId;

    public long getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(long membershipId) {
        this.membershipId = membershipId;
    }

    public String getRecurringId() {
        return recurringId;
    }

    public void setRecurringId(String recurringId) {
        this.recurringId = recurringId;
    }
}
