package com.odigeo.membership;

public enum MembershipRenewal {
    ENABLED, DISABLED
}
