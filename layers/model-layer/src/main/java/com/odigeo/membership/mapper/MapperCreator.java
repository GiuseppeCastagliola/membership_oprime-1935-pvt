package com.odigeo.membership.mapper;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;

public interface MapperCreator {
    MapperFactory getMapperFactory();

    MapperFacade getMapper();
}
