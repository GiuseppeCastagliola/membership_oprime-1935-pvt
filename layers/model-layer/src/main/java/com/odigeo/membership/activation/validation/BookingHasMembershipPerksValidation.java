package com.odigeo.membership.activation.validation;

import com.odigeo.bookingapi.v12.responses.BookingDetail;

public class BookingHasMembershipPerksValidation extends AbstractValidation<BookingDetail> {

    public BookingHasMembershipPerksValidation(BookingDetail o) {
        super(o);
    }

    @Override
    public boolean validate() {
        return o.getMembershipPerks() != null;
    }
}
