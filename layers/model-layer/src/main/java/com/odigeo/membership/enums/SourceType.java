package com.odigeo.membership.enums;

import org.apache.commons.lang.StringUtils;

public enum SourceType {
    POST_BOOKING,
    FUNNEL_BOOKING;

    public static SourceType getNullableValue(String name) {
        if (StringUtils.isEmpty(name)) {
            return null;
        }
        return valueOf(name);
    }
}
