CREATE TABLE JIRA_UNL6160 (
ID          VARCHAR2(25 CHAR) NOT NULL,
MEMBER_ID   VARCHAR2(25 CHAR) NOT NULL,
ACTION_DATE DATE,
PRIMARY KEY (ID)
);
