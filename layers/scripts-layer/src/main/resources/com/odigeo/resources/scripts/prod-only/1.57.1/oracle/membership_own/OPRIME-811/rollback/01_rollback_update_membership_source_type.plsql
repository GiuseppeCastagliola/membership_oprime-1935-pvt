SET DEFINE OFF;
SET SERVEROUTPUT ON;

DECLARE
    affected_rows NUMBER := 0;
    CURSOR regs IS
        select * from membership_own.ge_membership where id in (86946900, 58819745, 58819693, 51540582, 51540563, 51540575,
            51540580, 51540573, 51540578, 51540584, 51540557, 51540498, 51540543, 51540547, 51540456, 51540541, 51540551,
            51540511, 51540480, 51540516, 51540478, 51540493, 51540417, 34869518);
BEGIN
    dbms_output.put_line('--Start of the PLSQL block');
    FOR reg IN regs LOOP
        affected_rows := affected_rows + 1;
        dbms_output.put_line('reg.id ' || reg.id);
        UPDATE membership_own.ge_membership SET source_type = 'FUNNEL_BOOKING' WHERE id = reg.id;
        DELETE FROM membership_own.ge_member_status_action WHERE member_id = reg.id AND action_type = 'PB_PHONE_CREATION';
    END LOOP;
    COMMIT;
    dbms_output.put_line('--End of the PLSQL block. Processed records: ' || affected_rows);
EXCEPTION
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END;