MERGE INTO GE_MEMBERSHIP e
USING JIRA_UNL5847 t
ON (t.ID = e.ID)
WHEN MATCHED THEN UPDATE
SET E.expiration_date = T.expiration_date;
