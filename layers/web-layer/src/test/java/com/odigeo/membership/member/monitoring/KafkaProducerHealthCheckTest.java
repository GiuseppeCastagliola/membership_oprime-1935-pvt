package com.odigeo.membership.member.monitoring;

import com.codahale.metrics.health.HealthCheck;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.membership.robots.activator.MembershipActivationProducerConfiguration;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;

public class KafkaProducerHealthCheckTest {

    private static final String TEST_RESOURCE = "test";

    private HealthCheck healthCheck;

    @Mock
    private KafkaProducerHealthCheck kafkaProducerHealthCheckMock;

    @Mock
    private MembershipActivationProducerConfiguration membershipActivationProducerConfigurationMock;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        setUpConfigurationEngineAndMocks();
    }

    private void setUpConfigurationEngineAndMocks() {
        this.healthCheck = new KafkaProducerHealthCheck();
        setUpMembershipActivationProducerConfigurationMock();
        ConfigurationEngine.init(new AbstractModule() {
            @Override
            protected void configure() {
                bind(MembershipActivationProducerConfiguration.class).toInstance(membershipActivationProducerConfigurationMock);
            }
        });
        this.kafkaProducerHealthCheckMock = new KafkaProducerHealthCheck();
    }

    private void setUpMembershipActivationProducerConfigurationMock() {
        this.membershipActivationProducerConfigurationMock = mock(MembershipActivationProducerConfiguration.class);
        when(membershipActivationProducerConfigurationMock.getBrokerList()).thenReturn(TEST_RESOURCE);
    }

    @Test
    public void testCheckReturnsFalseWithInconsistentData() throws Exception {
        assertFalse(this.healthCheck.execute().isHealthy());
    }

    @Test
    public void testCheckReturnsFalseWhenMembershipActivationProducerConfigurationAvailableWithWrongResource() {
        assertFalse(this.kafkaProducerHealthCheckMock.execute().isHealthy());
    }
}
