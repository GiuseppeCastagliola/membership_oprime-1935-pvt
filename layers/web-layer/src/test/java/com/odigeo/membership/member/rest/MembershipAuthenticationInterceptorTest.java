package com.odigeo.membership.member.rest;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipUnauthorizedException;
import com.odigeo.membership.member.rest.utils.AuthenticationChecker;
import com.odigeo.membership.request.product.ActivationRequest;
import org.jboss.resteasy.core.ResourceMethod;
import org.jboss.resteasy.spi.HttpRequest;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.ws.rs.core.HttpHeaders;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipAuthenticationInterceptorTest {

    private static final String HASH_HEADER_NAME = "hash";
    private static final String CLIENT_HEADER_NAME = "client";
    private static final List<String> HASH_HEADERS = Collections.singletonList("asdfqwerty");
    private static final List<String> CLIENT_HEADERS = Collections.singletonList("one-front");
    @Mock
    private HttpRequest httpRequest;
    @Mock
    private HttpHeaders httpHeaders;
    @Mock
    private ResourceMethod resourceMethod;
    @Mock
    private AuthenticationChecker authenticationChecker;

    private MembershipAuthenticationInterceptor membershipAuthenticationInterceptor;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        when(httpRequest.getHttpHeaders()).thenReturn(httpHeaders);
        when(httpHeaders.getRequestHeader(HASH_HEADER_NAME)).thenReturn(HASH_HEADERS);
        when(httpHeaders.getRequestHeader(CLIENT_HEADER_NAME)).thenReturn(CLIENT_HEADERS);
        membershipAuthenticationInterceptor = new MembershipAuthenticationInterceptor();
    }

    @Test
    public void testExecuteInterceptorAuthorized() {
        when(authenticationChecker.check(anyString(), anyString(), anyString())).thenReturn(Boolean.TRUE);
        assertNull(membershipAuthenticationInterceptor.preProcess(httpRequest, resourceMethod));
    }

    @Test(expectedExceptions = MembershipUnauthorizedException.class)
    public void testExecuteInterceptorNotAuthorized() {
        when(authenticationChecker.check(anyString(), anyString(), anyString())).thenReturn(Boolean.FALSE);
        membershipAuthenticationInterceptor.preProcess(httpRequest, resourceMethod);
    }

    @Test(expectedExceptions = MembershipUnauthorizedException.class)
    public void testExecuteInterceptorMissingHeaders() {
        when(httpHeaders.getRequestHeader(anyString())).thenReturn(Collections.emptyList());
        membershipAuthenticationInterceptor.preProcess(httpRequest, resourceMethod);
    }

    @Test(expectedExceptions = MembershipUnauthorizedException.class)
    public void testExecuteInterceptorNullHeaders() {
        when(httpHeaders.getRequestHeader(anyString())).thenReturn(null);
        membershipAuthenticationInterceptor.preProcess(httpRequest, resourceMethod);
    }

    @Test
    public void testAccept() throws NoSuchMethodException {
        Class<MembershipService> membershipServiceClass = MembershipService.class;
        Method activateMembership = membershipServiceClass.getMethod("activateMembership", Long.TYPE, ActivationRequest.class);
        assertTrue(membershipAuthenticationInterceptor.accept(membershipServiceClass, activateMembership));
        Method isMembershipPerksActiveOn = membershipServiceClass.getMethod("isMembershipPerksActiveOn", String.class);
        assertFalse(membershipAuthenticationInterceptor.accept(membershipServiceClass, isMembershipPerksActiveOn));
    }

    private void configure(Binder binder) {
        binder.bind(AuthenticationChecker.class).toProvider(() -> authenticationChecker);
    }
}