package com.odigeo.membership.member.engineering.refresh;

import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class RefreshServletTest {

    @Test
    public void testConstructorReturnsNonNullInstance() throws Exception {
        assertNotNull(new RefreshServlet());
    }
}
