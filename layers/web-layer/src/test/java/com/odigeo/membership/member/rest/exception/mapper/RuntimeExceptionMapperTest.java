package com.odigeo.membership.member.rest.exception.mapper;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.ws.rs.core.Response;

public class RuntimeExceptionMapperTest {

    @Test
    public void testRuntimeExceptionMapper() {
        RuntimeExceptionMapper mapper = new RuntimeExceptionMapper();
        RuntimeException exception = new RuntimeException();
        Response response = mapper.toResponse(exception);
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getStatus(), Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
    }
}
