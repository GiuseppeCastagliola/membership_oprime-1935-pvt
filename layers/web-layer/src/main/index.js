import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { Root, App, PostBooking, RetryMembership } from 'Components';

const routing = (
    <Router>
        <Switch>
            <Route exact path="/" component={App} />
            <Route exact path="/retry-membership" component={RetryMembership} />
            <Route exact path="/postbooking" component={PostBooking} />
        </Switch>
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));
//
// if (module.hot) {
//     module.hot.accept('Components/', () => {
//         console.log('aquiiii');
//         // If you use Webpack 2 in ES modules mode, you can
//         // use <Root /> here rather than require() a <NextRoot />.
//         ReactDOM.render(
//             routing,
//             document.getElementById('main')
//         );
//     });
// }
