import React, { Component } from 'react';
import { Layout, Col, Row} from "antd";
import Bem from "react-bem-helper";
import PrimeLogo from "Assets/images/prime-logo.svg";
import PrimeLogoOp from "Assets/images/prime-logo-op.svg";
import PrimeLogoGo from "Assets/images/prime-logo-go.svg";
import './style.scss';

const { Footer: AntFooter } = Layout;
const classes = new Bem('footer');

class Footer extends Component {
    render() {
        return (
            <AntFooter {...classes('')}>
                <Row justify="center">
                    <Col span={6} offset={2}>
                        <PrimeLogo width={132} height={24}/>
                    </Col>
                    <Col span={6} offset={2}>
                        <PrimeLogoGo width={132} height={24}/>
                    </Col>
                    <Col span={6} offset={2}>
                        <PrimeLogoOp width={132} height={24}/>
                    </Col>
                </Row>
            </AntFooter>
        )
    }
}

export default Footer;
