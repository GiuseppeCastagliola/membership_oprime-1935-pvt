import React, { Component } from 'react';
import { Form, Input, Button, Layout, Col, Row } from 'antd';
import { Header, Footer } from 'Components';

const client = require('../common/client');
const { Content } = Layout;
const { Item } = Form;

class RetryMembership extends Component {

    constructor(props) {
        super(props);
        this.state = {bookingID: ''};
        this.state = {statusResponse: Boolean};
        this.state = {bookingSent: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    displayResults() {
        if (this.state.statusResponse) {
            alert('Booking ' + this.state.bookingSent + ' successfully sent to Membership Activation Retry Robot');
            return;
        }
        alert('Booking ' + this.state.bookingSent + ' cannot be processed for Membership activation retry');
    }

    handleChange (event) {
        this.setState({bookingID: event.target.value});
    }

    handleSubmit(event) {
        alert('A Booking was submitted: ' + this.state.bookingID);
        client({method: 'PUT', path: '/membership/membership/v1/retry-membership-activation/'+this.state.bookingID}).done(response => {
            this.setState({statusResponse :  response.entity});
            this.displayResults();
        });
        this.setState({bookingSent :  this.state.bookingID.toString()});
        this.setState({bookingID: ''});
        event.preventDefault();
    }

    render() {
        const formItemLayout = {
            labelCol: { span: 6 },
            wrapperCol: { span: 14 },
        };
        return (
            <Layout>
                <Header text="Retry Membership"/>
                <Content>
                    <Row>
                        <Col span={8} offset={8}>
                            <Form layout="vertical" onSubmit={this.handleSubmit} className="retry-form">
                                <Item {...formItemLayout} label="Booking ID:">
                                    <Input type="text" value={this.state.bookingID} onChange={this.handleChange} />
                                </Item>
                                <Item>
                                    <Button type="primary" htmlType="submit">Activate</Button>
                                </Item>
                            </Form>
                        </Col>
                    </Row>
                </Content>
                <Footer/>
            </Layout>
        );
    }
}

export default RetryMembership;
