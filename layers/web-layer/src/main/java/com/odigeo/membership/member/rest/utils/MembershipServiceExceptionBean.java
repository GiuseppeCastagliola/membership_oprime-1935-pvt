package com.odigeo.membership.member.rest.utils;

import com.odigeo.membership.exception.MembershipServiceException;

import javax.ws.rs.core.Response;

public class MembershipServiceExceptionBean extends BaseExceptionBean {

    private final int statusCode;

    public MembershipServiceExceptionBean(MembershipServiceException exception, boolean includeCause) {
        super(exception, includeCause);
        this.statusCode = exception.getStatus();
    }

    @Override
    public Response.Status getStatusToSend() {
        return Response.Status.fromStatusCode(statusCode);
    }
}
