package com.odigeo.membership.member.rest;

import com.odigeo.membership.member.rest.exception.mapper.MembershipServiceExceptionMapper;
import com.odigeo.membership.member.rest.exception.mapper.RuntimeExceptionMapper;
import com.odigeo.membership.member.rest.exception.mapper.ValidationExceptionMapper;

import java.util.HashSet;
import java.util.Set;

public final class MembershipServiceConfiguration {

    private MembershipServiceConfiguration() {
    }

    public static Set<Object> getMembershipApiExceptionMappers() {
        Set<Object> objects = new HashSet<>();
        objects.add(new MembershipServiceExceptionMapper());
        objects.add(new RuntimeExceptionMapper());
        objects.add(new ValidationExceptionMapper());
        return objects;
    }
}
