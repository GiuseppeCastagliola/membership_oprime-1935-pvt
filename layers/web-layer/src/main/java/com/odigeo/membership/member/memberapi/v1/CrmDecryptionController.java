package com.odigeo.membership.member.memberapi.v1;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.CRMDecryptionApi;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.member.memberapi.v1.util.MembershipExceptionMapper;
import com.odigeo.util.CrmCipher;

import java.io.UnsupportedEncodingException;

public class CrmDecryptionController implements CRMDecryptionApi {

    @Override
    public String getCrmDecryption(String token) throws MembershipServiceException {
        try {
            return getCipher().decryptToken(token);
        } catch (UnsupportedEncodingException e) {
            throw MembershipExceptionMapper.map(e);
        }
    }

    private CrmCipher getCipher() {
        return ConfigurationEngine.getInstance(CrmCipher.class);
    }
}
