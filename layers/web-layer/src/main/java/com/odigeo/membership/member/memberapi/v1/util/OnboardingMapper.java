package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.onboarding.Onboarding;
import com.odigeo.membership.onboarding.OnboardingDevice;
import com.odigeo.membership.onboarding.OnboardingEvent;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;

import javax.inject.Singleton;

@Singleton
public class OnboardingMapper {

    public Onboarding map(OnboardingEventRequest request) {

        return new Onboarding.Builder()
                .withMemberAccountId(request.getMemberAccountId())
                .withDevice(OnboardingDevice.valueOf(request.getDevice().toString()))
                .withEvent(OnboardingEvent.valueOf(request.getEvent().toString()))
                .build();
    }
}
