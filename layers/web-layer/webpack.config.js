const path = require("path");
const webpack = require("webpack");

module.exports = {
    entry: './src/main/index.js',
    devtool: 'sourcemaps',
    cache: true,
    mode: 'development',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel-loader",
                options: {
                    presets: ["@babel/env"]
                },
            },
            {
                test: /\.scss$/,
                use: ["style-loader","css-loader","sass-loader"]
            },
            {
                test: /\.less$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {loader: "less-loader",
                        options: {
                            javascriptEnabled: true
                        }}]
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                    {
                        loader: '@svgr/webpack',
                        options: {
                            babel: false,
                            icon: true,
                        },
                    },
                ],
            }
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx"],
        alias: {
            'Components': path.resolve(__dirname, './src/main/components/'),
            'Assets': path.resolve(__dirname, './src/main/assets/'),
        },
    },
    output: {
        path: path.join(__dirname, '/src/main/webapp/engineering/'),
        filename: 'bundle.js',
        publicPath: '/src/main/webapp/engineering'
    },
    devServer: {
        port: 9000,
        hot: true,
        publicPath: '/src/main/webapp/engineering'
    },
};

