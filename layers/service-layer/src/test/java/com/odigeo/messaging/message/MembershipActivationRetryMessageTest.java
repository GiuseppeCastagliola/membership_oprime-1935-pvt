package com.odigeo.messaging.message;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;

public class MembershipActivationRetryMessageTest {

    private static final Long BOOKING_ID = 123L;
    @Mock
    private MembershipActivationRetryMessageConfigurationManager configurationManager;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        when(configurationManager.getRetryAttempts()).thenReturn(2);
        when(configurationManager.getRetryPeriodUnit()).thenReturn(ChronoUnit.SECONDS.name());
        when(configurationManager.getRetryPeriod()).thenReturn(3L);
        ConfigurationEngine.init(binder -> binder.bind(MembershipActivationRetryMessageConfigurationManager.class)
                .toInstance(configurationManager));
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(MembershipActivationRetryMessage.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Test
    public void testCompareTo() throws InterruptedException {
        MembershipActivationRetryMessage retryMessage = MembershipActivationRetryMessage.createMessage(BOOKING_ID);
        TimeUnit.MILLISECONDS.sleep(500);
        MembershipActivationRetryMessage retryMessageNext = MembershipActivationRetryMessage.createMessageForNextAttempt(retryMessage);
        assertTrue(retryMessage.compareTo(retryMessageNext) < 0);
        assertTrue(retryMessageNext.compareTo(retryMessage) > 0);
    }

}