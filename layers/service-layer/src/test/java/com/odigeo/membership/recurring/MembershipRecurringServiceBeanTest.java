package com.odigeo.membership.recurring;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.recurring.selection.RecurringSelectionService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipRecurringServiceBeanTest {

    private static final String RECURRING_ID = "ABC";
    private static final long MEMBERSHIP_ID = 1L;
    private static final long BOOKING_ID = 123L;

    @Mock
    private DataSource dataSource;
    @Mock
    private RecurringService recurringService;
    @Mock
    private RecurringSelectionService recurringSelectionService;

    @InjectMocks
    private MembershipRecurringServiceBean membershipRecurringServiceBean = new MembershipRecurringServiceBean();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(RecurringSelectionService.class).toInstance(recurringSelectionService);
        binder.bind(RecurringService.class).toInstance(recurringService);
    }

    @Test
    public void testNoRecurringSelectionResponse() throws Exception {
        mockRecurringSelectionResponse(Boolean.FALSE);
        membershipRecurringServiceBean.processBooking(BOOKING_ID);
        verify(recurringService, never()).existsMembershipRecurringId(anyLong());
        verify(recurringService, never()).processRecurringBooking(anyString(), anyLong(), anyLong());
    }

    @Test
    public void testRecurringSelectionResponse() throws Exception {
        mockRecurringSelectionResponse(Boolean.TRUE);
        membershipRecurringServiceBean.processBooking(BOOKING_ID);
        verify(recurringService).existsMembershipRecurringId(MEMBERSHIP_ID);
    }

    @Test
    public void testNoOverrideRecurringId() throws Exception {
        mockRecurringSelectionResponse(Boolean.TRUE);
        when(recurringService.existsMembershipRecurringId(MEMBERSHIP_ID))
            .thenReturn(Boolean.TRUE);
        membershipRecurringServiceBean.processBooking(BOOKING_ID);
        verify(recurringService).existsMembershipRecurringId(MEMBERSHIP_ID);
        verify(recurringService, never()).processRecurringBooking(anyString(), anyLong(), anyLong());
    }

    @Test
    public void testCreateRecurringId() throws Exception {
        mockRecurringSelectionResponse(Boolean.TRUE);
        when(recurringService.existsMembershipRecurringId(anyLong())).thenReturn(Boolean.FALSE);
        membershipRecurringServiceBean.processBooking(BOOKING_ID);
        verify(recurringService).existsMembershipRecurringId(MEMBERSHIP_ID);
        verify(recurringService).processRecurringBooking(RECURRING_ID, MEMBERSHIP_ID, BOOKING_ID);
    }

    private void mockRecurringSelectionResponse(boolean withResult) throws RetryableException {
        RecurringSelectionResponse response = new RecurringSelectionResponse();
        response.setRecurringId(RECURRING_ID);
        response.setMembershipId(MEMBERSHIP_ID);
        when(recurringSelectionService.processBookingForRecurring(anyLong()))
            .thenReturn(withResult ? Optional.of(response) : Optional.empty());
    }
}
