package com.odigeo.membership.recurring.selection;

import com.codahale.metrics.SharedMetricRegistries;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.google.inject.multibindings.Multibinder;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.v12.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CollectionAttempt;
import com.odigeo.bookingapi.v12.responses.CollectionSummary;
import com.odigeo.bookingapi.v12.responses.ItineraryBooking;
import com.odigeo.bookingapi.v12.responses.Movement;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;
import com.odigeo.bookingapi.v12.responses.Website;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.recurring.RecurringSelectionResponse;
import com.odigeo.membership.recurring.selection.criteria.BookingStatusCriteria;
import com.odigeo.membership.recurring.selection.criteria.MembershipProductCriteria;
import com.odigeo.membership.recurring.selection.criteria.RecurringCriteria;
import com.odigeo.membership.recurring.selection.criteria.RecurringInfoCriteria;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class RecurringSelectionServiceTest {

    @Mock
    private BookingApiManager bookingApiManager;
    @Mock
    private RecurringBookingService recurringBookingService;

    private RecurringSelectionService recurringSelectionService;

    private static final String PAID_MOVEMENT_STATUS = "PAID";
    private static final String CONFIRM_ACTION_MOVEMENT = "CONFIRM";
    private static final String MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE = "MEMBERSHIP_SUBSCRIPTION";
    private static final String MEMBERSHIP_RENEWAL_PRODUCT_TYPE = "MEMBERSHIP_RENEWAL";
    private static final String FLIGHT = "FLIGHT";
    private static final String CONTRACT = "CONTRACT";
    private static final String INIT = "INIT";
    private static final String WEBSITE = "GOFR";
    private static final String RECURRING_ID = "1234";
    private static final Long MEMBERSHIP_ID = 415236L;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        recurringSelectionService = ConfigurationEngine.getInstance(RecurringSelectionService.class);
    }

    @AfterMethod
    public void clearMetric() {
        SharedMetricRegistries.clear();
    }

    @Test
    public void bookingStatusNotContract () throws RetryableException, BookingApiException {
        createResponseBookingDetailFromBookingApiManager(INIT, null, null, createBookingBasicInfo());
        Optional<RecurringSelectionResponse> response = recurringSelectionService.processBookingForRecurring(1l);
        assertFalse(response.isPresent());
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildMetric(MetricsNames.BOOKING_RECURRING), MetricsNames.METRICS_REGISTRY_NAME));
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildRecurringByWebsite(WEBSITE), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test
    public void bookingStatusContractTypeNotMembershipSubscriptionNotRenewal () throws RetryableException, BookingApiException {
        createResponseBookingDetailFromBookingApiManager(CONTRACT, createBookingProductsByType(FLIGHT), null, createBookingBasicInfo());
        Optional<RecurringSelectionResponse> response = recurringSelectionService.processBookingForRecurring(1l);
        assertFalse(response.isPresent());
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildMetric(MetricsNames.BOOKING_RECURRING), MetricsNames.METRICS_REGISTRY_NAME));
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildRecurringByWebsite(WEBSITE), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test
    public void bookingStatusContractTypeMembershipRecurringNull () throws RetryableException, BookingApiException {
        createResponseBookingDetailFromBookingApiManager(CONTRACT, createBookingProductsByType(MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE), createCollectionSummary(null), createBookingBasicInfo());
        Optional<RecurringSelectionResponse> response = recurringSelectionService.processBookingForRecurring(1l);
        assertFalse(response.isPresent());
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildMetric(MetricsNames.BOOKING_RECURRING), MetricsNames.METRICS_REGISTRY_NAME));
        assertFalse(MetricsUtils.existMetric(MetricsBuilder.buildRecurringByWebsite(WEBSITE), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test
    public void recurringMembershipSubscription () throws RetryableException, BookingApiException {
        createResponseBookingDetailFromBookingApiManager(CONTRACT, createBookingProductsByType(MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE), createCollectionSummary(RECURRING_ID), createBookingBasicInfo());
        when(recurringBookingService.getMembershipId(any())).thenReturn(Optional.of(MEMBERSHIP_ID));
        when(recurringBookingService.getRecurringId(any())).thenReturn(Optional.of(RECURRING_ID));
        Optional<RecurringSelectionResponse> response = recurringSelectionService.processBookingForRecurring(1l);
        assertTrue(response.isPresent());
        assertEquals(response.get().getMembershipId(), MEMBERSHIP_ID.longValue());
        assertEquals(response.get().getRecurringId(), RECURRING_ID);
        assertTrue(MetricsUtils.existMetric(MetricsBuilder.buildMetric(MetricsNames.BOOKING_RECURRING), MetricsNames.METRICS_REGISTRY_NAME));
        assertTrue(MetricsUtils.existMetric(MetricsBuilder.buildRecurringByWebsite(WEBSITE), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test
    public void recurringMembershipRenewal () throws RetryableException, BookingApiException {
        createResponseBookingDetailFromBookingApiManager(CONTRACT, createBookingProductsByType(MEMBERSHIP_RENEWAL_PRODUCT_TYPE), createCollectionSummary(RECURRING_ID), createBookingBasicInfo());
        when(recurringBookingService.getMembershipId(any())).thenReturn(Optional.of(MEMBERSHIP_ID));
        when(recurringBookingService.getRecurringId(any())).thenReturn(Optional.of(RECURRING_ID));
        Optional<RecurringSelectionResponse> response = recurringSelectionService.processBookingForRecurring(1l);
        assertTrue(response.isPresent());
        assertEquals(response.get().getMembershipId(), MEMBERSHIP_ID.longValue());
        assertEquals(response.get().getRecurringId(), RECURRING_ID);
        assertTrue(MetricsUtils.existMetric(MetricsBuilder.buildMetric(MetricsNames.BOOKING_RECURRING), MetricsNames.METRICS_REGISTRY_NAME));
        assertTrue(MetricsUtils.existMetric(MetricsBuilder.buildRecurringByWebsite(WEBSITE), MetricsNames.METRICS_REGISTRY_NAME));
    }

    @Test(expectedExceptions = RetryableException.class)
    public void getBookingRespondsException () throws RetryableException, BookingApiException {
        when(bookingApiManager.getBooking(anyLong())).thenThrow(new BookingApiException(StringUtils.EMPTY));
        recurringSelectionService.processBookingForRecurring(1l);
    }

    private void createResponseBookingDetailFromBookingApiManager(String status, List<ProductCategoryBooking> bookingProductsByType, CollectionSummary collectionSummary, BookingBasicInfo bookingBasicInfo) throws BookingApiException {
        BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setBookingStatus(status);
        bookingDetail.setBookingProducts(bookingProductsByType);
        bookingDetail.setCollectionSummary(collectionSummary);
        bookingDetail.setBookingBasicInfo(bookingBasicInfo);
        when(bookingApiManager.getBooking(anyLong())).thenReturn(bookingDetail);
    }

    private BookingBasicInfo createBookingBasicInfo() {
        Website website = new Website();
        website.setCode(WEBSITE);
        BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
        bookingBasicInfo.setWebsite(website);
        bookingBasicInfo.setId(1L);
        return bookingBasicInfo;
    }

    private List<ProductCategoryBooking> createBookingProductsByType(String type) {
        ProductCategoryBooking product = new ItineraryBooking();
        product.setProductType(type);
        return Collections.singletonList(product);
    }

    private CollectionSummary createCollectionSummary(String recurring) {
        Movement movement = new Movement();
        movement.setStatus(PAID_MOVEMENT_STATUS);
        movement.setAction(CONFIRM_ACTION_MOVEMENT);
        CollectionAttempt attempt = new CollectionAttempt();
        attempt.setRecurringId(recurring);
        attempt.setValidMovements(Collections.singletonList(movement));
        CollectionSummary collectionSummary = new CollectionSummary();
        collectionSummary.setAttempts(Collections.singletonList(attempt));
        return collectionSummary;
    }

    private void configure(Binder binder) {
        binder.bind(BookingApiManager.class).toInstance(bookingApiManager);
        binder.bind(RecurringBookingService.class).toInstance(recurringBookingService);
        Multibinder<RecurringCriteria> multiBinder =  Multibinder.newSetBinder(binder, RecurringCriteria.class);
        multiBinder.addBinding().toInstance(new BookingStatusCriteria());
        multiBinder.addBinding().toInstance(new MembershipProductCriteria());
        multiBinder.addBinding().toInstance(new RecurringInfoCriteria());
    }
}
