package com.odigeo.membership.recurring;

import org.apache.log4j.Logger;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class RecurringUtilsTest {
    private static final long BOOKING_ID = 1L;
    private static final String TEST_MESSAGE = "TEST MESSAGE";
    private static final String WRITER = "ME";
    private static final int ONE = 1;

    @Mock
    private Logger logger;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        logger = mock(Logger.class);
    }

    @Test
    public void testRecurringStartLog() throws Exception {
        RecurringUtils.startRecurringLog(logger, BOOKING_ID);
        verify(logger, times(ONE)).info(RecurringUtils.START_MESSAGE + BOOKING_ID);
    }

    @Test
    public void testRecurringStopLog() throws Exception {
        RecurringUtils.stopRecurringLog(logger, BOOKING_ID);
        verify(logger, times(ONE)).info(RecurringUtils.STOP_MESSAGE + BOOKING_ID);
    }

    @Test
    public void testProcessedRecurringLog() throws Exception {
        RecurringUtils.processedRecurringLog(logger, BOOKING_ID, TEST_MESSAGE);
        verify(logger, times(ONE)).info(RecurringUtils.RECURRING_ROBOT + RecurringUtils.PROCESSING_BOOKING + BOOKING_ID + TEST_MESSAGE);
    }

    @Test
    public void testProcessingRecurringLog() throws Exception {
        RecurringUtils.processingRecurringLog(logger, BOOKING_ID, WRITER, TEST_MESSAGE);
    }
}
