package com.odigeo.membership.recurring.selection.criteria;

import com.odigeo.bookingapi.v12.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class StatusCriteriaTest {

    private static final String INIT_STATUS = "INIT";
    private final List<String> statusWithValidRecurring = Arrays.asList("CONTRACT", "REQUEST");

    private BookingDetail bookingDetail = new BookingDetail();
    private BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();

    private RecurringCriteria statusCriteria = new BookingStatusCriteria();

    @BeforeMethod
    public void setUp() throws Exception {
        bookingBasicInfo.setId(1L);
        bookingDetail.setBookingBasicInfo(bookingBasicInfo);
    }

    @Test
    public void testStatusCriteriaCorrectStatus() throws Exception {
        bookingDetail.setBookingStatus(statusWithValidRecurring.get(0));
        assertTrue(statusCriteria.evaluate(bookingDetail));
    }

    @Test
    public void testStatusCriteriaInvalidStatus() throws Exception {
        bookingDetail.setBookingStatus(INIT_STATUS);
        assertFalse(statusCriteria.evaluate(bookingDetail));
    }

}