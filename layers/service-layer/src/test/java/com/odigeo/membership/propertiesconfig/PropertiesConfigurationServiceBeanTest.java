package com.odigeo.membership.propertiesconfig;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.exception.DataNotFoundException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PropertiesConfigurationServiceBeanTest {

    private static final String TEST_KEY = "test key";

    private PropertiesConfigurationManager propertiesConfigManagerMock;
    private PropertiesCacheService propertiesCacheServiceMock;
    private PropertiesConfigurationService propertiesConfigService;

    @BeforeMethod
    public void setUp() {
        propertiesConfigService = new PropertiesConfigurationServiceBean();
        ConfigurationEngine.init(this::configureMocks);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsTrueWhenRetrievedValueIsTrue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isSendingIdsToKafkaActive(any(DataSource.class))).willReturn(true);
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertTrue(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsFalseWhenRetrievedValueIsFalse() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isSendingIdsToKafkaActive(any(DataSource.class))).willReturn(false);
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsFalseWhenSqlExceptionRetrievingValue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isSendingIdsToKafkaActive(any(DataSource.class))).willThrow(new SQLException("expected SQL exception"));
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testIsSendingIdsToKafkaActiveIsFalseWhenDataNotFoundExceptionRetrievingValue() throws SQLException, DataNotFoundException {
        //Given
        given(propertiesConfigManagerMock.isSendingIdsToKafkaActive(any(DataSource.class))).willThrow(new SQLException("expected DataNotFoundException exception"));
        //When
        boolean sendingIdsToKafkaActive = propertiesConfigService.isSendingIdsToKafkaActive();
        //Then
        assertFalse(sendingIdsToKafkaActive);
    }

    @Test
    public void testUpdatePropertyValueIsTrueWhenUpdated() throws Exception {
        //Given
        given(propertiesConfigManagerMock.updatePropertyValue(any(DataSource.class), eq(TEST_KEY), eq(FALSE))).willReturn(TRUE);
        //When
        boolean updatePropertyValue = propertiesConfigService.updatePropertyValue(TEST_KEY, FALSE);
        //Then
        assertTrue(updatePropertyValue);
    }

    @Test
    public void testUpdatePropertyValueIsFalseWhenNotUpdated() throws Exception {
        //Given
        given(propertiesConfigManagerMock.updatePropertyValue(any(DataSource.class), eq(TEST_KEY), eq(TRUE))).willReturn(FALSE);
        //When
        boolean updatePropertyValue = propertiesConfigService.updatePropertyValue(TEST_KEY, TRUE);
        //Then
        assertFalse(updatePropertyValue);
    }

    @Test
    public void testUpdatePropertyValueIsFalseWhenErrorUpdating() throws Exception {
        //Given
        given(propertiesConfigManagerMock.updatePropertyValue(any(DataSource.class), eq(TEST_KEY), eq(TRUE))).willThrow(new DataAccessException("expected exception"));
        //When
        boolean updatePropertyValue = propertiesConfigService.updatePropertyValue(TEST_KEY, TRUE);
        //Then
        assertFalse(updatePropertyValue);
    }

    private void configureMocks(final Binder binder) {
        propertiesConfigManagerMock = mock(PropertiesConfigurationManager.class);
        propertiesCacheServiceMock = mock(PropertiesCacheService.class);

        binder.bind(PropertiesConfigurationManager.class).toInstance(propertiesConfigManagerMock);
        binder.bind(PropertiesCacheService.class).toInstance(propertiesCacheServiceMock);
    }
}