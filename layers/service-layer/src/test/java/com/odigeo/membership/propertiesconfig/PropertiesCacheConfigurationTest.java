package com.odigeo.membership.propertiesconfig;


import bean.test.BeanTest;
import com.edreams.persistance.cache.Cache;

import static org.mockito.Mockito.mock;

public class PropertiesCacheConfigurationTest extends BeanTest<PropertiesCacheConfiguration> {

    private final Cache cacheMock = mock(Cache.class);

    @Override
    protected PropertiesCacheConfiguration getBean() {
        PropertiesCacheConfiguration configuration = new PropertiesCacheConfiguration();
        configuration.setIsSendingIdsToKafkaActiveCache(cacheMock);
        return configuration;
    }
}