package com.odigeo.membership.recurring.selection.criteria;

import com.odigeo.bookingapi.v12.responses.BookingBasicInfo;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.singletonList;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class ProductCriteriaTest {

    private static final String MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE = "MEMBERSHIP_SUBSCRIPTION";

    private BookingDetail bookingDetail = new BookingDetail();
    private BookingBasicInfo bookingBasicInfo = new BookingBasicInfo();
    private RecurringCriteria productCriteria = new MembershipProductCriteria();

    @BeforeMethod
    public void setUp() throws Exception {
        bookingBasicInfo.setId(1L);
        bookingDetail.setBookingBasicInfo(bookingBasicInfo);
    }

    @Test
    public void testProductCriteraProductFound() throws Exception {
        MembershipSubscriptionBooking membershipSubscriptionBooking = new MembershipSubscriptionBooking();
        membershipSubscriptionBooking.setProductType(MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE);
        bookingDetail.setBookingProducts(singletonList(membershipSubscriptionBooking));
        assertTrue(productCriteria.evaluate(bookingDetail));
    }

    @Test
    public void testProductCriteraProductNotFound() throws Exception {
        bookingDetail.setBookingProducts(EMPTY_LIST);
        assertFalse(productCriteria.evaluate(bookingDetail));
    }

    @Test
    public void testProductCriteraEmptyBookingProducts() throws Exception {
        bookingDetail.setBookingProducts(null);
        assertFalse(productCriteria.evaluate(bookingDetail));
    }

}