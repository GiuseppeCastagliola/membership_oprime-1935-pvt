package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.parameters.MemberOnPassengerListParameter;
import com.odigeo.membership.parameters.TravellerParameter;
import com.odigeo.membership.parameters.search.MemberAccountSearch;
import com.odigeo.membership.product.MembershipSubscriptionConfiguration;
import com.odigeo.membership.request.product.FreeTrialCandidateRequest;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v1.model.Brand;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import com.odigeo.visitengineapi.last.VisitEngineException;
import com.odigeo.visitengineapi.last.multitest.TestAssignment;
import com.odigeo.visitengineapi.last.multitest.TestAssignmentNotFoundException;
import com.odigeo.visitengineapi.last.multitest.TestAssignmentService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.ArrayUtils.removeElement;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MembershipValidationServiceBeanTest {

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private UserApiManager userApiManager;
    @Mock
    private SearchService searchService;
    @Mock
    private MemberVatModelService memberVatModelService;
    @Mock
    private MemberAccountService memberAccountService;
    @Mock
    private BookingTrackingService bookingTrackingService;
    @Mock
    private TestAssignmentService testAssignmentService;
    @Mock
    private MembershipSubscriptionConfiguration membershipSubscriptionConfiguration;

    @InjectMocks
    private MembershipValidationServiceBean membershipValidationServiceBean = new MembershipValidationServiceBean();

    private static final String SITE_ES = "ES";
    private static final String SITE_IT = "IT";
    private static final String SITE_OPFR = "OPFR";
    private static final int DEFAULT_DURATION = 12;
    private static final String VAT_MODEL_DATE = "2019-09-30";
    private static final LocalDateTime NOW = LocalDateTime.now();
    private static final long USER_ID = 123L;
    private static final long MEMBER_ID1 = 10L;
    private static final long MEMBER_ID2 = 12L;
    private static final long MEMBER_ID3 = 13L;
    private static final long MEMBER_ACCOUNT_ID1 = 1L;
    private static final long MEMBER_ACCOUNT_ID2 = 3L;
    private static final String MEMBER_NAME1 = "JOSE";
    private static final String MEMBER_SURNAME1 = "LUIS";
    private static final String MEMBER_NAME3_SPACE = "  Member3 Compose ";
    private static final String EMAIL = "oprime@mail.com";

    private MemberAccount memberAccount1 = new MemberAccount(1L, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
    private MemberAccount memberAccount2 = new MemberAccount(3L, USER_ID, MEMBER_NAME3_SPACE, MEMBER_SURNAME1);

    private Membership activeMembership1 = new MembershipBuilder().setId(MEMBER_ID1).setWebsite(SITE_ES).setStatus(
            MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setActivationDate(NOW)
            .setExpirationDate(NOW.plusMonths(DEFAULT_DURATION)).setMemberAccountId(MEMBER_ACCOUNT_ID1).build();
    private Membership activeMembership2 = new MembershipBuilder().setId(MEMBER_ID3).setWebsite(SITE_IT)
            .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED)
            .setExpirationDate(NOW.minusMonths(DEFAULT_DURATION)).setMemberAccountId(MEMBER_ACCOUNT_ID2).build();
    private Membership pendingMembership = new MembershipBuilder().setId(MEMBER_ID2).setWebsite(SITE_IT)
            .setStatus(MemberStatus.PENDING_TO_ACTIVATE).setMembershipRenewal(MembershipRenewal.ENABLED)
            .setMemberAccountId(MEMBER_ACCOUNT_ID2).build();

    private UserBasicInfo userBasicInfo = new UserBasicInfo();
    private UserInfo userInfo;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(new AbstractModule() {
            protected void configure() {
                bind(BookingTrackingService.class).toInstance(bookingTrackingService);
                bind(TestAssignmentService.class).toInstance(testAssignmentService);
            }
        });
        userBasicInfo.setId(USER_ID);
        userBasicInfo.setEmail(EMAIL);
        userBasicInfo.setBrand(Brand.ED);
        userBasicInfo.setWebsite(SITE_ES);
        userInfo = UserInfo.fromUserBasicInfo(userBasicInfo);
    }

    @Test
    public void testApplyMembershipBookingLimit() throws Exception {
        testApplyMembershipWithCustomBookingLimit(Boolean.FALSE);
    }

    @Test
    public void testApplyMembershipNoBookingLimit() throws Exception {
        setUpMembersByUserIdResponse();
        testApplyMembershipWithCustomBookingLimit(Boolean.TRUE);
    }

    private void testApplyMembershipWithCustomBookingLimit(final Boolean expectedResult) throws Exception {
        List<TravellerParameter> travelers = getTravellerParameters(MEMBER_SURNAME1, MEMBER_NAME1);
        MemberOnPassengerListParameter parameters = getMemberOnPassengerListParameter(travelers, USER_ID, SITE_ES);
        when(memberManager.getMembersWithActivatedMembershipsByUserId(USER_ID, dataSource))
                .thenReturn(Arrays.asList(memberAccount1, memberAccount2));
        assertEquals(membershipValidationServiceBean.applyMembership(parameters), expectedResult);
    }

    @Test
    public void testIsMembershipActiveOnWebsitePositive() {
        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Arrays.asList(SITE_OPFR, SITE_IT));
        assertTrue(membershipValidationServiceBean.isMembershipActiveOnWebsite(SITE_OPFR));
    }

    @Test
    public void testIsMembershipActiveOnWebsiteNegative() {
        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.emptyList());
        assertFalse(membershipValidationServiceBean.isMembershipActiveOnWebsite(SITE_OPFR));
    }

    private MemberOnPassengerListParameter getMemberOnPassengerListParameter(List<TravellerParameter> travelers, long userId, String website) {
        MemberOnPassengerListParameter parameters = new MemberOnPassengerListParameter();
        parameters.setSite(website);
        parameters.setUserId(userId);
        parameters.setTravellerList(travelers);
        return parameters;
    }

    private List<TravellerParameter> getTravellerParameters(String memberSurname1, String memberName1) {
        TravellerParameter traveller = new TravellerParameter();
        traveller.setName(memberName1);
        traveller.setLastNames(memberSurname1);
        return Collections.singletonList(traveller);
    }

    private void setUpMembersByUserIdResponse() throws DataAccessException {
        memberAccount1.setMemberships(Collections.singletonList(activeMembership1));
        memberAccount2.setMemberships(Arrays.asList(pendingMembership, activeMembership2));
        when(memberAccountService.getActiveMembersByUserId(USER_ID))
                .thenReturn(Arrays.asList(memberAccount1, memberAccount2));
    }

    @Test
    public void testGetNewVatModel() throws TestAssignmentNotFoundException, VisitEngineException {
        when(memberVatModelService.getCutOverDate()).thenReturn(VAT_MODEL_DATE);
        when(testAssignmentService.getTestAssignment(anyString())).thenReturn(createTestAssignment(2));
        assertEquals(membershipValidationServiceBean.getNewVatModelDate(), memberVatModelService.getCutOverDate());
    }

    private TestAssignment createTestAssignment(int winnerPartition) {
        TestAssignment testAssignment = new TestAssignment();
        testAssignment.setEnabled(true);
        testAssignment.setWinnerPartition(winnerPartition);
        return testAssignment;
    }

    @Test
    public void testIsMembershipToBeRenewedActiveMembershipAndPastDue() throws MissingElementException, DataAccessException {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID3)).thenReturn(activeMembership2);
        assertTrue(membershipValidationServiceBean.isMembershipToBeRenewed(MEMBER_ID3));
    }

    @Test
    public void testIsMembershipToBeRenewedActiveMembershipAndNotPastDue() throws MissingElementException, DataAccessException {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID1)).thenReturn(activeMembership1);
        assertFalse(membershipValidationServiceBean.isMembershipToBeRenewed(MEMBER_ID1));
    }

    @Test
    public void testIsMembershipToBeRenewedInactiveMembershipAndPastDue() throws MissingElementException, DataAccessException {
        when(memberManager.getMembershipById(dataSource, MEMBER_ID2)).thenReturn(pendingMembership);
        assertFalse(membershipValidationServiceBean.isMembershipToBeRenewed(MEMBER_ID2));
    }

    @Test
    public void isBasicFreeMembershipPermitted_ineligibleWebsite() throws DataAccessException {
        //Given
        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.emptyList());
        //When
        boolean isBasicFreeMembershipPermitted = membershipValidationServiceBean.isEligibleForFreeTrial(candidateRequestBuilder("ZZ"));
        //Then
        assertFalse(isBasicFreeMembershipPermitted);
        verify(userApiManager, never()).getUserInfo(eq(USER_ID));
        verify(searchService, never()).searchMemberAccounts(any(MemberAccountSearch.class));
    }

    @Test
    public void isBasicFreeMembershipPermitted_defaultUserInfo() throws DataAccessException {
        //Given
        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.singletonList(SITE_ES));
        when(userApiManager.getUserInfo(eq(EMAIL), eq(SITE_ES))).thenReturn(UserInfo.defaultUserInfo());
        //When
        boolean isBasicFreeMembershipPermitted = membershipValidationServiceBean.isEligibleForFreeTrial(candidateRequestBuilder(SITE_ES));
        //Then
        assertTrue(isBasicFreeMembershipPermitted);
        verify(userApiManager).getUserInfo(eq(EMAIL), eq(SITE_ES));
        verify(searchService, never()).searchMemberAccounts(any(MemberAccountSearch.class));
    }

    @Test
    public void isBasicFreeMembershipPermitted_noMemberAccountsFound() throws DataAccessException {
        //Given
        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.singletonList(SITE_ES));
        when(userApiManager.getUserInfo(eq(EMAIL), eq(SITE_ES))).thenReturn(userInfo);
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.emptyList());
        //When
        boolean isBasicFreeMembershipPermitted = membershipValidationServiceBean.isEligibleForFreeTrial(candidateRequestBuilder(SITE_ES));
        //Then
        assertTrue(isBasicFreeMembershipPermitted);
        verify(userApiManager).getUserInfo(eq(EMAIL), eq(SITE_ES));
        verify(searchService).searchMemberAccounts(any(MemberAccountSearch.class));
    }

    @Test
    public void isBasicFreeMembershipPermitted_memberAccountNoMemberships() throws DataAccessException {
        //Given
        MemberAccount memberAccount = new MemberAccount(MEMBER_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);

        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.singletonList(SITE_ES));
        when(userApiManager.getUserInfo(eq(EMAIL), eq(SITE_ES))).thenReturn(userInfo);
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.singletonList(memberAccount));
        //When
        boolean isBasicFreeMembershipPermitted = membershipValidationServiceBean.isEligibleForFreeTrial(candidateRequestBuilder(SITE_ES));
        //Then
        assertTrue(isBasicFreeMembershipPermitted);
        verify(userApiManager).getUserInfo(eq(EMAIL), eq(SITE_ES));
        verify(searchService).searchMemberAccounts(any(MemberAccountSearch.class));
    }

    @DataProvider(name = "freeTrialPermittedData")
    public Object[][] getData() {
        return new Object[][] {
                {MembershipType.values(), SourceType.values(), new int[]{1,3,6,12}, removeElement(MembershipRenewal.values(),MembershipRenewal.DISABLED)},
                {MembershipType.values(), SourceType.values(), new int[]{3,6,12}, MembershipRenewal.values()},
                {MembershipType.values(), removeElement(SourceType.values(), SourceType.FUNNEL_BOOKING), new int[]{1,3,6,12}, MembershipRenewal.values()},
                {removeElement(MembershipType.values(), MembershipType.BASIC), SourceType.values(), new int[]{1,3,6,12}, MembershipRenewal.values()}
        };
    }

    @Test(dataProvider = "freeTrialPermittedData")
    public void isFreeTrialMembershipPermitted_FreeTrialPermitted(MembershipType[] membershipTypes, SourceType[] sourceTypes, int[] monthsDuration, MembershipRenewal[] renewalValues) throws DataAccessException {
        List<Membership> memberships = new ArrayList<>();
        for (MemberStatus status : MemberStatus.values()) {
            for (MembershipRenewal renewal : renewalValues) {
                for (int duration : monthsDuration) {
                    for (SourceType source : sourceTypes) {
                        for (MembershipType membershipType : membershipTypes) {
                            memberships.add(new MembershipBuilder().setStatus(status).setMembershipRenewal(renewal).setMonthsDuration(duration)
                                    .setSourceType(source).setMembershipType(membershipType).build());
                        }
                    }
                }
            }
        }
        assertFreeTrialIsPermittedWithExistingMemberships(memberships);
    }

    private void assertFreeTrialIsPermittedWithExistingMemberships(List<Membership> memberships) throws DataAccessException {
        //Given
        MemberAccount memberAccount = new MemberAccount(MEMBER_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
        memberAccount.setMemberships(memberships);

        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.singletonList(SITE_ES));
        when(userApiManager.getUserInfo(eq(EMAIL), eq(SITE_ES))).thenReturn(userInfo);
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.singletonList(memberAccount));
        //When
        boolean isFreeTrialMembershipPermitted = membershipValidationServiceBean.isEligibleForFreeTrial(candidateRequestBuilder(SITE_ES));
        //Then
        assertTrue(isFreeTrialMembershipPermitted);
        verify(userApiManager).getUserInfo(eq(EMAIL), eq(SITE_ES));
        verify(searchService).searchMemberAccounts(any(MemberAccountSearch.class));
    }

    @Test
    public void isFreeTrialMembershipPermitted_FreeTrialProhibited() throws DataAccessException {
        //Given
        MemberAccount memberAccount = new MemberAccount(MEMBER_ID1, USER_ID, MEMBER_NAME1, MEMBER_SURNAME1);
        Membership membership = new MembershipBuilder()
                .setMembershipType(MembershipType.BASIC)
                .setSourceType(SourceType.FUNNEL_BOOKING)
                .setMonthsDuration(1)
                .setMembershipRenewal(MembershipRenewal.DISABLED)
                .build();
        memberAccount.setMemberships(Collections.singletonList(membership));

        when(membershipSubscriptionConfiguration.getActiveSitesList()).thenReturn(Collections.singletonList(SITE_ES));
        when(userApiManager.getUserInfo(eq(EMAIL), eq(SITE_ES))).thenReturn(userInfo);
        when(searchService.searchMemberAccounts(any(MemberAccountSearch.class))).thenReturn(Collections.singletonList(memberAccount));
        //When
        boolean isFreeTrialMembershipPermitted = membershipValidationServiceBean.isEligibleForFreeTrial(candidateRequestBuilder(SITE_ES));
        //Then
        assertFalse(isFreeTrialMembershipPermitted);
        verify(userApiManager).getUserInfo(eq(EMAIL), eq(SITE_ES));
        verify(searchService).searchMemberAccounts(any(MemberAccountSearch.class));
    }

    private FreeTrialCandidateRequest candidateRequestBuilder(String website) {
        return new FreeTrialCandidateRequest.Builder().withEmail(EMAIL).withWebsite(website).build();
    }
}
