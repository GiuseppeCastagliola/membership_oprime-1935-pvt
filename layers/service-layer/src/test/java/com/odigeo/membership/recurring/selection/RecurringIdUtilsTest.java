package com.odigeo.membership.recurring.selection;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CollectionAttempt;
import com.odigeo.bookingapi.v12.responses.CollectionSummary;
import com.odigeo.bookingapi.v12.responses.Movement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RecurringIdUtilsTest {

    @Test
    public void testPaidAndConfirmedLastMovementReturnsRecurringIdWhenPresent() {
        RecurringIdUtils recurringIdUtils = new RecurringIdUtils();
        BookingDetail bookingDetail = getBookingDetail("abc123", "CONFIRM", "PAID");
        Assert.assertEquals(recurringIdUtils.getRecurringId(bookingDetail).get(), "abc123");
    }

    @Test
    public void testPaidAndDirectyPaymentLastMovementReturnsRecurringIdWhenPresent() {
        RecurringIdUtils recurringIdUtils = new RecurringIdUtils();
        BookingDetail bookingDetail = getBookingDetail("abc123", "DIRECTY_PAYMENT", "PAID");
        Assert.assertEquals(recurringIdUtils.getRecurringId(bookingDetail).get(), "abc123");
    }

    @Test
    public void testRefundedAndManualRefundLastMovementReturnsRecurringIdWhenPresent() {
        RecurringIdUtils recurringIdUtils = new RecurringIdUtils();
        BookingDetail bookingDetail = getBookingDetail("abc123", "MANUAL_REFUND", "REFUNDED");
        Assert.assertEquals(recurringIdUtils.getRecurringId(bookingDetail).get(), "abc123");
    }

    @Test
    public void testRefundedAndDirectyPaymentLastMovementReturnsRecurringIdWhenPresent() {
        RecurringIdUtils recurringIdUtils = new RecurringIdUtils();
        BookingDetail bookingDetail = getBookingDetail("abc123", "DIRECTY_PAYMENT", "REFUNDED");
        Assert.assertEquals(recurringIdUtils.getRecurringId(bookingDetail).get(), "abc123");
    }

    @Test
    public void testChargedBackLastMovementReturnsRecurringIdWhenPresent() {
        RecurringIdUtils recurringIdUtils = new RecurringIdUtils();
        BookingDetail bookingDetail = getBookingDetail("abc123", "CHARGEBACK", "CHARGEBACKED");
        Assert.assertEquals(recurringIdUtils.getRecurringId(bookingDetail).get(), "abc123");
    }

    private BookingDetail getBookingDetail(String recurringId, String action, String status) {
        BookingDetail bookingDetail = new BookingDetail();
        List<CollectionAttempt> attempts = new ArrayList<CollectionAttempt>();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        collectionAttempt.setRecurringId(recurringId);
        List<Movement> validMovements = new ArrayList<Movement>();
        Movement movement = new Movement();
        movement.setAction(action);
        movement.setStatus(status);
        validMovements.add(movement);
        collectionAttempt.setValidMovements(validMovements);
        attempts.add(collectionAttempt);
        CollectionSummary collectionSummary = new CollectionSummary();
        collectionSummary.setAttempts(attempts);
        bookingDetail.setCollectionSummary(collectionSummary);
        return bookingDetail;
    }

}
