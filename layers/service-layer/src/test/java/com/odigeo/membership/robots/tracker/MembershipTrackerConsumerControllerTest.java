package com.odigeo.membership.robots.tracker;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.robots.activator.BookingUpdatesConsumerConfiguration;
import com.odigeo.membership.robots.activator.MockBasicMessageConsumerIterator;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

public class MembershipTrackerConsumerControllerTest {

    private static final int NUM_THREADS = 2;
    private static final String ZOO_KEEPER_URL = "4.4.4.4";
    private static final String TRACKER_CONSUMER_GROUP = "MembershipTrackingRobot";
    private static final String TOPIC_NAME = "BOOKING_UPDATES_V1";

    @Mock
    private BookingUpdatesConsumerConfiguration bookingUpdatesConsumerConfiguration;
    @Mock
    private ExecutorService executorService;
    @Mock
    private Consumer<BasicMessage> consumer;

    @InjectMocks
    private MembershipTrackerConsumerController membershipTrackerConsumerController;

    @BeforeMethod
    public void setUp() {
        setConfiguration();
        membershipTrackerConsumerController = new MembershipTrackerConsumerController(bookingUpdatesConsumerConfiguration);
        initMocks(this);
        ConfigurationEngine.init(this::configure);
    }

    private void configure(Binder binder) {
        binder.bind(BookingUpdatesConsumerConfiguration.class).toInstance(bookingUpdatesConsumerConfiguration);
    }

    private void setConfiguration() {
        bookingUpdatesConsumerConfiguration = new BookingUpdatesConsumerConfiguration();
        bookingUpdatesConsumerConfiguration.setTrackerConsumerGroup(TRACKER_CONSUMER_GROUP);
        bookingUpdatesConsumerConfiguration.setNumConsumerThreads(NUM_THREADS);
        bookingUpdatesConsumerConfiguration.setZooKeeperUrl(ZOO_KEEPER_URL);
        bookingUpdatesConsumerConfiguration.setTopicName(TOPIC_NAME);
    }

    @Test
    public void testConstructor() {
        assertNotNull(membershipTrackerConsumerController);
    }

    @Test
    public void testStartMultiThreadConsumer() {
        ConsumerIterator<BasicMessage> iteratorA = new MockBasicMessageConsumerIterator("10", "20", "30");
        ConsumerIterator<BasicMessage> iteratorB = new MockBasicMessageConsumerIterator("40", "50", "60");
        List<ConsumerIterator<BasicMessage>> consumerIterators = Arrays.asList(iteratorA, iteratorB);
        when(consumer.connectAndIterate(eq(NUM_THREADS))).thenReturn(consumerIterators);
        membershipTrackerConsumerController.startMultiThreadConsumer();
        verify(executorService, times(NUM_THREADS)).execute(any(MembershipTrackerConsumerWorker.class));
    }

    @Test
    public void testStopMultiThreadConsumer() {
        membershipTrackerConsumerController.stopMultiThreadConsumer();
        verify(executorService).shutdown();
        verify(consumer).close();
    }

    @Test
    public void testJoin() {
        try {
            membershipTrackerConsumerController.join();
            verify(executorService).awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            fail();
        }
    }
}
