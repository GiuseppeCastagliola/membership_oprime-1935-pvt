package com.odigeo.membership.recurring.selection;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CollectionAttempt;
import com.odigeo.bookingapi.v12.responses.CollectionSummary;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import com.odigeo.bookingapi.v12.responses.Movement;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;
import com.odigeo.bookingapi.v12.responses.TransferBooking;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RecurringBookingServiceTest {

    @Test
    public void testGetRecurringIdWithNullObjectsReturnsNull() {

        Assert.assertFalse(new RecurringBookingService().getRecurringId(null).isPresent());
        Assert.assertFalse(new RecurringBookingService().getRecurringId(new BookingDetail()).isPresent());

        final BookingDetail bookingDetail = new BookingDetail();
        bookingDetail.setCollectionSummary(new CollectionSummary());
        Assert.assertFalse(new RecurringBookingService().getRecurringId(bookingDetail).isPresent());
    }

    @Test
    public void testGetRecurringIdWithSingleAttemptWithRecurringId() {

        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        List<CollectionAttempt> attempts = new ArrayList<>();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        final ArrayList<Movement> validMovements = new ArrayList<>();
        final Movement movement = new Movement();
        movement.setStatus("PAID");
        movement.setAction("DIRECTY_PAYMENT");
        validMovements.add(movement);
        collectionAttempt.setValidMovements(validMovements);
        collectionAttempt.setRecurringId("123");
        attempts.add(collectionAttempt);
        collectionSummary.setAttempts(attempts);
        bookingDetail.setCollectionSummary(collectionSummary);

        Assert.assertEquals(new RecurringBookingService().getRecurringId(bookingDetail).get(), "123");
    }

    @Test
    public void testGetRecurringIdWithMultipleAttemptsAndRecurringIdIsNotInLastAttempt() {

        BookingDetail bookingDetail = new BookingDetail();
        CollectionSummary collectionSummary = new CollectionSummary();
        CollectionAttempt collectionAttempt = new CollectionAttempt();
        final ArrayList<Movement> validMovements = new ArrayList<>();
        final Movement movement = new Movement();
        movement.setStatus("PAID");
        movement.setAction("DIRECTY_PAYMENT");
        validMovements.add(movement);
        collectionAttempt.setValidMovements(validMovements);
        collectionAttempt.setRecurringId("456");
        List<CollectionAttempt> attempts = new ArrayList<>();
        attempts.add(new CollectionAttempt());
        attempts.add(collectionAttempt);
        attempts.add(new CollectionAttempt());
        collectionSummary.setAttempts(attempts);
        bookingDetail.setCollectionSummary(collectionSummary);

        String actualRecurringId = new RecurringBookingService().getRecurringId(bookingDetail).get();
        Assert.assertEquals(actualRecurringId, "456");
    }

    @Test
    public void testGetMembershipSubscription() {

        BookingDetail bookingDetail = new BookingDetail();
        final List<ProductCategoryBooking> bookingProducts = new ArrayList<>();
        final MembershipSubscriptionBooking membershipSubscriptionBooking = new MembershipSubscriptionBooking();
        membershipSubscriptionBooking.setProductType("MEMBERSHIP_SUBSCRIPTION");
        membershipSubscriptionBooking.setMemberId(123L);
        bookingProducts.add(membershipSubscriptionBooking);
        bookingDetail.setBookingProducts(bookingProducts);
        Assert.assertTrue(new RecurringBookingService().getMembershipId(bookingDetail).get() == 123L);
    }

    @Test
    public void testGetMembershipRenewal() {

        BookingDetail bookingDetail = new BookingDetail();
        final List<ProductCategoryBooking> bookingProducts = new ArrayList<>();
        final MembershipSubscriptionBooking membershipSubscriptionBooking = new MembershipSubscriptionBooking();
        membershipSubscriptionBooking.setProductType("MEMBERSHIP_RENEWAL");
        membershipSubscriptionBooking.setMemberId(123L);
        bookingProducts.add(membershipSubscriptionBooking);
        bookingDetail.setBookingProducts(bookingProducts);
        Assert.assertTrue(new RecurringBookingService().getMembershipId(bookingDetail).get() == 123L);
    }

    @Test
    public void testGetMembershipIdNoProductReturnsNull() {
        BookingDetail bookingDetail = new BookingDetail();
        Assert.assertFalse(new RecurringBookingService().getMembershipId(bookingDetail).isPresent());
    }

    @Test
    public void testGetMembershipIdNoMembershipProductReturnsNull() {

        BookingDetail bookingDetail = new BookingDetail();
        final List<ProductCategoryBooking> bookingProducts = new ArrayList<>();
        final TransferBooking transferBooking = new TransferBooking();
        transferBooking.setProductType("FANCY_PRODUCT");
        bookingProducts.add(transferBooking);
        bookingDetail.setBookingProducts(bookingProducts);
        Assert.assertFalse(new RecurringBookingService().getMembershipId(bookingDetail).isPresent());
    }


}
