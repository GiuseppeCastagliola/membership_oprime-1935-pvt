package com.odigeo.membership.recurring;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.RetryableException;
import org.apache.commons.lang.StringUtils;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.sql.SQLException;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

public class RecurringServiceBeanTest {

    private static final String RECURRING_ID = "ABC";
    private static final long MEMBERSHIP_ID = 1L;
    private static final long BOOKING_ID = 111L;

    @Mock
    private DataSource dataSource;
    @Mock
    private MembershipRecurringStore membershipRecurringStore;

    @InjectMocks
    private RecurringServiceBean recurringServiceBean = new RecurringServiceBean();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init((binder) -> binder.bind(MembershipRecurringStore.class).toInstance(membershipRecurringStore));
    }

    @Test
    public void testProcessRecurringBooking() throws SQLException, RetryableException {
        doNothing().when(membershipRecurringStore).insertMembershipRecurring(eq(dataSource), anyLong(), anyString());
        recurringServiceBean.processRecurringBooking(RECURRING_ID, MEMBERSHIP_ID, BOOKING_ID);
        verify(membershipRecurringStore).insertMembershipRecurring(eq(dataSource), anyLong(), anyString());
    }

    @Test
    public void testProcessRecurringBookingKO() throws SQLException {
        doThrow(new SQLException(StringUtils.EMPTY)).when(membershipRecurringStore).insertMembershipRecurring(eq(dataSource), anyLong(), anyString());
        try {
            recurringServiceBean.processRecurringBooking(RECURRING_ID, MEMBERSHIP_ID, BOOKING_ID);
            fail("Expected an RetryableException to be thrown");
        } catch (RetryableException e) {
            assertTrue(MetricsUtils.existMetric(MetricsBuilder.buildFailProcessRecurring(StringUtils.EMPTY), MetricsNames.METRICS_REGISTRY_NAME));
        }
    }
}
