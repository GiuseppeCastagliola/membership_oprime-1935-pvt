package com.odigeo.commons.monitoring.metrics;

import java.util.Map;

public final class MetricsBuilder {

    private MetricsBuilder() {
    }

    public static Metric buildMetric(String constant) {
        return new Metric.Builder(constant)
                .build();
    }

    public static Metric buildActivationRetryAttemptNumberMetric(String metricName, Integer attemptNumber) {
        return new Metric.Builder(metricName)
                .tag(MetricsNames.ATTEMPT_NUMBER, attemptNumber.toString())
                .build();
    }

    public static Metric buildTrackingRetryAttemptNumberMetric(String metricName, Integer attemptNumber) {
        return new Metric.Builder(metricName)
                .tag(MetricsNames.TRACKING_RETRY_NUMBER, attemptNumber.toString())
                .build();
    }

    public static Metric composeResponseTimeMetric(String operation) {
        return new Metric.Builder(MetricsNames.METRICS_TIME_TABLE_NAME)
                .tag(MetricsNames.OPERATION, operation)
                .build();
    }

    public static Metric buildRecurringByWebsite(String website) {
        return new Metric.Builder(MetricsNames.BOOKING_MEMBERSHIP_RECURRING_BY_WEBSITE)
                .tag(MetricsNames.WEBSITE, website)
                .build();
    }

    public static Metric buildFailProcessRecurring(String errorMessage) {
        return new Metric.Builder(MetricsNames.FAIL_RECURRING)
                .tag(MetricsNames.ERROR_RECURRING, errorMessage)
                .build();
    }

    public static Metric buildAutorenewalBySource(Map<String, String> fields) {
        return new Metric.Builder(MetricsNames.MEMBERSHIP_AUTORENEWAL)
                .tag(MetricsNames.AUTORENEWAL_CLIENT_MODULE, fields.get(MetricsNames.AUTORENEWAL_CLIENT_MODULE))
                .tag(MetricsNames.AUTORENEWAL_OPERATION, fields.get(MetricsNames.AUTORENEWAL_OPERATION))
                .tag(MetricsNames.AUTORENEWAL_METHOD, fields.get(MetricsNames.AUTORENEWAL_METHOD))
                .tag(MetricsNames.AUTORENEWAL_ID, fields.get(MetricsNames.AUTORENEWAL_ID))
                .build();
    }
}
