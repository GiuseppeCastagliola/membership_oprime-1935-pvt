package com.odigeo.membership.propertiesconfig;

public interface PropertiesConfigurationService {

    boolean isSendingIdsToKafkaActive();

    boolean updatePropertyValue(String key, boolean value);
}
