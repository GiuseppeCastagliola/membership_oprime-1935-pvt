package com.odigeo.membership.recurring.selection.criteria;

import com.odigeo.bookingapi.v12.responses.BookingDetail;

public interface RecurringCriteria {
    boolean evaluate(BookingDetail bookingDetail);
}
