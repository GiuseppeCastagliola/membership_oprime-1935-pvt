package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.parameters.MembershipCreation;

import javax.sql.DataSource;
import java.sql.SQLException;

public abstract class MembershipCreationFactory {

    public abstract Long createMembership(DataSource dataSource, MembershipCreation membershipCreation)
            throws MissingElementException, DataAccessException;

    void storeMemberStatusAction(DataSource dataSource, Long memberId, StatusAction action) throws DataAccessException {
        try {
            getMemberStatusActionStore().createMemberStatusAction(dataSource, memberId, action);
        } catch (SQLException e) {
            throw new DataAccessRollbackException("Error trying to create memberStatusAction " + action.toString() + " for membershipId " + memberId, e);
        }
    }

    void incrementCreationMetricsCounter(String metricName) {
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(metricName), MetricsNames.METRICS_REGISTRY_NAME);
    }

    private MemberStatusActionStore getMemberStatusActionStore() {
        return ConfigurationEngine.getInstance(MemberStatusActionStore.class);
    }
}
