package com.odigeo.membership.recurring.selection.criteria;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.membership.recurring.RecurringUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class BookingStatusCriteria implements RecurringCriteria {

    private static final Logger LOGGER = Logger.getLogger(BookingStatusCriteria.class);

    private final List<String> statusWithValidRecurring = Arrays.asList("CONTRACT", "REQUEST");

    @Override
    public boolean evaluate(BookingDetail bookingDetail) {
        boolean contains = statusWithValidRecurring.contains(bookingDetail.getBookingStatus());
        RecurringUtils.processingRecurringLog(LOGGER, bookingDetail.getBookingBasicInfo().getId(), getClass().getName(), Boolean.toString(contains));
        return contains;
    }
}
