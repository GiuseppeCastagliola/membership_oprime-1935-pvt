package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.enums.SourceType;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.parameters.MembershipCreation;

import javax.sql.DataSource;

public class NewMembershipSubscriptionCreation extends MembershipCreationFactory {

    @Override
    public Long createMembership(DataSource dataSource,
                                 MembershipCreation membershipCreation) throws DataAccessException {

        membershipCreation.setSourceType(SourceType.FUNNEL_BOOKING);
        long membershipId = getMemberManager().createMember(dataSource, membershipCreation);
        storeMemberStatusAction(dataSource, membershipId, StatusAction.CREATION);
        incrementCreationMetricsCounter(MetricsNames.BASIC_CREATION);
        return membershipId;
    }

    private MemberManager getMemberManager() {
        return ConfigurationEngine.getInstance(MemberManager.class);
    }
}
