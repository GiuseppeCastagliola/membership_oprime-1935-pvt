package com.odigeo.membership.recurring.selection;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import com.odigeo.bookingapi.v12.responses.ProductCategoryBooking;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class RecurringBookingService {

    private static final String MEMBERSHIP_SUBSCRIPTION = "MEMBERSHIP_SUBSCRIPTION";
    private static final String MEMBERSHIP_RENEWAL = "MEMBERSHIP_RENEWAL";

    public Optional<String> getRecurringId(BookingDetail bookingDetail) {
        return ConfigurationEngine.getInstance(RecurringIdUtils.class).getRecurringId(bookingDetail);
    }

    public Optional<Long> getMembershipId(BookingDetail bookingDetail) {

        Long membershipId = null;

        final List<ProductCategoryBooking> bookingProducts = bookingDetail.getBookingProducts();
        if (Objects.nonNull(bookingProducts)) {
            final List<ProductCategoryBooking> membershipSubscriptions = bookingProducts.stream().filter(pcb -> MEMBERSHIP_SUBSCRIPTION.equals(pcb.getProductType()) || MEMBERSHIP_RENEWAL.equals(pcb.getProductType())).collect(Collectors.toList());
            if (!membershipSubscriptions.isEmpty()) {
                final MembershipSubscriptionBooking membershipSubscriptionBooking = (MembershipSubscriptionBooking) membershipSubscriptions.get(0);
                membershipId = membershipSubscriptionBooking.getMemberId();
            }
        }
        return Optional.ofNullable(membershipId);
    }

}
