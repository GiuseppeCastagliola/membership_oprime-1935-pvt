package com.odigeo.membership.recurring.selection;

import com.google.inject.Singleton;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.CollectionAttempt;
import com.odigeo.bookingapi.v12.responses.Movement;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class RecurringIdUtils {

    private static final String PAID_MOVEMENT_STATUS = "PAID";
    private static final String REFUNDED_MOVEMENT_STATUS = "REFUNDED";
    private static final String CHARGED_BACK_MOVEMENT_STATUS = "CHARGEBACKED";
    private static final String CONFIRM_ACTION_MOVEMENT = "CONFIRM";
    private static final String DIRECTY_PAYMENT_ACTION_MOVEMENT = "DIRECTY_PAYMENT";
    private static final String MANUAL_REFUND_ACTION_MOVEMENT = "MANUAL_REFUND";
    private static final String CHARGEBACK_ACTION_MOVEMENT = "CHARGEBACK";

    public boolean hasValidMovementForRecurringId(BookingDetail bookingDetail) {
        return findCollectionAttemptWithRecurringId(bookingDetail) != null;
    }

    public Optional<String> getRecurringId(BookingDetail bookingDetail) {
        CollectionAttempt collectionAttemptWithRecurringId = findCollectionAttemptWithRecurringId(bookingDetail);
        return Optional.ofNullable(collectionAttemptWithRecurringId == null ? null : collectionAttemptWithRecurringId.getRecurringId());
    }

    private CollectionAttempt findCollectionAttemptWithRecurringId(BookingDetail bookingDetail) {

        if (bookingDetail == null || bookingDetail.getCollectionSummary() == null || bookingDetail.getCollectionSummary().getAttempts() == null) {
            return null;
        }
        for (CollectionAttempt collectionAttempt : bookingDetail.getCollectionSummary().getAttempts()) {
            if (collectionAttempt.getRecurringId() != null) {
                if (isLastMovementPaidAndConfirmedOrDirectyPayment(collectionAttempt) || isLastMovementRefunded(collectionAttempt) || isLastMovementChargedBack(collectionAttempt)) {
                    return collectionAttempt;
                }
            }
        }

        return null;

    }

    private boolean isLastMovementPaidAndConfirmedOrDirectyPayment(CollectionAttempt collectionAttempt) {
        return checkLastMovementStatusAndActions(collectionAttempt, PAID_MOVEMENT_STATUS, CONFIRM_ACTION_MOVEMENT, DIRECTY_PAYMENT_ACTION_MOVEMENT);
    }

    private boolean isLastMovementRefunded(CollectionAttempt collectionAttempt) {
        return checkLastMovementStatusAndActions(collectionAttempt, REFUNDED_MOVEMENT_STATUS, MANUAL_REFUND_ACTION_MOVEMENT, DIRECTY_PAYMENT_ACTION_MOVEMENT);
    }

    private boolean isLastMovementChargedBack(CollectionAttempt collectionAttempt) {
        return checkLastMovementStatusAndActions(collectionAttempt, CHARGED_BACK_MOVEMENT_STATUS, CHARGEBACK_ACTION_MOVEMENT);
    }

    private boolean checkLastMovementStatusAndActions(CollectionAttempt collectionAttempt, String status, String... actions) {
        boolean valuesChecked = false;
        List<Movement> validMovements = collectionAttempt.getValidMovements();
        if (!validMovements.isEmpty()) {
            Movement movement = validMovements.get(validMovements.size() - 1);
            valuesChecked = status.equals(movement.getStatus()) && Arrays.asList(actions).contains(movement.getAction());
        }
        return valuesChecked;
    }

}
