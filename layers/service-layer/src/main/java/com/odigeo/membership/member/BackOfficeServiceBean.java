package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.Membership;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.messaging.MembershipSubscriptionMessageService;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.Optional;

import static java.util.Objects.isNull;

@Stateless
@Local(BackOfficeService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BackOfficeServiceBean implements BackOfficeService {

    @EJB
    private MemberService memberService;

    private MembershipMessageSendingService membershipMessageSendingService;
    private MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;
    private MembershipSubscriptionMessageService membershipSubscriptionMessageService;

    @Override
    public boolean updateMembershipMarketingInfo(Long membershipId) throws MissingElementException, DataAccessException {
        Membership membership = getMembershipWithMemberAccount(membershipId);
        SubscriptionStatus subscriptionStatus = getMemberStatusToCrmStatusMapper().map(membership.getStatus());
        MembershipSubscriptionMessage message = getMembershipSubscriptionMessageService().getMembershipSubscriptionMessage(membership, subscriptionStatus);
        return getMembershipMessageSendingService().sendCompletedMembershipSubscriptionMessage(message);
    }

    private Membership getMembershipWithMemberAccount(Long membershipId) throws MissingElementException, DataAccessException {
        return Optional.ofNullable(memberService.getMembershipByIdWithMemberAccount(membershipId))
            .orElseThrow(() -> new MissingElementException("Membership not found by id " + membershipId));
    }

    private MembershipSubscriptionMessageService getMembershipSubscriptionMessageService() {
        if (isNull(membershipSubscriptionMessageService)) {
            membershipSubscriptionMessageService = ConfigurationEngine.getInstance(MembershipSubscriptionMessageService.class);
        }
        return membershipSubscriptionMessageService;
    }

    private MembershipMessageSendingService getMembershipMessageSendingService() {
        if (isNull(membershipMessageSendingService)) {
            membershipMessageSendingService = ConfigurationEngine.getInstance(MembershipMessageSendingService.class);
        }
        return membershipMessageSendingService;
    }

    private MemberStatusToCrmStatusMapper getMemberStatusToCrmStatusMapper() {
        if (isNull(memberStatusToCrmStatusMapper)) {
            memberStatusToCrmStatusMapper = ConfigurationEngine.getInstance(MemberStatusToCrmStatusMapper.class);
        }
        return memberStatusToCrmStatusMapper;
    }
}
