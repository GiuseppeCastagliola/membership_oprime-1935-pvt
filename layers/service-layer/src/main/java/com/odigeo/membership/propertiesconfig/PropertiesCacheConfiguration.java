package com.odigeo.membership.propertiesconfig;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.edreams.persistance.cache.Cache;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class PropertiesCacheConfiguration {

    private Cache isSendingIdsToKafkaActiveCache;

    public Cache getIsSendingIdsToKafkaActiveCache() {
        return isSendingIdsToKafkaActiveCache;
    }

    public void setIsSendingIdsToKafkaActiveCache(final Cache isSendingIdsToKafkaActiveCache) {
        this.isSendingIdsToKafkaActiveCache = isSendingIdsToKafkaActiveCache;
    }
}
