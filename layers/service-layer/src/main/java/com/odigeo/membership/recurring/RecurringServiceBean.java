package com.odigeo.membership.recurring;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.member.AbstractServiceBean;
import org.apache.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.sql.SQLException;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@Local(RecurringService.class)
public class RecurringServiceBean extends AbstractServiceBean implements RecurringService {

    private static final Logger LOGGER = Logger.getLogger(RecurringServiceBean.class);

    @Override
    public void processRecurringBooking(String recurringFromBooking, Long membershipId, long bookingId) throws RetryableException {
        try {
            getMembershipRecurringStore().insertMembershipRecurring(dataSource, membershipId, recurringFromBooking);
            RecurringUtils.processedRecurringLog(LOGGER, bookingId, " processed recurringId " + recurringFromBooking + " for MembershipId " + membershipId);
        } catch (SQLException e) {
            MetricsUtils.incrementCounter(MetricsBuilder.buildFailProcessRecurring(e.getMessage()), MetricsNames.METRICS_REGISTRY_NAME);
            throw new RetryableException(new DataAccessException("Cannot insert recurringId for membership: " + membershipId, e));
        }
    }

    @Override
    public boolean existsMembershipRecurringId(Long membershipId) {
        try {
            String recurringId = getMembershipRecurringStore().getMembershipRecurring(dataSource, membershipId);
            return recurringId != null;
        } catch (DataNotFoundException | SQLException e) {
            return false;
        }
    }

    private MembershipRecurringStore getMembershipRecurringStore() {
        return ConfigurationEngine.getInstance(MembershipRecurringStore.class);
    }
}
