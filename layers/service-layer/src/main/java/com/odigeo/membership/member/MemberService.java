package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.Membership;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.messaging.utils.MessageDataAccessException;

import java.math.BigDecimal;
import java.util.List;

public interface MemberService {

    Membership getMembershipById(long membershipId) throws MissingElementException, DataAccessException;

    Membership getMembershipByIdWithMemberAccount(long membershipId) throws MissingElementException, DataAccessException;

    List<Membership> getMembershipsByAccountId(long memberAccountId) throws MissingElementException, DataAccessException;

    Boolean activateMembership(long membershipId, BigDecimal balance) throws DataAccessException, MessageDataAccessException;

    Long createMembership(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException;

    boolean existsMembership(long memberId) throws DataAccessException;
}
