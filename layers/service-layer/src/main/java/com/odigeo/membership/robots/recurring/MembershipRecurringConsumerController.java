package com.odigeo.membership.robots.recurring;

import com.google.inject.Inject;
import com.odigeo.membership.robots.ConsumerController;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MembershipRecurringConsumerController implements ConsumerController {

    private final Consumer<BasicMessage> consumer;
    private final int numThreads;
    private final ExecutorService executorService;

    @Inject
    public MembershipRecurringConsumerController(RecurringConsumerConfiguration configuration) {
        consumer = new KafkaJsonConsumer<>(BasicMessage.class, configuration.getZooKeeperUrl(), configuration.getRecurringConsumerGroup(), configuration.getTopicName());
        numThreads = configuration.getNumConsumerThreads();
        executorService = Executors.newFixedThreadPool(numThreads);
    }

    @Override
    public void startMultiThreadConsumer() {
        List<ConsumerIterator<BasicMessage>> consumerIteratorList = consumer.connectAndIterate(numThreads);
        for (ConsumerIterator<BasicMessage> consumerIterator : consumerIteratorList) {
            executorService.execute(new MembershipRecurringConsumerWorker(consumerIterator));
        }
    }

    @Override
    public void stopMultiThreadConsumer() {
        consumer.close();
        executorService.shutdown();
    }

    @Override
    public void join() throws InterruptedException {
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
    }
}
