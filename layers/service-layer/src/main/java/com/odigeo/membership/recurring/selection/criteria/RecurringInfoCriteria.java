package com.odigeo.membership.recurring.selection.criteria;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.membership.recurring.RecurringUtils;
import com.odigeo.membership.recurring.selection.RecurringIdUtils;
import org.apache.log4j.Logger;

public class RecurringInfoCriteria implements RecurringCriteria {

    private static final Logger LOGGER = Logger.getLogger(RecurringInfoCriteria.class);

    @Override
    public boolean evaluate(BookingDetail bookingDetail) {
        boolean hasValidMovementForRecurringId = ConfigurationEngine.getInstance(RecurringIdUtils.class).hasValidMovementForRecurringId(bookingDetail);
        RecurringUtils.processingRecurringLog(LOGGER, bookingDetail.getBookingBasicInfo().getId(), getClass().getName(), Boolean.toString(hasValidMovementForRecurringId));
        return hasValidMovementForRecurringId;
    }
}
