package com.odigeo.membership.robots.activator;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.bookingapi.v12.responses.MembershipSubscriptionBooking;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.activation.validation.BookingHasContractStatusValidation;
import com.odigeo.membership.activation.validation.BookingHasMembershipSubscriptionProductValidation;
import com.odigeo.membership.activation.validation.Validation;
import com.odigeo.membership.activation.validation.ValidationHelper;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.member.MemberService;
import com.odigeo.messaging.utils.MessageDataAccessException;
import org.apache.kafka.common.config.ConfigException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.nonNull;
import static org.apache.commons.lang.BooleanUtils.isTrue;

public class MembershipActivationServiceBean implements MembershipActivationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipActivationServiceBean.class);
    private static final String MEMBERSHIP_SUBSCRIPTION = "MEMBERSHIP_SUBSCRIPTION";

    @Override
    public boolean processBookingId(Long bookingId) {
        MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.PROCESSED_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
        boolean isProcessedSuccessfully = false;
        Optional<BookingDetail> bookingDetail = retrieveBookingDetail(bookingId);
        if (bookingDetail.isPresent()) {
            MetricsUtils.startTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_ACTIVATION), MetricsNames.METRICS_REGISTRY_NAME);
            isProcessedSuccessfully = isTrue(bookingDetail.get().isTestBooking()) || processBookingCatchException(bookingDetail.get(), bookingId);
            logExecutionResult(bookingId, isProcessedSuccessfully);
            MetricsUtils.stopTimer(MetricsBuilder.composeResponseTimeMetric(MetricsNames.OPERATION_ACTIVATION), MetricsNames.METRICS_REGISTRY_NAME);
        } else {
            LOGGER.info("processBooking::bookingId {} bookingDetail is null", bookingId);
        }
        return isProcessedSuccessfully;
    }

    private void logExecutionResult(Long bookingId, boolean isProcessedSuccessfully) {
        if (isProcessedSuccessfully) {
            LOGGER.info("BookingId {} processing success", bookingId);
        } else {
            LOGGER.error("BookingId {} processing fails, will be rescheduled for retry", bookingId);
        }
    }

    private Optional<BookingDetail> retrieveBookingDetail(Long bookingId) {
        Optional<BookingDetail> bookingDetail = Optional.empty();
        try {
            LOGGER.info("MembershipActivatorRobot::Processing bookingId {}", bookingId);
            bookingDetail = Optional.ofNullable(getBookingApiManager().getBooking(bookingId));
        } catch (BookingApiException e) {
            LOGGER.error("Cannot retrieve booking for bookingId: {}, will be scheduled for retry", bookingId);
        }
        return bookingDetail;
    }

    private boolean processBookingCatchException(BookingDetail bookingDetail, Long bookingId) {
        boolean processedSuccessfully = false;
        try {
            processedSuccessfully = processBooking(bookingDetail, bookingId);
            LOGGER.info("BookingId {} processed : {} ", bookingId, processedSuccessfully);
        } catch (MessageDataAccessException | ConfigException | DataAccessException e) {
            LOGGER.error("{} processing BookingId : {}", e.getClass().getSimpleName(), bookingId);
        }
        return processedSuccessfully;
    }

    private boolean processBooking(BookingDetail bookingDetail, Long bookingId) throws DataAccessException, MessageDataAccessException {
        boolean success = true;
        Optional<MembershipSubscriptionBooking> optionalOfMembershipSubscription = getMembershipSubscriptionFromBooking(bookingDetail);
        Optional<Long> optionalOfMembershipId = optionalOfMembershipSubscription.map(MembershipSubscriptionBooking::getMemberId);
        if (optionalOfMembershipId.isPresent() && isBookingValidForActivation(bookingDetail)) {
            long membershipId = optionalOfMembershipId.get();
            LOGGER.info("activateMembership:: Proceeding to activate membershipId {}", membershipId);
            success = getMemberService().activateMembership(membershipId, optionalOfMembershipSubscription.get().getTotalPrice());
            if (success) {
                LOGGER.info("processBooking:: Membership {} activated successfully for bookingId {}", membershipId, bookingId);
            }
        }
        return success;
    }

    private boolean isBookingValidForActivation(BookingDetail bookingDetail) {
        List<Validation> validations = Arrays.asList(
                new BookingHasMembershipSubscriptionProductValidation(bookingDetail),
                new BookingHasContractStatusValidation(bookingDetail));
        return ValidationHelper.runValidations(validations);
    }

    Optional<MembershipSubscriptionBooking> getMembershipSubscriptionFromBooking(BookingDetail bookingDetail) {
        Optional<MembershipSubscriptionBooking> optionalOfMembershipSubscription = Optional.empty();
        if (nonNull(bookingDetail.getBookingProducts())) {
            optionalOfMembershipSubscription = bookingDetail.getBookingProducts().stream()
                    .filter(Objects::nonNull)
                    .filter(productCategoryBooking -> MEMBERSHIP_SUBSCRIPTION.equals(productCategoryBooking.getProductType()))
                    .findFirst()
                    .map(product -> (MembershipSubscriptionBooking) product);
        }
        return optionalOfMembershipSubscription;
    }

    private BookingApiManager getBookingApiManager() {
        return ConfigurationEngine.getInstance(BookingApiManager.class);
    }

    private MemberService getMemberService() {
        return ConfigurationEngine.getInstance(MemberService.class);
    }
}
