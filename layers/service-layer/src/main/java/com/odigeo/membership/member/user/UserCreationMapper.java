package com.odigeo.membership.member.user;

import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.userapi.BrandMapper;
import com.odigeo.userprofiles.api.v1.model.Login;
import com.odigeo.userprofiles.api.v1.model.TrafficInterface;
import com.odigeo.userprofiles.api.v1.model.User;

import java.security.InvalidParameterException;
import java.util.Optional;

final class UserCreationMapper {

    static User mapToUser(UserCreation userCreation) {
        User user = new User();
        user.setEmail(userCreation.getEmail());
        user.setLocale(userCreation.getLocale());
        user.setWebsite(userCreation.getWebsite());
        user.setBrand(BrandMapper.map(userCreation.getWebsite()).orElse(null));
        user.setTrafficInterface(mapToTrafficInterface(userCreation.getTrafficInterfaceId()));
        Login login = new Login();
        login.setPassword(null);
        user.setLogin(login);
        return user;
    }

    private static TrafficInterface mapToTrafficInterface(Integer trafficInterfaceId) {
        TrafficInterface trafficInterface = TrafficInterface.fromId(trafficInterfaceId);
        return Optional.ofNullable(trafficInterface).orElseThrow(() -> new InvalidParameterException("Invalid traffic interface id " + trafficInterfaceId));
    }
}
