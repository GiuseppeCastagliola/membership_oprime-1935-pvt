package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.v12.requests.FeeRequest;
import com.odigeo.bookingapi.v12.requests.UpdateBookingRequest;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.membership.BookingTracking;
import com.odigeo.membership.Membership;
import com.odigeo.membership.booking.BookingDetailWrapper;
import com.odigeo.membership.booking.UpdateBookingRequestBuilder;
import com.odigeo.membership.exception.RetryableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Optional;

import static com.odigeo.membership.booking.MembershipVatModelFee.AVOID_FEES;
import static com.odigeo.membership.booking.MembershipVatModelFee.COST_FEE;
import static com.odigeo.membership.booking.MembershipVatModelFee.PERKS;

public class BookingTrackingNewVatModel extends AbstractBookingTracking {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingTrackingNewVatModel.class);

    public BookingTrackingNewVatModel(Membership membership, BookingDetailWrapper bookingDetailWrapper) {
        super(membership, bookingDetailWrapper);
    }

    @Override
    public Optional<BookingTracking> getBookingTracking() throws RetryableException {
        BigDecimal balance = membership.getBalance();
        BigDecimal costFeeAmount = bookingDetailWrapper.calculateFeeAmount(COST_FEE);
        BigDecimal avoidFeeAmount = bookingDetailWrapper.calculateFeeAmount(AVOID_FEES);
        LOGGER.info("Tracking bookingId {} with new VAT model: balance {}, cost {}, avoid {}",
            bookingDetailWrapper.getBookingId(), balance, costFeeAmount, avoidFeeAmount);
        if (feesAreUnbalanced(balance, costFeeAmount, avoidFeeAmount)) {
            LOGGER.info("Booking {} fees unbalanced! Proceeding to adjust", bookingDetailWrapper.getBookingId());
            balanceFees(balance.add(avoidFeeAmount), bookingDetailWrapper.getCurrencyCode(), bookingDetailWrapper.getBookingId());
            return Optional.empty();
        }
        return Optional.of(bookingDetailWrapper.createBookingTracking(membership.getId()));
    }

    @Override
    public void deleteTrackedBookingAndRestoreBalance(DataSource dataSource) throws RetryableException {
        Long bookingId = bookingDetailWrapper.getBookingDetail().getBookingBasicInfo().getId();
        try {
            restoreMemberBalance(dataSource, bookingId);
            removeTrackedBooking(dataSource, bookingId);
        } catch (SQLException e) {
            throw new RetryableException("Cannot remove tracking for booking " + bookingId + AND_MEMBER + membership.getId(), e);
        }
    }

    private void restoreMemberBalance(DataSource dataSource, Long bookingId) throws SQLException {
        BookingTracking bookingTracked = getBookingTrackingStore().getBookingTracked(dataSource, bookingId);
        membership.setBalance(membership.getBalance().subtract(bookingTracked.getAvoidFeeAmount()));
        getMembershipStore().updateMembershipBalance(dataSource, membership.getId(), membership.getBalance());
    }

    private void removeTrackedBooking(DataSource dataSource, Long bookingId) throws SQLException {
        LOGGER.info("Tracking booking {} deleted and balance restored to {}", bookingId, membership.getBalance());
        decreaseTrackedBookingsMetric();
        getBookingTrackingStore().removeTrackedBooking(dataSource, bookingId);
    }

    @Override
    public boolean areFeesAmountChanged(BookingTracking bookingTracked) {
        BigDecimal avoidFee = bookingDetailWrapper.calculateFeeAmount(AVOID_FEES);
        BigDecimal costFee = bookingDetailWrapper.calculateFeeAmount(COST_FEE);
        BigDecimal perks = bookingDetailWrapper.calculateFeeAmount(PERKS);
        return bookingTracked.getAvoidFeeAmount().compareTo(avoidFee) != 0
            || bookingTracked.getCostFeeAmount().compareTo(costFee) != 0
            || bookingTracked.getPerksAmount().compareTo(perks) != 0;
    }

    boolean feesAreUnbalanced(BigDecimal balance, BigDecimal costFeeAmount, BigDecimal avoidFeeAmount) {
        BigDecimal balanceUpdate = balance.add(avoidFeeAmount);
        return balanceUpdate.compareTo(BigDecimal.ZERO) < 0
            || (costFeeAmount.compareTo(BigDecimal.ZERO) != 0 && balanceUpdate.compareTo(BigDecimal.ZERO) > 0);
    }

    void balanceFees(BigDecimal amount, String currency, long bookingId) throws RetryableException {
        FeeRequest avoidFeeRequest = createFeeRequest(amount.negate(), currency, AVOID_FEES.getFeeCode());
        FeeRequest costFeeRequest = createFeeRequest(amount, currency, COST_FEE.getFeeCode());
        UpdateBookingRequest updateRequest = new UpdateBookingRequestBuilder()
            .setFeeRequests(avoidFeeRequest, costFeeRequest).build();
        LOGGER.info("Going to update fees of {}", bookingId);
        try {
            BookingDetail bookingDetail = getBookingApiManager().updateBooking(bookingId, updateRequest);
            if (Objects.isNull(bookingDetail)) {
                throw new RetryableException("Error updating fees for bookingId " + bookingId + ": updateBooking response is null");
            }
        } catch (DataAccessException e) {
            throw new RetryableException("Error updating fees for bookingId " + bookingId, e);
        }
    }

    private FeeRequest createFeeRequest(BigDecimal amount, String currency, String feeCode) {
        FeeRequest avoidFeeRequest = new FeeRequest();
        avoidFeeRequest.setAmount(amount);
        avoidFeeRequest.setCurrency(currency);
        avoidFeeRequest.setSubCode(feeCode);
        return avoidFeeRequest;
    }

    private BookingApiManager getBookingApiManager() {
        return ConfigurationEngine.getInstance(BookingApiManager.class);
    }
}
