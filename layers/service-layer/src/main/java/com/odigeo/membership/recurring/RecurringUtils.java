package com.odigeo.membership.recurring;

import com.google.common.annotations.VisibleForTesting;
import org.apache.log4j.Logger;

public final class RecurringUtils {

    @VisibleForTesting
    static final String RECURRING_ROBOT = "[RecurringRobot]::";
    @VisibleForTesting
    static final String PROCESSING_BOOKING = "Processing booking::";
    @VisibleForTesting
    static final String START_MESSAGE = "Start Processing booking::";
    @VisibleForTesting
    static final String STOP_MESSAGE = "Stop Processing booking::";

    private RecurringUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static void processingRecurringLog(Logger logger, long bookingId, String writer, String result) {
        logger.info(RECURRING_ROBOT + PROCESSING_BOOKING + bookingId + " " + writer + "::" + result);
    }

    static void processedRecurringLog(Logger logger, long bookingId, String message) {
        logger.info(RECURRING_ROBOT + PROCESSING_BOOKING + bookingId + message);
    }

    static void startRecurringLog(Logger logger, long bookingId) {
        logger.info(START_MESSAGE + bookingId);
    }

    static void stopRecurringLog(Logger logger, long bookingId) {
        logger.info(STOP_MESSAGE + bookingId);
    }
}
