package com.odigeo.membership.robots.recurring;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.recurring.MembershipRecurringService;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import net.jodah.failsafe.Failsafe;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static com.odigeo.membership.robots.utils.RobotUtils.RETRY_POLICY;

public class MembershipRecurringConsumerWorker implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(MembershipRecurringConsumerWorker.class);

    private final ConsumerIterator<BasicMessage> consumerMessageIterator;
    private final ScheduledExecutorService executor;

    private static final int CORE_POOL_SIZE = 2;

    MembershipRecurringConsumerWorker(ConsumerIterator<BasicMessage> consumerMessageIterator) {
        this.consumerMessageIterator = consumerMessageIterator;
        executor = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
    }

    @Override
    public void run() {
        List<CompletableFuture<Void>> completableFutureList = new ArrayList<>();
        try {
            while (consumerMessageIterator.hasNext()) {
                BasicMessage bookingMessage = consumerMessageIterator.next();
                String bookingId = bookingMessage.getKey();

                RETRY_POLICY.onRetry(executionEvent -> LOGGER.info("Retry #" + executionEvent.getAttemptCount() + " for bookingId " + bookingId));

                completableFutureList.add(Failsafe.with(RETRY_POLICY)
                        .with(executor)
                        .onFailure(failure -> LOGGER.warn("Booking " + bookingId + " will not be checked after " + failure.getAttemptCount() + " attempts, last error: " + failure.getFailure().getMessage()))
                        .runAsync(() -> processBookingForRenewal(Long.parseLong(bookingId))));
            }
        } catch (MessageDataAccessException e) {
            LOGGER.error("Error accessing Kafka", e);
            MetricsUtils.incrementCounter(MetricsBuilder.buildFailProcessRecurring(e.getMessage()), MetricsNames.METRICS_REGISTRY_NAME);
        } catch (MessageParserException e) {
            LOGGER.error("Error parsing message from Kafka before tracking", e);
            MetricsUtils.incrementCounter(MetricsBuilder.buildFailProcessRecurring(e.getMessage()), MetricsNames.METRICS_REGISTRY_NAME);
        }
        CompletableFuture.allOf(new CompletableFuture[completableFutureList.size()]).join();
    }

    private void processBookingForRenewal(final long bookingId) throws MissingElementException, DataAccessException, RetryableException {
        getMembershipRecurringService().processBooking(bookingId);
    }

    private MembershipRecurringService getMembershipRecurringService() {
        return ConfigurationEngine.getInstance(MembershipRecurringService.class);
    }

}
