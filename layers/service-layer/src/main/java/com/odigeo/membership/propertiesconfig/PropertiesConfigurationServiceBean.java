package com.odigeo.membership.propertiesconfig;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.configuration.JeeResourceLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.sql.DataSource;
import java.sql.SQLException;

@Stateless
@Local(PropertiesConfigurationService.class)
public class PropertiesConfigurationServiceBean implements PropertiesConfigurationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesConfigurationServiceBean.class);

    @Resource(mappedName = JeeResourceLocator.DEFAULT_DATASOURCE_NAME)
    protected DataSource dataSource;

    @Override
    public boolean isSendingIdsToKafkaActive() {
        try {
            return getPropertiesConfigurationManager().isSendingIdsToKafkaActive(dataSource);
        } catch (SQLException | DataNotFoundException e) {
            LOGGER.error("The configuration isSendingIsdToKafkaActive cannot be retrieved from database.", e);
            return false;
        }
    }

    @Override
    public boolean updatePropertyValue(final String key, final boolean value) {
        try {
            boolean updatedPropertyValue = getPropertiesConfigurationManager().updatePropertyValue(dataSource, key, value);
            getPropertiesCacheService().refreshIsSendingIdsToKafkaActive();
            return updatedPropertyValue;
        } catch (DataAccessException e) {
            LOGGER.error("The configuration " + key + " cannot be updated.", e);
            return false;
        }
    }

    private PropertiesConfigurationManager getPropertiesConfigurationManager() {
        return ConfigurationEngine.getInstance(PropertiesConfigurationManager.class);
    }

    private PropertiesCacheService getPropertiesCacheService() {
        return ConfigurationEngine.getInstance(PropertiesCacheService.class);
    }
}
