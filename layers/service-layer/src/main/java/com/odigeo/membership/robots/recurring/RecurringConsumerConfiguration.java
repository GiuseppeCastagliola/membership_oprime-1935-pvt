package com.odigeo.membership.robots.recurring;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class RecurringConsumerConfiguration {

    private String zooKeeperUrl;
    private String topicName;
    private String recurringConsumerGroup;
    private int numConsumerThreads;

    public String getZooKeeperUrl() {
        return zooKeeperUrl;
    }

    public void setZooKeeperUrl(String zooKeeperUrl) {
        this.zooKeeperUrl = zooKeeperUrl;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public int getNumConsumerThreads() {
        return numConsumerThreads;
    }

    public void setNumConsumerThreads(int numConsumerThreads) {
        this.numConsumerThreads = numConsumerThreads;
    }

    public String getRecurringConsumerGroup() {
        return recurringConsumerGroup;
    }

    public void setRecurringConsumerGroup(String recurringConsumerGroup) {
        this.recurringConsumerGroup = recurringConsumerGroup;
    }
}
