package com.odigeo.membership.exception.bookingapi;

public class BookingApiException extends Exception {

    public BookingApiException(String message) {
        super(message);
    }

    public BookingApiException(String message, Throwable e) {
        super(message, e);
    }

    public BookingApiException(Throwable e) {
        super(e);
    }
}
