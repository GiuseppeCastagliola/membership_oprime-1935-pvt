package com.odigeo.membership.recurring;

import com.odigeo.membership.exception.RetryableException;

public interface RecurringService {
    void processRecurringBooking(String recurringFromBooking, Long membershipId, long bookingId) throws RetryableException;

    boolean existsMembershipRecurringId(Long membershipId);
}
