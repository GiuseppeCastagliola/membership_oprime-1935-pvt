package com.odigeo.membership.retry;

import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.robots.activator.MembershipActivationService;
import com.odigeo.membership.tracking.BookingTrackingService;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.MessageDataAccessException;
import org.apache.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Stateless
@Local(MembershipActivationRetryService.class)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class MembershipActivationRetryServiceBean implements MembershipActivationRetryService {

    private final MembershipActivationRetryMessageConfigurationManager messageManager = ConfigurationEngine.getInstance(MembershipActivationRetryMessageConfigurationManager.class);
    private final MessageIntegrationService<MembershipActivationRetryMessage> messageIntegrationService = ConfigurationEngine.getInstance(MessageIntegrationService.class);
    private final MembershipActivationService membershipActivationService = ConfigurationEngine.getInstance(MembershipActivationService.class);
    private final BookingTrackingService bookingTrackingService = ConfigurationEngine.getInstance(BookingTrackingService.class);
    private static final Logger logger = Logger.getLogger(MembershipActivationRetryServiceBean.class);

    @Override
    public Boolean scheduleForRetry(Long bookingId) {
        logger.info("Scheduling for retry activation. BookingId: " + bookingId);
        if (Objects.isNull(bookingId)) {
            logger.error("BookingId is NULL.");
            return false;
        }
        return scheduleForRetry(MembershipActivationRetryMessage.createMessage(bookingId));
    }

    @Override
    public Boolean retryActivation(MembershipActivationRetryMessage message) {
        logger.info("Retrying activation: " + message);
        if (Objects.isNull(message)) {
            logger.error("MembershipActivationRetryMessage is NULL.");
            return false;
        }

        boolean result;
        if (membershipActivationService.processBookingId(message.getBookingId())) {
            logger.info("Activation successful: " + message);
            MetricsUtils.incrementCounter(MetricsBuilder.buildActivationRetryAttemptNumberMetric(MetricsNames.SUCCESSFUL_ACTIVATION_RETRY_ATTEMPT, getRetryAttemptNumber(message.getRetryAttempts())), MetricsNames.METRICS_REGISTRY_NAME);
            CompletableFuture.runAsync(() -> trackBooking(message.getBookingId()));
            result = true;
        } else if (message.getRetryAttempts() > 1) {
            result = scheduleForRetry(MembershipActivationRetryMessage.createMessageForNextAttempt(message));
        } else {
            logger.error("Retry max attempts reached for: " + message);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.DISCARDED_ACTIVATION_RETRY), MetricsNames.METRICS_REGISTRY_NAME);
            result = false;
        }
        return result;
    }

    private Boolean scheduleForRetry(MembershipActivationRetryMessage message) {
        try {
            messageIntegrationService
                    .createKafkaPublisher(messageManager.getBrokerProducerList())
                    .publishMessages(Collections.singletonList(message));
        } catch (MessageDataAccessException e) {
            logger.error("MessageDataAccessException: " + e.getMessage(), e);
            MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.DISCARDED_ACTIVATION_RETRY), MetricsNames.METRICS_REGISTRY_NAME);
            return false;
        }
        logger.info("Message scheduled for activation retry: " + message);
        MetricsUtils.incrementCounter(MetricsBuilder.buildActivationRetryAttemptNumberMetric(MetricsNames.ACTIVATION_RETRY_ATTEMPT, getRetryAttemptNumber(message.getRetryAttempts())), MetricsNames.METRICS_REGISTRY_NAME);
        return true;
    }

    private int getRetryAttemptNumber(int remainingRetryAttempts) {
        return (messageManager.getRetryAttempts() - remainingRetryAttempts) + 1;
    }

    private void trackBooking(Long bookingId) {
        try {
            bookingTrackingService.processBooking(bookingId);
        } catch (MissingElementException | RetryableException e) {
            logger.error("Failed to track booking " + bookingId + " with error message " + e.getMessage());
        }
    }
}
