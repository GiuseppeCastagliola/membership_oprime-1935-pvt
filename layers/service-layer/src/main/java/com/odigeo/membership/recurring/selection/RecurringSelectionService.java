package com.odigeo.membership.recurring.selection;

import com.edreams.base.MissingElementException;
import com.google.inject.Inject;
import com.odigeo.bookingapi.BookingApiManager;
import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;
import com.odigeo.membership.recurring.RecurringSelectionResponse;
import com.odigeo.membership.recurring.selection.criteria.RecurringCriteria;
import org.apache.commons.lang.StringUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

public class RecurringSelectionService {

    private final BookingApiManager bookingApiManager;
    private final RecurringBookingService recurringBookingService;
    private final Set<RecurringCriteria> recurringCriteria;

    @Inject
    public RecurringSelectionService(BookingApiManager bookingApiManager, RecurringBookingService recurringBookingService, Set<RecurringCriteria> recurringCriteria) {
        this.bookingApiManager = bookingApiManager;
        this.recurringBookingService = recurringBookingService;
        this.recurringCriteria = recurringCriteria;
    }

    public Optional<RecurringSelectionResponse> processBookingForRecurring(long bookingId) throws RetryableException {
        try {
            BookingDetail bookingDetail = Optional.ofNullable(bookingApiManager.getBooking(bookingId))
                    .orElseThrow(() -> new RetryableException("Error retrieving booking detail " + bookingId));
            return obtainRecurringSelectionResponse(bookingDetail);
        } catch (BookingApiException e) {
            throw new RetryableException(new MissingElementException("Cannot get booking for bookingId: " + bookingId, e));
        }
    }

    private Optional<RecurringSelectionResponse> obtainRecurringSelectionResponse(BookingDetail bookingDetail) {
        if (recurringCriteria.stream().allMatch(criteria -> criteria.evaluate(bookingDetail))) {
            RecurringSelectionResponse recurringSelectionResponse = new RecurringSelectionResponse();
            recurringSelectionResponse.setRecurringId(recurringBookingService.getRecurringId(bookingDetail).orElseGet(() -> null));
            recurringSelectionResponse.setMembershipId(recurringBookingService.getMembershipId(bookingDetail).orElseGet(() -> Long.valueOf(0)));
            if (StringUtils.isNotEmpty(recurringSelectionResponse.getRecurringId())) {
                if (recurringSelectionResponse.getMembershipId() != 0) {
                    processCounterRecurringBookingWithMembershipByWebsite(bookingDetail);
                }
                MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.BOOKING_RECURRING), MetricsNames.METRICS_REGISTRY_NAME);
            }
            return Optional.of(recurringSelectionResponse);
        }
        return Optional.empty();
    }

    private void processCounterRecurringBookingWithMembershipByWebsite(BookingDetail bookingDetail) {
        if (Objects.nonNull(bookingDetail.getBookingBasicInfo()) && Objects.nonNull(bookingDetail.getBookingBasicInfo().getWebsite())) {
            String website = bookingDetail.getBookingBasicInfo().getWebsite().getCode();
            if (StringUtils.isNotEmpty(website)) {
                MetricsUtils.incrementCounter(MetricsBuilder.buildRecurringByWebsite(website), MetricsNames.METRICS_REGISTRY_NAME);
            }
        }
    }

}
