package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.exception.RetryableException;

public interface BookingTrackingService {

    void processBooking(long bookingId) throws MissingElementException, RetryableException;

    boolean isBookingLimitReached(Long memberId) throws DataAccessException;

}
