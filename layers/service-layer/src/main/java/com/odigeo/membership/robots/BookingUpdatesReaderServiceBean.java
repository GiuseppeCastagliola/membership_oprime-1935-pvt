package com.odigeo.membership.robots;

import com.google.inject.Inject;
import com.odigeo.membership.robots.activator.MembershipActivationConsumerController;
import com.odigeo.membership.robots.recurring.MembershipRecurringConsumerController;
import com.odigeo.membership.robots.tracker.MembershipTrackerConsumerController;
import org.I0Itec.zkclient.exception.ZkTimeoutException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BookingUpdatesReaderServiceBean implements BookingUpdatesReaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookingUpdatesReaderServiceBean.class);

    @Inject
    private MembershipActivationConsumerController membershipActivationConsumerController;

    @Inject
    private MembershipTrackerConsumerController membershipTrackerConsumerController;

    @Inject
    private MembershipRecurringConsumerController membershipRecurringConsumerController;

    @Override
    public boolean startActivationConsumerController() {
        return startConsumerController(membershipActivationConsumerController);
    }

    @Override
    public void stopActivationConsumerController() {
        membershipActivationConsumerController.stopMultiThreadConsumer();
    }

    @Override
    public boolean startTrackerConsumerController() {
        return startConsumerController(membershipTrackerConsumerController);
    }

    @Override
    public void stopTrackerConsumerController() {
        membershipTrackerConsumerController.stopMultiThreadConsumer();
    }

    @Override
    public boolean startRecurringConsumerController() {
        return startConsumerController(membershipRecurringConsumerController);
    }

    @Override
    public void stopRecurringConsumerController() {
        membershipRecurringConsumerController.stopMultiThreadConsumer();
    }

    private boolean startConsumerController(ConsumerController consumerController) {
        boolean success = false;
        boolean connected = false;
        try {
            while (!connected) {
                connected = startMultiThreadConsumer(consumerController);
            }
            consumerController.join();
            success = true;
        } catch (InterruptedException e) {
            LOGGER.error("Timeout for search results consumer threads", e);
        }
        return success;
    }

    private boolean startMultiThreadConsumer(ConsumerController consumerController) {
        try {
            consumerController.startMultiThreadConsumer();
        } catch (ZkTimeoutException e) {
            LOGGER.error("Timeout for search results consumer threads", e);
        }
        return true;
    }


}
