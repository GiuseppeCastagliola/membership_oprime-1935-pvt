package com.odigeo.membership.recurring;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.exception.RetryableException;

public interface MembershipRecurringService {
    void processBooking(long bookingId) throws MissingElementException, RetryableException, DataAccessException;
}
