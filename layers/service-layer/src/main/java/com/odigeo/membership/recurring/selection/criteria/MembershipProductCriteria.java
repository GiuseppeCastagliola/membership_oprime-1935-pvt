package com.odigeo.membership.recurring.selection.criteria;

import com.odigeo.bookingapi.v12.responses.BookingDetail;
import com.odigeo.membership.recurring.RecurringUtils;
import org.apache.log4j.Logger;

import java.util.Objects;

public class MembershipProductCriteria implements RecurringCriteria {

    private static final Logger LOGGER = Logger.getLogger(MembershipProductCriteria.class);

    private static final String MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE = "MEMBERSHIP_SUBSCRIPTION";
    private static final String MEMBERSHIP_RENEWAL_PRODUCT_TYPE = "MEMBERSHIP_RENEWAL";

    @Override
    public boolean evaluate(BookingDetail bookingDetail) {
        if (Objects.nonNull(bookingDetail.getBookingProducts())) {
            boolean bookingHasRecurringProduct = bookingDetail.getBookingProducts().stream()
                    .anyMatch(productCategoryBooking -> Objects.nonNull(productCategoryBooking)
                            && (MEMBERSHIP_SUBSCRIPTION_PRODUCT_TYPE.equals(productCategoryBooking.getProductType()) || MEMBERSHIP_RENEWAL_PRODUCT_TYPE.equals(productCategoryBooking.getProductType())));
            RecurringUtils.processingRecurringLog(LOGGER, bookingDetail.getBookingBasicInfo().getId(), getClass().getName(), Boolean.toString(bookingHasRecurringProduct));
            return bookingHasRecurringProduct;
        }
        RecurringUtils.processingRecurringLog(LOGGER, bookingDetail.getBookingBasicInfo().getId(), getClass().getName(), Boolean.toString(Boolean.FALSE));
        return Boolean.FALSE;
    }

}
