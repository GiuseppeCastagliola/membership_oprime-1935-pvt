package com.odigeo.membership.member.update;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.odigeo.membership.UpdateMembership;
import com.odigeo.membership.exception.ExistingRecurringException;
import com.odigeo.membership.exception.bookingapi.BookingApiException;

@FunctionalInterface
public interface MembershipUpdateOperation {

    boolean apply(UpdateMembership updateMembership) throws DataAccessException, MissingElementException, BookingApiException, ExistingRecurringException;
}
