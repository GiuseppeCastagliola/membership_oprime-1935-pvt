package com.odigeo.membership.recurring;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.exception.RetryableException;
import com.odigeo.membership.recurring.selection.RecurringSelectionService;
import org.apache.log4j.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.Optional;

@Stateless
@Local(MembershipRecurringService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MembershipRecurringServiceBean implements MembershipRecurringService {

    private static final Logger LOGGER = Logger.getLogger(MembershipRecurringServiceBean.class);

    @Override
    public void processBooking(long bookingId) throws RetryableException {
        try {
            RecurringUtils.startRecurringLog(LOGGER, bookingId);
            Optional<RecurringSelectionResponse> recurringSelectionResponse = getRecurringSelectionService().processBookingForRecurring(bookingId);
            if (recurringSelectionResponse.isPresent()) {
                if (!getRecurringService().existsMembershipRecurringId(recurringSelectionResponse.get().getMembershipId())) {
                    getRecurringService().processRecurringBooking(recurringSelectionResponse.get().getRecurringId(), recurringSelectionResponse.get().getMembershipId(), bookingId);
                } else {
                    RecurringUtils.processedRecurringLog(LOGGER, bookingId, " MembershipId " + recurringSelectionResponse.get().getMembershipId() + " has already a recurringId");
                }
            }
        } finally {
            RecurringUtils.stopRecurringLog(LOGGER, bookingId);
        }
    }

    private RecurringSelectionService getRecurringSelectionService() {
        return ConfigurationEngine.getInstance(RecurringSelectionService.class);
    }

    private RecurringService getRecurringService() {
        return ConfigurationEngine.getInstance(RecurringService.class);
    }
}
