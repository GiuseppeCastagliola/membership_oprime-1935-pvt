package com.odigeo.messaging.kafka;

import com.odigeo.membership.v1.messages.MembershipUpdateMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.utils.MessageDataAccessException;

public interface MembershipKafkaSender {

    void sendMembershipSubscriptionMessageToKafka(MembershipSubscriptionMessage membershipSubscriptionMessage) throws MessageDataAccessException;

    void sendMembershipUpdateMessageToKafka(MembershipUpdateMessage membershipUpdateMessage) throws MessageDataAccessException;
}
