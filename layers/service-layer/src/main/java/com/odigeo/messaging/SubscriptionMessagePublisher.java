package com.odigeo.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class SubscriptionMessagePublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionMessagePublisher.class);

    private final MembershipMessageSendingService membershipMessageSendingService;
    private final MembershipSubscriptionMessageService membershipSubscriptionMessageService;

    @Inject
    public SubscriptionMessagePublisher(MembershipMessageSendingService membershipMessageSendingService,
                                        MembershipSubscriptionMessageService membershipSubscriptionMessageService) {
        this.membershipMessageSendingService = membershipMessageSendingService;
        this.membershipSubscriptionMessageService = membershipSubscriptionMessageService;
    }

    public void sendSubscriptionMessageToCRMTopic(Membership membership, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for membershipId = {} with subscription status {}", membership.getId(), subscriptionStatus);
        try {
            final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService
                    .getMembershipSubscriptionMessage(membership, subscriptionStatus);
            membershipMessageSendingService.sendCompletedMembershipSubscriptionMessage(membershipSubscriptionMessage);
        } catch (DataNotFoundException e) {
            LOGGER.warn("Impossible to send subscription message with membershipId {}: {}", membership.getId(), e.getMessage());
        }
    }

    public void sendSubscriptionMessageToCRMTopic(UserInfo userInfo, Membership membership, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for membershipId = {} with subscription status {}", membership.getId(), subscriptionStatus);
        try {
            final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService
                    .buildMembershipSubscriptionMessage(userInfo, membership, subscriptionStatus);
            membershipMessageSendingService.sendCompletedMembershipSubscriptionMessage(membershipSubscriptionMessage);
        } catch (DataNotFoundException e) {
            LOGGER.warn("Impossible to send subscription message with membershipId {}: {}", membership.getId(), e.getMessage());
        }
    }
}
