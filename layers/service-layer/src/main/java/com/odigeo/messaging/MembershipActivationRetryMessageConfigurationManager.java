package com.odigeo.messaging;

import com.edreams.configuration.ConfiguredInPropertiesFile;
import com.google.inject.Singleton;

@Singleton
@ConfiguredInPropertiesFile
public class MembershipActivationRetryMessageConfigurationManager {

    private String brokerProducerList;
    private String topicName;
    private Long retryPeriod;
    private String retryPeriodUnit;
    private Integer retryAttempts;
    private String consumerGroup;
    private Integer numberOfIterators;
    private Integer threadsPerIterator;
    private String consumerList;

    public String getBrokerProducerList() {
        return brokerProducerList;
    }

    public void setBrokerProducerList(String brokerProducerList) {
        this.brokerProducerList = brokerProducerList;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public Long getRetryPeriod() {
        return retryPeriod;
    }

    public void setRetryPeriod(Long retryPeriod) {
        this.retryPeriod = retryPeriod;
    }

    public String getRetryPeriodUnit() {
        return retryPeriodUnit;
    }

    public void setRetryPeriodUnit(String retryPeriodUnit) {
        this.retryPeriodUnit = retryPeriodUnit;
    }

    public Integer getRetryAttempts() {
        return retryAttempts;
    }

    public void setRetryAttempts(Integer retryAttempts) {
        this.retryAttempts = retryAttempts;
    }

    public String getConsumerGroup() {
        return consumerGroup;
    }

    public void setConsumerGroup(String consumerGroup) {
        this.consumerGroup = consumerGroup;
    }

    public Integer getNumberOfIterators() {
        return numberOfIterators;
    }

    public void setNumberOfIterators(Integer numberOfIterators) {
        this.numberOfIterators = numberOfIterators;
    }

    public Integer getThreadsPerIterator() {
        return threadsPerIterator;
    }

    public void setThreadsPerIterator(Integer threadsPerIterator) {
        this.threadsPerIterator = threadsPerIterator;
    }

    public String getConsumerList() {
        return consumerList;
    }

    public void setConsumerList(String consumerList) {
        this.consumerList = consumerList;
    }
}
