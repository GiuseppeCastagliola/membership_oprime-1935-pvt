package com.odigeo.messaging.kafka;

import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.Message;
import com.odigeo.messaging.utils.Publisher;

public interface MessageIntegrationService<T extends Message> {
    Publisher<T> createKafkaPublisher(String url);
    Consumer<T> createKafkaConsumer(String url, String groupId, String topic, Class<T> typeOfMessage);
}
