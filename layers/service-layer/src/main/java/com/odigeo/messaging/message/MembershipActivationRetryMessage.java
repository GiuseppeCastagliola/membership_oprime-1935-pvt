package com.odigeo.messaging.message;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.utils.Message;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;

public final class MembershipActivationRetryMessage implements Message, Comparable<MembershipActivationRetryMessage> {

    private final String topicName;
    private final Long retryPeriodMillis;
    private final Date creationTime;
    private Integer retryAttempts;
    private Long bookingId;

    private MembershipActivationRetryMessage() {
        MembershipActivationRetryMessageConfigurationManager configManager = ConfigurationEngine.getInstance(MembershipActivationRetryMessageConfigurationManager.class);
        topicName = configManager.getTopicName();
        retryPeriodMillis = Duration.of(configManager.getRetryPeriod(), ChronoUnit.valueOf(configManager.getRetryPeriodUnit())).toMillis();
        creationTime = Calendar.getInstance().getTime();
        retryAttempts = configManager.getRetryAttempts();
    }

    private MembershipActivationRetryMessage(Long bookingId) {
        this();
        this.bookingId = bookingId;
    }

    private MembershipActivationRetryMessage(Long bookingId, Integer retryAttempts) {
        this();
        this.bookingId = bookingId;
        this.retryAttempts = retryAttempts;
    }

    public static MembershipActivationRetryMessage createMessage(Long bookingId) {
        if (Objects.isNull(bookingId)) {
            throw new IllegalArgumentException();
        }
        return new MembershipActivationRetryMessage(bookingId);
    }

    public static MembershipActivationRetryMessage createMessageForNextAttempt(MembershipActivationRetryMessage message) {
        if (Objects.isNull(message) || message.getRetryAttempts() < 0) {
            throw new IllegalArgumentException("Wrong Message: " + message);
        }
        return new MembershipActivationRetryMessage(message.getBookingId(), message.getRetryAttempts() - 1);
    }

    @Override
    public String getTopicName() {
        return topicName;
    }

    @Override
    public String getKey() {
        return bookingId.toString();
    }

    public Long getDelayForNextAttemptInMillis() {
        return Duration.between(Instant.now(), creationTime.toInstant().plus(Duration.ofMillis(retryPeriodMillis))).toMillis();
    }

    public Integer getRetryAttempts() {
        return retryAttempts;
    }

    public Long getRetryPeriodMillis() {
        return retryPeriodMillis;
    }

    public Date getCreationTime() {
        return new Date(creationTime.getTime());
    }

    public Long getBookingId() {
        return bookingId;
    }

    @Override
    public String toString() {
        return com.google.common.base.MoreObjects.toStringHelper(this)
                .add("topicName", getTopicName())
                .add("key", getKey())
                .add("bookingId", getBookingId())
                .add("retryPeriodMillis", getRetryPeriodMillis())
                .add("getDelayForNextAttemptInMillis", getDelayForNextAttemptInMillis())
                .add("retryAttempts", getRetryAttempts())
                .add("creationTime", getCreationTime())
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MembershipActivationRetryMessage)) {
            return false;
        }
        MembershipActivationRetryMessage that = (MembershipActivationRetryMessage) o;
        boolean timeVariablesAreEquals = Objects.equals(retryPeriodMillis, that.retryPeriodMillis) && Objects.equals(creationTime, that.creationTime);
        return Objects.equals(topicName, that.topicName) && timeVariablesAreEquals
                && Objects.equals(retryAttempts, that.retryAttempts)
                && Objects.equals(bookingId, that.bookingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topicName, retryPeriodMillis, creationTime, retryAttempts, bookingId);
    }

    @Override
    public int compareTo(MembershipActivationRetryMessage o) {
        return Comparator.comparing(MembershipActivationRetryMessage::getCreationTime)
                .thenComparing(MembershipActivationRetryMessage::getDelayForNextAttemptInMillis)
                .compare(this, o);
    }

}
