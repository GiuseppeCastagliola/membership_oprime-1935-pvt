package com.odigeo.membership.robots.retry;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class MembershipActivationRetryWorkerTest {

    @Mock
    private MessageIntegrationService<MembershipActivationRetryMessage> messageIntegrationService;
    @Mock
    private MembershipActivationRetryService membershipActivationRetryService;
    @Mock
    private ScheduledExecutorService executorService;

    private MembershipActivationRetryMessage message;
    private ConsumerIterator<MembershipActivationRetryMessage> consumerMessageIterator;
    private MembershipActivationRetryWorker membershipActivationRetryWorker;

    private static final Long BOOKING_ID = 123456L;
    private static final long DELAY_IN_SECONDS = 3;
    private static final long DELAY_FOR_NEXT_ATTEMPT = 2;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        ConfigurationEngine.getInstance(MembershipActivationRetryMessageConfigurationManager.class).setRetryPeriod(DELAY_IN_SECONDS);
        ConfigurationEngine.getInstance(MembershipActivationRetryMessageConfigurationManager.class).setRetryPeriodUnit(ChronoUnit.SECONDS.name());
        message = MembershipActivationRetryMessage.createMessage(BOOKING_ID);
        consumerMessageIterator = spy(new MockedConsumerIterator(Collections.singletonList(message)));
        membershipActivationRetryWorker = new MembershipActivationRetryWorker(consumerMessageIterator, executorService);
    }

    private void configure(Binder binder) {
        binder.bind(MessageIntegrationService.class).toInstance(messageIntegrationService);
        binder.bind(MembershipActivationRetryService.class).toInstance(membershipActivationRetryService);
    }

    @Test
    public void testRunHappyPath() {
        membershipActivationRetryWorker.run();
        verify(executorService, times(1)).schedule(any(Callable.class), anyLong(), any(TimeUnit.class));
    }

    @Test
    public void testRunWhenMessageDataAccessException() throws MessageDataAccessException, MessageParserException {
        doThrow(new MessageDataAccessException("MessageDataAccessException", new Throwable()))
                .when(consumerMessageIterator)
                .hasNext();
        membershipActivationRetryWorker.run();
        verify(executorService, never()).schedule(any(Callable.class), anyLong(), any(TimeUnit.class));
        verify(consumerMessageIterator, never()).next();
    }

    @Test
    public void testRunWhenMessageParserException() throws MessageDataAccessException, MessageParserException {
        doThrow(new MessageParserException("MessageParserException", new Throwable(), "MessageParserException"))
                .when(consumerMessageIterator)
                .next();
        membershipActivationRetryWorker.run();
        verify(executorService, never()).schedule(any(Callable.class), anyLong(), any(TimeUnit.class));
        verify(consumerMessageIterator, times(1)).hasNext();
    }

    @Test
    public void testRunHappyPathIntegratedWithRealExecutorService() throws InterruptedException {
        membershipActivationRetryWorker = new MembershipActivationRetryWorker(consumerMessageIterator, Executors.newSingleThreadScheduledExecutor());
        membershipActivationRetryWorker.run();
        Thread.sleep(message.getDelayForNextAttemptInMillis() > 0 ? message.getDelayForNextAttemptInMillis() * DELAY_FOR_NEXT_ATTEMPT : 0);
        verify(membershipActivationRetryService, times(1)).retryActivation(message);
    }

    class MockedConsumerIterator implements ConsumerIterator<MembershipActivationRetryMessage> {

        private Iterator<MembershipActivationRetryMessage> stream;

        MockedConsumerIterator(Iterable<MembershipActivationRetryMessage> stream) {
            this.stream = stream.iterator();
        }

        @Override
        public boolean hasNext() throws MessageDataAccessException {
            return stream.hasNext();
        }

        @Override
        public MembershipActivationRetryMessage next() throws MessageParserException, MessageDataAccessException {
            return stream.next();
        }
    }
}
