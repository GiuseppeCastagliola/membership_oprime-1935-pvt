package com.odigeo.membership.robots.retry;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.kafka.MessageIntegrationService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.Consumer;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.ScheduledExecutorService;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertNotNull;

public class MembershipActivationRetryExecutorsTest {

    private static final long RETRY_PERIOD = 2L;
    private static final int NUM_OF_ITERATORS = 2;
    private static final int THREADS_PER_ITERATORS = 2;

    @Mock
    private ScheduledExecutorService executorService;
    @Mock
    private Consumer<MembershipActivationRetryMessage> kafkaJsonConsumer;
    @Mock
    private MembershipActivationRetryMessageConfigurationManager messageConfigurationManager;
    @Mock
    private MessageIntegrationService<MembershipActivationRetryMessage> messageIntegrationService;

    private MembershipActivationRetryExecutors executors;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        doReturn(kafkaJsonConsumer).when(messageIntegrationService)
                .createKafkaConsumer(anyString(), anyString(), anyString(), any());
        doReturn(NUM_OF_ITERATORS).when(messageConfigurationManager).getNumberOfIterators();
        doReturn(THREADS_PER_ITERATORS).when(messageConfigurationManager).getThreadsPerIterator();
        doReturn(RETRY_PERIOD).when(messageConfigurationManager).getRetryPeriod();
        doReturn(ChronoUnit.SECONDS.name()).when(messageConfigurationManager).getRetryPeriodUnit();
        executors = ConfigurationEngine.getInstance(MembershipActivationRetryExecutors.class);
    }

    private void configure(Binder binder) {
        binder.bind(MessageIntegrationService.class).toInstance(messageIntegrationService);
        binder.bind(MembershipActivationRetryMessageConfigurationManager.class).toInstance(messageConfigurationManager);
    }

    @Test
    public void testStopWhenInterruptedException() throws InterruptedException {
        executors = spy(executors);
        doThrow(new InterruptedException()).when(executorService).awaitTermination(anyLong(), any());
        doReturn(executorService).when(executors).getScheduledPool();
        executors.stop();
        verify(kafkaJsonConsumer, times(1)).close();
    }

    @Test
    public void testGetScheduledPoolHappyPath() {
        assertNotNull(executors.getScheduledPool());
    }

    @Test
    public void testGetConsumerPoolHappyPath() {
        assertNotNull(executors.getConsumerPool());
    }

    @Test
    public void testGetConsumerHappyPath() {
        assertNotNull(executors.getConsumer());
    }

    @Test
    public void testStopHappyPath() {
        executors.stop();
        verify(kafkaJsonConsumer, times(1)).close();
    }
}
