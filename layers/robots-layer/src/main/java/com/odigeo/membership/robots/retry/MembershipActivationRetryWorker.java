package com.odigeo.membership.robots.retry;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.retry.MembershipActivationRetryService;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import org.apache.log4j.Logger;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class MembershipActivationRetryWorker implements Runnable {

    private final ConsumerIterator<MembershipActivationRetryMessage> consumerMessageIterator;
    private static final Logger logger = Logger.getLogger(MembershipActivationRetryWorker.class);
    private final ScheduledExecutorService executorService;
    private final MembershipActivationRetryService activationRetryService = ConfigurationEngine.getInstance(MembershipActivationRetryService.class);

    public MembershipActivationRetryWorker(ConsumerIterator<MembershipActivationRetryMessage> consumerMessageIterator, ScheduledExecutorService executorService) {
        this.consumerMessageIterator = consumerMessageIterator;
        this.executorService = executorService;
    }

    @Override
    public void run() {
        logger.info("MembershipActivationRetryWorker STARTED");
        try {
            while (consumerMessageIterator.hasNext()) {
                logger.info("MembershipActivationRetryWorker received a message");
                MembershipActivationRetryMessage bookingMessage = consumerMessageIterator.next();
                logger.info("Message to be retried for Membership activation: " + bookingMessage);
                executorService.schedule(()-> activationRetryService.retryActivation(bookingMessage), bookingMessage.getDelayForNextAttemptInMillis(), TimeUnit.MILLISECONDS);
            }
        } catch (MessageDataAccessException e) {
            logger.error("Error accessing message system", e);
        } catch (MessageParserException e) {
            logger.error("Error parsing message from message system", e);
        }
    }
}
