package com.odigeo.membership.robots.recurring;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.membership.robots.BookingUpdatesReaderService;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;
import org.apache.log4j.Logger;

import java.util.Map;

public class MembershipRecurringRobot extends AbstractRobot {

    private static final Logger LOGGER = Logger.getLogger(MembershipRecurringRobot.class);

    @Inject
    public MembershipRecurringRobot(@Assisted String robotId) {
        super(robotId);
    }


    @Override
    public RobotExecutionResult execute(Map<String, String> map) {
        final boolean success = getBookingUpdatesService().startRecurringConsumerController();
        LOGGER.info("Recurring robot finished successfully: " + success);
        return success ? RobotExecutionResult.OK : RobotExecutionResult.CRITICAL;
    }

    @Override
    public void interrupt() {
        getBookingUpdatesService().stopRecurringConsumerController();
        super.interrupt();
    }

    private BookingUpdatesReaderService getBookingUpdatesService() {
        return ConfigurationEngine.getInstance(BookingUpdatesReaderService.class);
    }

}
