package com.odigeo.membership.robots.retry;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.odigeo.messaging.MembershipActivationRetryMessageConfigurationManager;
import com.odigeo.messaging.message.MembershipActivationRetryMessage;
import com.odigeo.messaging.utils.Consumer;
import com.odigeo.robots.AbstractRobot;
import com.odigeo.robots.RobotExecutionResult;

import java.util.Map;
import java.util.concurrent.ExecutorService;

public final class MembershipActivationRetryRobot extends AbstractRobot {

    private final ExecutorService executorService;
    private final Consumer<MembershipActivationRetryMessage> kafkaJsonConsumer;
    private final MembershipActivationRetryMessageConfigurationManager configurationManager = ConfigurationEngine.getInstance(MembershipActivationRetryMessageConfigurationManager.class);
    private final MembershipActivationRetryExecutors retryExecutors = ConfigurationEngine.getInstance(MembershipActivationRetryExecutors.class);

    @Inject
    public MembershipActivationRetryRobot(@Assisted String robotId) {
        super(robotId);
        kafkaJsonConsumer = retryExecutors.getConsumer();
        this.executorService = retryExecutors.getConsumerPool();
    }

    @Override
    public RobotExecutionResult execute(Map<String, String> map) {
        kafkaJsonConsumer
                .connectAndIterate(configurationManager.getNumberOfIterators())
                .forEach(t -> executorService.execute(new MembershipActivationRetryWorker(t, retryExecutors.getScheduledPool())));
        return RobotExecutionResult.OK;
    }

    @Override
    public void interrupt() {
        retryExecutors.stop();
        super.interrupt();
    }

}
