package com.odigeo.membership.mocks.userapi;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.server.JaxRsServiceHttpServer;

@Singleton
public class UserApiHttpServer extends JaxRsServiceHttpServer {

    private static final String USER_API_PATH = "/user-profiles/service";
    private static final int USER_API_PORT = 56001;

    @Inject
    public UserApiHttpServer() {
        super(USER_API_PATH, USER_API_PORT);
    }
}
