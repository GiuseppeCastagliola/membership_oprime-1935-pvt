package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.MembershipProductFeeBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MembershipProductFeeStore {
    private static final String ADD_FEE_SQL = "INSERT INTO GE_MEMBERSHIP_FEES(ID, MEMBERSHIP_ID, AMOUNT, CURRENCY, FEE_TYPE) VALUES (?, ?, ?, ?, ?) ";

    public void saveMembershipProductFees(Connection conn, List<MembershipProductFeeBuilder> membershipProductFees) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(ADD_FEE_SQL)) {
            int idx = 1;
            for (MembershipProductFeeBuilder fee : membershipProductFees) {
                pstmt.setString(1, String.valueOf(idx++));
                pstmt.setString(2, fee.getMembershipId());
                pstmt.setBigDecimal(3, fee.getAmount());
                pstmt.setString(4, fee.getCurrency());
                pstmt.setString(5, fee.getFeeType());

                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }
}
