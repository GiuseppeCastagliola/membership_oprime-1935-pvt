package com.odigeo.membership.functionals.membership;

import com.odigeo.product.response.BookingApiMembershipInfo;

import java.math.BigDecimal;

import static org.testng.Assert.assertEquals;

public class MembershipProductBookingApiVerifier {
    private final long membershipId;
    private final BigDecimal amount;

    public MembershipProductBookingApiVerifier(long productId, BigDecimal amount) {
        this.membershipId = productId;
        this.amount = amount;
    }

    public void verifyMembershipProduct(BookingApiMembershipInfo bookingApiMembershipInfo) {
        assertEquals(this.membershipId, bookingApiMembershipInfo.getMembershipId());
        assertEquals(this.amount, bookingApiMembershipInfo.getPrice());
    }
}
