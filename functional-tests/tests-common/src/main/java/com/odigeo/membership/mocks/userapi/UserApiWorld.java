package com.odigeo.membership.mocks.userapi;

import com.google.inject.Inject;
import com.odigeo.membership.server.ServerStopException;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.Objects;

@ScenarioScoped
public class UserApiWorld {

    private final UserApiInternalHttpServer userApiInternalHttpServer;
    private final UserApiHttpServer userApiHttpServer;
    private final UserApiInternalMock userApiInternalMock;
    private final UserApiServiceMock  userApiServiceMock;

    @Inject
    public UserApiWorld(UserApiInternalHttpServer userApiInternalHttpServer, UserApiHttpServer userApiHttpServer, UserApiInternalMock userApiInternalMock, UserApiServiceMock userApiServiceMock) {
        this.userApiInternalHttpServer = userApiInternalHttpServer;
        this.userApiHttpServer = userApiHttpServer;
        this.userApiInternalMock = userApiInternalMock;
        this.userApiServiceMock = userApiServiceMock;
    }

    public void install() throws ServerStopException {
        if (userApiInternalHttpServer.serverNotCreated()) {
            userApiInternalHttpServer.startServer();
        }
        userApiInternalHttpServer.addService(userApiInternalMock);

        if (userApiHttpServer.serverNotCreated()) {
            userApiHttpServer.startServer();
        }
        userApiHttpServer.addService(userApiServiceMock);
    }

    public void uninstall() {
        if (Objects.nonNull(userApiInternalHttpServer)) {
            userApiInternalHttpServer.clearServices();
        }
        if (Objects.nonNull(userApiHttpServer)) {
            userApiHttpServer.clearServices();
        }
    }

    public UserApiInternalMock getUserApiInternalMock() {
        return userApiInternalMock;
    }
}
