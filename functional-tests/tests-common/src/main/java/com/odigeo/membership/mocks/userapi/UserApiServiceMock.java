package com.odigeo.membership.mocks.userapi;

import com.odigeo.userprofiles.api.mocks.v1.OperationSaveUserBuilder;
import com.odigeo.userprofiles.api.mocks.v1.UserBuilder;
import com.odigeo.userprofiles.api.mocks.v1.UserRegisteredResponseBuilder;
import com.odigeo.userprofiles.api.v1.UserApiService;
import com.odigeo.userprofiles.api.v1.model.AccessTokenRequest;
import com.odigeo.userprofiles.api.v1.model.ChangeEmailRequest;
import com.odigeo.userprofiles.api.v1.model.ExceptionCodeEnum;
import com.odigeo.userprofiles.api.v1.model.FlightStatusRequest;
import com.odigeo.userprofiles.api.v1.model.FlightStatusResponse;
import com.odigeo.userprofiles.api.v1.model.ForgottenPasswordRequest;
import com.odigeo.userprofiles.api.v1.model.HashCode;
import com.odigeo.userprofiles.api.v1.model.InvalidCredentialsException;
import com.odigeo.userprofiles.api.v1.model.InvalidValidationException;
import com.odigeo.userprofiles.api.v1.model.PasswordChangeRequest;
import com.odigeo.userprofiles.api.v1.model.RemoteException;
import com.odigeo.userprofiles.api.v1.model.StatusUserException;
import com.odigeo.userprofiles.api.v1.model.ThirdAppRequest;
import com.odigeo.userprofiles.api.v1.model.User;
import com.odigeo.userprofiles.api.v1.model.UserCredentials;
import com.odigeo.userprofiles.api.v1.model.UserCreditCardRequest;
import com.odigeo.userprofiles.api.v1.model.UserRegisteredResponse;
import com.odigeo.userprofiles.api.v1.model.UserSaveRequest;
import com.odigeo.userprofiles.api.v1.model.UserSearchRequest;

import java.util.Random;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class UserApiServiceMock implements UserApiService {

    private static final String MOCK_NOT_IMPLEMENTED = "Mock not implemented";
    private static final String REGISTERED_EMAIL_PREFIX = "registered";
    private final Random random = new Random();

    @Override
    public String ping() {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User getUser(UserCredentials credentials) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User logIn(UserCredentials credentials) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User saveUser(User request) throws RemoteException, InvalidValidationException, InvalidCredentialsException, StatusUserException {
        OperationSaveUserBuilder operationSaveUserBuilder = new OperationSaveUserBuilder(new UserBuilder(random));
        if (isNotBlank(request.getEmail()) && request.getEmail().startsWith(REGISTERED_EMAIL_PREFIX)) {
            operationSaveUserBuilder.setExceptionCodeEnum(new ExceptionCodeEnum(("STA_000")));
        } else {
            operationSaveUserBuilder.setExceptionCodeEnum(null);
            operationSaveUserBuilder.getUserBuilder().setEmail(request.getEmail());
        }
        return operationSaveUserBuilder.build();
    }

    @Override
    public User updateUser(UserSaveRequest request) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User setUserPassword(PasswordChangeRequest request) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User activateUser(UserCredentials credentials) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public boolean requestForgottenPassword(ForgottenPasswordRequest forgottenPasswordRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User changeUserEmail(ChangeEmailRequest changeEmailRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public boolean deleteUser(UserCredentials deleteUserRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User manageCreditCard(UserCreditCardRequest userCreditCardRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public User manageSearch(UserSearchRequest userSearchRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FlightStatusResponse putFlightStatusId(FlightStatusRequest flightStatusRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public FlightStatusResponse getFlightStatusId(FlightStatusRequest flightStatusRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public HashCode retrieveAccessToken(AccessTokenRequest accessTokenRequest) {
        throw new UnsupportedOperationException(MOCK_NOT_IMPLEMENTED);
    }

    @Override
    public UserRegisteredResponse isUserRegistered(ThirdAppRequest thirdAppRequest) {
        UserRegisteredResponseBuilder userRegisteredResponseBuilder = new UserRegisteredResponseBuilder(random);
        if (isNotBlank(thirdAppRequest.getEmail()) && thirdAppRequest.getEmail().startsWith(REGISTERED_EMAIL_PREFIX)) {
            userRegisteredResponseBuilder.setRegistered(true);
        } else {
            userRegisteredResponseBuilder.setRegistered(false);
        }
        return userRegisteredResponseBuilder.build();
    }
}
