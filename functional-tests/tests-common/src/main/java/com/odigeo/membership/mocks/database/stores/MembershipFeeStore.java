package com.odigeo.membership.mocks.database.stores;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class MembershipFeeStore {

    private static final String GET_MEMBERSHIP_FEES_BY_MEMBERSHIP_ID_SQL = "SELECT AMOUNT, CURRENCY, FEE_TYPE FROM MEMBERSHIP_OWN.GE_MEMBERSHIP_FEES WHERE MEMBERSHIP_ID = ?";

    public Optional<MembershipFee> getMembershipFeesByMembershipId(Connection conn, final long membershipId) throws SQLException {
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_MEMBERSHIP_FEES_BY_MEMBERSHIP_ID_SQL)) {
            preparedStatement.setLong(1, membershipId);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                if (rs.next()) {
                    return Optional.of(buildMembershipFee(rs));
                }
            }
        }
        return Optional.empty();
    }

    private MembershipFee buildMembershipFee(ResultSet rs) throws SQLException {
        BigDecimal amount = rs.getBigDecimal("AMOUNT");
        String currency = rs.getString("CURRENCY");
        String feeType = rs.getString("FEE_TYPE");
        return new MembershipFee(amount, currency, feeType);
    }

    public static class MembershipFee {

        private final BigDecimal amount;
        private final String currency;
        private final String feeType;

        MembershipFee(BigDecimal amount, String currency, String feeType) {
            this.amount = amount;
            this.currency = currency;
            this.feeType = feeType;
        }

        public BigDecimal getAmount() {
            return amount;
        }

        public String getCurrency() {
            return currency;
        }

        public String getFeeType() {
            return feeType;
        }
    }
}
