package com.odigeo.membership.functionals.database;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.functionals.config.DatabaseConfig;
import com.odigeo.technology.scriptsexecutor.runner.ScriptRunner;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLRecoverableException;
import java.util.Properties;

public class DatabaseInstallerWorld {

    private static final Logger LOGGER = Logger.getLogger(DatabaseInstallerWorld.class);
    private static final int GET_CONNECTION_MAX_RETRIES = 3;
    private static final String CONNECTION_HEADER = "jdbc:oracle:thin:@//";
    private static final String SCRITPS_01_CLEAR_DATA_SQL = "1.01_clear-data.sql";

    public void clearData() throws SQLException, IOException, ClassNotFoundException, InterruptedException {
        LOGGER.info("Start clear data database");
        try (Connection connectionWithUserApp = getConnection()) {
            runScript(SCRITPS_01_CLEAR_DATA_SQL, connectionWithUserApp, true);
            LOGGER.info("End clear data database");
        }
    }

    public Connection getConnection() throws InterruptedException, SQLException, ClassNotFoundException {
        DatabaseConfig databaseConfig = ConfigurationEngine.getInstance(DatabaseConfig.class);
        return getConnection(CONNECTION_HEADER + databaseConfig.getUrl(), databaseConfig.getUser(), databaseConfig.getPassword());
    }

    private Connection getConnection(String urlConnection, String userName, String password) throws ClassNotFoundException, SQLException, InterruptedException {
        Properties connectionProps = new Properties();
        connectionProps.put("user", userName);
        connectionProps.put("password", password);
        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection conn = getInnerConnectionWithRetry(urlConnection, connectionProps);
        LOGGER.info("Connected to database with user: " + userName);
        return conn;
    }

    private Connection getInnerConnectionWithRetry(String urlConnection, Properties connectionProps) throws SQLException, InterruptedException {
        Connection connection = null;
        int retries = 0;
        while (connection == null && retries < GET_CONNECTION_MAX_RETRIES) {
            try {
                connection = DriverManager.getConnection(urlConnection, connectionProps);
            } catch (SQLRecoverableException e) {
                retries++;
                Thread.sleep(1000L);
            }
        }
        return connection;
    }

    private void runScript(String scriptName, Connection conn, boolean isLineByLine) throws SQLException, IOException {
        LOGGER.info("Going to execute " + scriptName);
        try (InputStream resourceAsStream = DatabaseInstallerWorld.class.getResourceAsStream(scriptName)) {
            runScript(resourceAsStream, conn, isLineByLine);
        }
    }

    private void runScript(InputStream inputStream, Connection conn, boolean isLineByLine) throws SQLException {
        StringWriter stringWriter = new StringWriter();
        try {
            ConfigurationEngine.getInstance(ScriptRunner.class).runScript(inputStream, isLineByLine, conn, stringWriter);
        } finally {
            LOGGER.debug("ScriptRunner.runScript output: " + stringWriter.toString());
        }
        if (!conn.getAutoCommit()) {
            conn.commit();
        }
    }
}
