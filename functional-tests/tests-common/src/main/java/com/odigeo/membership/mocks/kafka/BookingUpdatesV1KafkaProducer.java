package com.odigeo.membership.mocks.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Singleton;
import com.odigeo.membership.functionals.config.KafkaConfig;
import com.odigeo.messaging.utils.BasicMessage;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.kafka.KafkaJsonPublisher;

import java.util.Collections;
import java.util.List;

@Singleton
public class BookingUpdatesV1KafkaProducer {

    private static final String BOOKING_UPDATES_V1_TOPIC_NAME = "BOOKING_UPDATES_v1";
    public static final int WAIT_TIME_FOR_ROBOT_SPOOL_UP = 5000;

    private final KafkaJsonPublisher<BasicMessage> producer;

    public BookingUpdatesV1KafkaProducer() {
        KafkaConfig kafkaConfig = ConfigurationEngine.getInstance(KafkaConfig.class);
        KafkaJsonPublisher.Builder builder = new KafkaJsonPublisher.Builder(kafkaConfig.getCompleteHost());
        this.producer = new KafkaJsonPublisher<>(builder);
    }

    public void sendMessage(long bookingId) throws MessageDataAccessException, InterruptedException {
        Thread.sleep(WAIT_TIME_FOR_ROBOT_SPOOL_UP);

        BasicMessage message = new BasicMessage();
        message.setTopicName(BOOKING_UPDATES_V1_TOPIC_NAME);
        message.setKey(String.valueOf(bookingId));
        List<BasicMessage> messageList = Collections.singletonList(message);

        producer.publishMessages(messageList);
    }

}
