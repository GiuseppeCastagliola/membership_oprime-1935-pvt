package com.odigeo.membership.functionals.membership;

import java.util.Objects;

public class MemberAccountBuilder {
    private Long memberAccountId;
    private Long userId;
    private String firstName;
    private String lastNames;
    private String timestamp;

    public MemberAccountBuilder() {
    }

    public Long getMemberAccountId() {
        return memberAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastNames() {
        return lastNames;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public MemberAccountBuilder setMemberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MemberAccountBuilder setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public MemberAccountBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public MemberAccountBuilder setLastNames(String lastNames) {
        this.lastNames = lastNames;
        return this;
    }

    public MemberAccountBuilder setTimestamp(final String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MemberAccountBuilder)) {
            return false;
        }
        MemberAccountBuilder that = (MemberAccountBuilder) o;
        return Objects.equals(memberAccountId, that.memberAccountId)
                && Objects.equals(userId, that.userId)
                && Objects.equals(firstName, that.firstName)
                && Objects.equals(lastNames, that.lastNames);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberAccountId, userId, firstName, lastNames);
    }
}
