package com.odigeo.membership.mocks.kafka;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.functionals.config.KafkaConfig;
import com.odigeo.messaging.utils.ConsumerIterator;
import com.odigeo.messaging.utils.Message;
import com.odigeo.messaging.utils.MessageDataAccessException;
import com.odigeo.messaging.utils.MessageParserException;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

abstract class AbstractKafkaConsumer<T extends Message> implements Runnable {

    private static final int NUM_THREADS = 1;
    private final KafkaJsonConsumer<T> consumer;
    private final Class<T> messageClass;
    private final List<T> messagesReceived = Collections.synchronizedList(new ArrayList<>());

    AbstractKafkaConsumer(Class<T> messageClass) {
        this.messageClass = messageClass;
        KafkaConfig kafkaConfig = ConfigurationEngine.getInstance(KafkaConfig.class);
        this.consumer = new KafkaJsonConsumer<>(messageClass,
                kafkaConfig.getCompleteZooKeeperHost(),
                getMessagesGroupId(kafkaConfig),
                getMessagesTopicName(kafkaConfig));
    }

    @Override
    public void run() {
        ConsumerIterator<T> consumerIterator = consumer.connectAndIterate(NUM_THREADS).get(0);
        logger().info("Kafka consumer connected");
        try {
            while (consumerIterator.hasNext()) {
                T message = consumerIterator.next();
                if (Objects.nonNull(message.getKey())) {
                    logger().info("{} {}", messageClass.getName(), "read!");
                    messagesReceived.add(message);
                }
            }
        } catch (MessageParserException | MessageDataAccessException e) {
            logger().error(e.getMessage(), e);
        } finally {
            consumer.close();
        }
    }

    public List<T> getMessagesReceived() {
        return messagesReceived;
    }

    public void clearMessages() {
        messagesReceived.clear();
    }

    public void close() {
        consumer.close();
    }

    protected abstract String getMessagesGroupId(final KafkaConfig kafkaConfig);

    protected abstract String getMessagesTopicName(final KafkaConfig kafkaConfig);

    protected abstract Logger logger();
}
