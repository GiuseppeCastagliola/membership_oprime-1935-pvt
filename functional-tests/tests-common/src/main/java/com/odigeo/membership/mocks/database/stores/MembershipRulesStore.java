package com.odigeo.membership.mocks.database.stores;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MembershipRulesStore {
    private static final String SAVE_RULES = "INSERT INTO MEMBERSHIP_OWN.MS_MEMBERSHIP_RULES (NUM_DAYS, NUM_BOOKINGS, ID, IS_ACTIVE) VALUES (?, ? , 1 , ?)";

    public void saveMembershipRules(Connection conn, int numBookings, int numDays, int ruleActivated) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(SAVE_RULES)) {
            pstmt.setInt(1, numDays);
            pstmt.setInt(2, numBookings);
            pstmt.setInt(3, ruleActivated);
            pstmt.execute();
        }
    }

}
