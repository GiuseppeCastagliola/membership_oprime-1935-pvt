package com.odigeo.membership.mocks.database.stores;

import com.odigeo.membership.functionals.membership.MemberStatusActionBuilder;
import com.odigeo.membership.util.DateConverter;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class MemberStatusActionStore {

    private static final String SAVE_MEMBER_STATUS_ACTIONS = "INSERT INTO MEMBERSHIP_OWN.GE_MEMBER_STATUS_ACTION(ID, MEMBER_ID, ACTION_TYPE, ACTION_DATE) VALUES (?, ?, ?, ?)";

    public void saveMemberStatusActions(final Connection conn, final List<MemberStatusActionBuilder> statusActions) throws SQLException {
        try (PreparedStatement pstmt = conn.prepareStatement(SAVE_MEMBER_STATUS_ACTIONS)) {
            for (MemberStatusActionBuilder memberStatusAction : statusActions) {
                pstmt.setString(1, memberStatusAction.getId());
                pstmt.setString(2, memberStatusAction.getMemberId());
                pstmt.setString(3, memberStatusAction.getActionType());
                Date date = DateConverter.toSqlDate(memberStatusAction.getActionDate());
                pstmt.setDate(4, date);
                pstmt.addBatch();
            }
            pstmt.executeBatch();
        }
    }

}
