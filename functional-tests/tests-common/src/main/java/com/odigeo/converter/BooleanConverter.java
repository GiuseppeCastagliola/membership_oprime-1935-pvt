package com.odigeo.converter;

import cucumber.api.Transformer;

import java.util.Objects;

public class BooleanConverter extends Transformer<Boolean> {
    @Override
    public Boolean transform(String stringBoolean) {
        if (Objects.isNull(stringBoolean)) {
            throw new FunctionalTestConverterException("Expecting true or false but is null");
        }

        String trimmedParam = stringBoolean.trim();
        if ("true".equalsIgnoreCase(trimmedParam) || "false".equalsIgnoreCase(trimmedParam)) {
            return Boolean.parseBoolean(trimmedParam);
        }
        throw new FunctionalTestConverterException("Error while reading a boolean parameter: " + stringBoolean);
    }
}
