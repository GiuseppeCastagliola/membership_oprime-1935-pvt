package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.functionals.membership.RecurringCollectionVerifier;
import com.odigeo.membership.response.Membership;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.fail;

@ScenarioScoped
public class RecurringCollectionSteps extends CommonSteps {

    private final ServerConfiguration serverConfiguration;

    @Inject
    public RecurringCollectionSteps(ServerConfiguration serverConfiguration) {
        super(serverConfiguration);
        this.serverConfiguration = serverConfiguration;
    }

    @When("^the membership recurring robot is run$")
    public void runRobot() throws IOException {
        final String membershipRecurringRobotUrl = "http://" + serverConfiguration.getMembershipServer() + "/membership/engineering/robots/RobotsAdmin/executeOnce?robotId=MembershipRecurringRobot";
        try (CloseableHttpResponse response = HttpClients.createDefault().execute(new HttpGet(membershipRecurringRobotUrl))) {
            response.getStatusLine().getStatusCode();
        }
    }

    @Then("^the following membership recurring information is stored in db:$")
    public void verifyRecurringCollectionInformation(List<RecurringCollectionVerifier> verifierList) throws InterruptedException {

        Thread.sleep(6000);

        verifierList.forEach(this::verifyRecurring);

    }

    @And("^the following membership recurring information is null:$")
    public void verifyNotStoredRecurringCollectionInformation(List<RecurringCollectionVerifier> verifierList) throws InvalidParametersException {

        RecurringCollectionVerifier recurringCollectionVerifier = verifierList.get(0);

        String userId = recurringCollectionVerifier.getUserId();
        Membership membership = membershipService.getMembership(Long.parseLong(userId), recurringCollectionVerifier.getWebsite());

        Long actualMembershipId = membership.getMembershipId();
        Long expectedMembershipId = Long.parseLong(recurringCollectionVerifier.getMembershipId());
        assertEquals(actualMembershipId, expectedMembershipId);

        assertNull(membership.getRecurringId());
    }

    private void verifyRecurring(RecurringCollectionVerifier recurringCollectionVerifier) {
        try {
            String userId = recurringCollectionVerifier.getUserId();
            Membership membership = membershipService.getMembership(Long.parseLong(userId), recurringCollectionVerifier.getWebsite());

            String actualRecurringId = membership.getRecurringId();
            String expectedRecurringId = recurringCollectionVerifier.getRecurringId();
            assertEquals(actualRecurringId, expectedRecurringId);

            Long actualMembershipId = membership.getMembershipId();
            Long expectedMembershipId = Long.parseLong(recurringCollectionVerifier.getMembershipId());
            assertEquals(actualMembershipId, expectedMembershipId);

            String actualWebsite = membership.getWebsite();
            String expectedWebsite = recurringCollectionVerifier.getWebsite();
            assertEquals(actualWebsite, expectedWebsite);
        } catch (InvalidParametersException e) {
            fail("Should not have thrown InvalidParametersException");
        }
    }

}
