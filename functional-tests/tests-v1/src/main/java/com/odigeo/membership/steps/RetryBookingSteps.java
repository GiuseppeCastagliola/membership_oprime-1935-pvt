package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.ServerConfiguration;
import com.odigeo.membership.exception.InvalidParametersException;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.java.guice.ScenarioScoped;

import static org.testng.AssertJUnit.assertTrue;

@ScenarioScoped
public class RetryBookingSteps extends CommonSteps {

    private final MembershipManagementWorld world;

    @Inject
    public RetryBookingSteps(ServerConfiguration serverConfiguration, MembershipManagementWorld world) {
        super(serverConfiguration);
        this.world = world;
    }

    @When("^requested to retry bookingId (\\d+)$")
    public void callProcessBookingId(long bookingId) throws InvalidParametersException {
        world.setMembershipActivationSucceeded(membershipService.retryMembershipActivation(bookingId));
    }

    @Then("^booking processing succeeds$")
    public void bookingProcessingSucceeds() {
        assertTrue(world.isMembershipActivationSucceeded());
    }

}
