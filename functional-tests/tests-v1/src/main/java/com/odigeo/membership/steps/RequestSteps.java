package com.odigeo.membership.steps;

import com.google.inject.Inject;
import com.odigeo.membership.functionals.membership.MembershipCreationRequestBuilder;
import com.odigeo.membership.functionals.membership.product.CreateMembershipRequestBuilder;
import com.odigeo.membership.world.MemberAccountManagementWorld;
import com.odigeo.membership.world.MembershipManagementWorld;
import cucumber.api.java.en.Given;
import cucumber.runtime.java.guice.ScenarioScoped;

import java.util.List;

@ScenarioScoped
public class RequestSteps {
    private final MembershipManagementWorld world;
    private final MemberAccountManagementWorld accountManagementWorld;

    @Inject
    public RequestSteps(MembershipManagementWorld world, MemberAccountManagementWorld accountManagementWorld) {
        this.world = world;
        this.accountManagementWorld = accountManagementWorld;
    }

    @Given("^the next createMembershipSubscriptionRequest:$")
    public void theNextCreateMembershipSubscriptionRequest(List<MembershipCreationRequestBuilder> membershipCreationRequestList) {
        world.setMembershipCreationRequest(membershipCreationRequestList);
    }

    @Given("^the next createMembershipPendingToCollectRequest:$")
    public void theNextCreateMembershipRequest(List<CreateMembershipRequestBuilder> createMembershipRequestBuilderList) {
        accountManagementWorld.setCreateMembershipRequest(createMembershipRequestBuilderList.get(0).buildPendingToCollect());
    }

    @Given("^the following createNewMembershipRequest:$")
    public void theNextCreateNewMembershipRequest(List<CreateMembershipRequestBuilder> createMembershipRequestBuilderList) {
        accountManagementWorld.setCreateMembershipRequest(createMembershipRequestBuilderList.get(0).buildCreateNewMembership());
    }

    @Given("^the following createBasicFreeMembershipRequest:$")
    public void theNextCreateBasicFreeMembershipRequest(List<CreateMembershipRequestBuilder> createMembershipRequestBuilderList) {
        accountManagementWorld.setCreateMembershipRequest(createMembershipRequestBuilderList.get(0).buildCreateBasicFreeMembership());
    }
}
