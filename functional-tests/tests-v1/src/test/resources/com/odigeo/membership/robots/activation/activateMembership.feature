Feature: Test activate membership authenticated

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 123             | 4321   | JOSE      | GARCIA    |
      | 321             | 1234   | ANA       | JUNYENT   |
      | 456             | 1111   | JUAN      | PEREZ     |
    And the next membership stored in db:
      | memberId | website | status              | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 1        | ES      | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 2        | ES      | PENDING_TO_ACTIVATE | ENABLED     | 123             | 2017-07-06     | 2018-07-06     |
      | 3        | ES      | PENDING_TO_ACTIVATE | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |
      | 4        | ES      | EXPIRED             | ENABLED     | 321             | 2017-07-06     | 2018-07-06     |

  Scenario Outline: Activate member only if status is PENDING_TO_ACTIVATE and doesn't have other activated memberships
    When requested to activate membership <membershipId> and set balance equals to 50 with authentication
    Then membership response has status <expectedStatus> and balance <expectedBalance>
    Examples:
      | membershipId | expectedStatus      | expectedBalance |
      | 2            | PENDING_TO_ACTIVATE | 0               |
      | 3            | ACTIVATED           | 50              |
      | 4            | EXPIRED             | 0               |

  Scenario: unauthorized
    When requested to activate membership without authentication unauthorized exception is thrown

