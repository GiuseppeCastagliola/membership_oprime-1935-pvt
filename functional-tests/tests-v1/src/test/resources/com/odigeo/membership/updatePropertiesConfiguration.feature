Feature: Update properties configuration test

  Scenario Outline: Test enable configuration property
    Given the configuration property <property> stored in db with the value <valueBefore>
    When enabling the configuration for the property <property>
    Then the property was updated
    And the property <property> has the value true
    Examples:
      | property           | valueBefore |
      | TEST_PROP_ENABLED  | true        |
      | TEST_PROP_DISABLED | false       |

  Scenario Outline: Test disable configuration property
    Given the configuration property <property> stored in db with the value <valueBefore>
    When disabling the configuration for the property <property>
    Then the property was updated
    And the property <property> has the value false
    Examples:
      | property          | valueBefore |
      | PROPERTY_ENABLED  | true        |
      | PROPERTY_DISABLED | false       |