Feature: Recurring collection robot
  Listen for and store recurring collection information on membership bookings

  Background:
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 55555           | 12345  | JOSE      | GARCIA    |
      | 44444           | 54321  | ALON      | GARCIA    |
      | 33333           | 85214  | JOAO      | GARCIA    |
    And the next membership stored in db:
      | memberId | website     | status    | autoRenewal | memberAccountId | activationDate | expirationDate |
      | 66666    | ES          | ACTIVATED | ENABLED     | 55555           | 2017-07-06     | 2018-07-06     |
      | 77777    | ES          | ACTIVATED | ENABLED     | 44444           | 2018-07-06     | 2019-07-06     |
      | 88888    | ES          | ACTIVATED | ENABLED     | 33333           | 2017-07-06     | 2018-07-06     |

  Scenario: Store recurring collection information when such information is available on a membership booking
    Given BookingAPI getBookingById returns the following information for that booking:
      | bookingId | membershipId | lastRecurringId | bookingStatus | website | productType             |
      | 123456789 | 66666        | abc2839f10      | CONTRACT      | ES      | MEMBERSHIP_SUBSCRIPTION |
      | 987654321 | 77777        | 10abc2839f      | CONTRACT      | ES      | MEMBERSHIP_RENEWAL      |
      | 582471693 | 88888        | 10cab2839f      | CONTRACT      | ES      | FLIGHT                  |
    When the membership recurring robot is run
    And the booking 123456789 is updated
    And the booking 987654321 is updated
    And the booking 582471693 is updated
    Then the following membership recurring information is stored in db:
      | userId | membershipId | recurringId | website |
      | 12345  | 66666        | abc2839f10  | ES      |
      | 54321  | 77777        | 10abc2839f  | ES      |
    And the following membership recurring information is null:
      | userId | membershipId | website |
      | 85214  | 88888        | ES      |