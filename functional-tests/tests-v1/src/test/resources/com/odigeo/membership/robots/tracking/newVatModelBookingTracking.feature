Feature: Finance new VAT model booking tracking

  Scenario Outline: Update balance and track booking. Prime booking in contract, not tracked, and booking fees balanced
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | CONTRACT      | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> | <creationDate> |
    And the next membership stored in db:
      | memberId   | status             | balance         | activationDate   | expirationDate | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | <activationDate> | 2019-10-10     | 1               | ENABLED     |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking exists in db
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    |
      | <bookingId> | <memberId> | <creationDate> | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> |
    And the stored membership with ID <memberId> now has <balanceAfter> as balance
    Examples:
      | bookingId | memberId | membershipStatus | activationDate | creationDate | balanceBefore | bookingAvoidFee | bookingCostFee | bookingPerksFee | balanceAfter |
      | 66330     | 55440    | ACTIVATED        | 2019-05-24     | 2019-07-01   | 60.0          | -18.5           | 0              | -10.0           | 41.5         |
      | 66331     | 55441    | ACTIVATED        | 2019-05-24     | 2019-07-01   | 10.0          | -10.0           | -8.5           | -10.0           | 0.0          |

  Scenario Outline: Update balance and track booking. Prime booking in contract and not tracked but booking fees unbalanced
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | CONTRACT      | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> | <creationDate> |
    And the next membership stored in db:
      | memberId   | status             | balance         | activationDate   | expirationDate | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | <activationDate> | 2019-10-10     | 1               | ENABLED     |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking with the booking ID <bookingId> does not exist
    And the stored membership with ID <memberId> now has <balanceAfter> as balance
    Examples:
      | bookingId | memberId | membershipStatus | activationDate | creationDate | balanceBefore | bookingAvoidFee | bookingCostFee | bookingPerksFee | balanceAfter |
      | 66330     | 55440    | ACTIVATED        | 2019-05-24     | 2019-07-01   | 30.0          | -38.5           | 0.0            | -10.0           | 30.0         |
      | 66331     | 55441    | ACTIVATED        | 2019-05-24     | 2019-07-01   | 10.0          | -15.0           | 0.0            | -10.0           | 10.0         |

  Scenario Outline: Update balance and tracked booking. Prime booking in contract, already tracked, but fees amount changed (avoid fee less than balance)
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | CONTRACT      | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> | <creationDate> |
    And the next membership activated today is stored in db:
      | memberId   | status             | balance         | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | 1               | ENABLED     |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | <trackedAvoidFee> | <trackedCostFee> | <trackedPerks> |
    When the booking with the ID <bookingId> is tracked
    Then the stored membership with ID <memberId> now has <balanceAfter> as balance
    And the tracked booking exists in db
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    |
      | <bookingId> | <memberId> | <creationDate> | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> |
    Examples:
      | bookingId | memberId | membershipStatus | bookingDate | creationDate | balanceBefore | trackedAvoidFee | trackedCostFee | trackedPerks | bookingAvoidFee | bookingCostFee | bookingPerksFee | balanceAfter |
      | 9880      | 8760     | ACTIVATED        | 2019-01-02  | 2019-06-01   | 39.99         | -5.0            | 0.0            | -20.0        | -8.0            | 0.0            | -20.0           | 36.99        |
      | 9882      | 8762     | DEACTIVATED      | 2019-03-02  | 2019-06-03   | 30.0          | -30.0           | 0.0            | -20.0        | -30.0           | 0.0            | -25.0           | 30.0         |
      | 9883      | 8763     | ACTIVATED        | 2019-04-02  | 2019-06-04   | 15.0          | -30.0           | 0.0            | -20.0        | -15.0           | 0.0            | -20.0           | 30.0         |
      | 9884      | 8764     | ACTIVATED        | 2019-01-01  | 2019-01-01   | 50.0          | -15.0           | 0.0            | 0.0          | -60.0           | 0.0            | -10.0           | 5.0          |

  Scenario Outline: Remove tracked booking and balance booking fees. Prime booking in contract, already tracked, but fees amount changed (avoid fee bigger than balance)
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus | avoidFeeAmount    | costFeeAmount    | perksFeeAmount    | creationDate   |
      | <bookingId> | true     | <memberId> | CONTRACT      | <bookingAvoidFee> | <bookingCostFee> | <bookingPerksFee> | <creationDate> |
    And the next membership activated today is stored in db:
      | memberId   | status             | balance         | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | 1               | ENABLED     |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount    | costFeeAmount    | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | <trackedAvoidFee> | <trackedCostFee> | <trackedPerks> |
    When the booking with the ID <bookingId> is tracked
    Then the stored membership with ID <memberId> now has <balanceAfter> as balance
    But the tracked booking with the booking ID <bookingId> does not exist
    Examples:
      | bookingId | memberId | membershipStatus | bookingDate | creationDate | balanceBefore | trackedAvoidFee | trackedCostFee | trackedPerks | bookingAvoidFee | bookingCostFee | bookingPerksFee | balanceAfter |
      | 9881      | 8761     | EXPIRED          | 2019-02-02  | 2019-06-02   | 0.0           | -10.0           | -10.0          | -20.0        | -32.0           | -10.0          | -20.0           | 10.0         |
      | 9882      | 8764     | ACTIVATED        | 2019-05-02  | 2019-06-05   | 0.0           | -23.0           | 0.0            | -20.0        | -33.0           | 0.0            | -24.0           | 23.0         |
      | 124       | 322      | ACTIVATED        | 2019-02-02  | 2019-02-02   | 10.0          | -5.0            | 0.0            | 0.0          |                 | -20.0          |                 | 15.0         |
      | 125       | 323      | DEACTIVATED      | 2019-03-03  | 2019-03-03   | -5.0          | -10.0           | -80.5          | 0.0          | -10.0           |                |                 | 5.0          |
      | 126       | 324      | DEACTIVATED      | 2019-04-04  | 2019-01-04   | 40.0          | -15.0           | 0.0            | 0.0          | -60.0           |                |                 | 55.0         |
      | 128       | 326      | EXPIRED          | 2019-06-06  | 2019-03-06   | -5.0          | -10.0           | 0.0            | 1.23         | -17.0           |                |                 | 5.0          |
      | 127       | 325      | EXPIRED          | 2019-05-05  | 2019-02-05   | -20.0         | -5.0            | 0.0            | 0.0          |                 |                | -20.0           | -15.0        |

  Scenario Outline: Restore update balance and delete tracked booking. Prime booking already tracked but not in contract
    Given the next booking detail retrieved by the API:
      | bookingId   | hasPerks | memberId   | bookingStatus   |
      | <bookingId> | true     | <memberId> | <bookingStatus> |
    And the next membership activated today is stored in db:
      | memberId   | status             | balance         | memberAccountId | autoRenewal |
      | <memberId> | <membershipStatus> | <balanceBefore> | 1               | ENABLED     |
    And the next tracked booking stored in db:
      | bookingId   | memberId   | bookingDateIso | avoidFeeAmount   | costFeeAmount | perksFeeAmount |
      | <bookingId> | <memberId> | <bookingDate>  | <avoidFeeAmount> | -8.2          | -9.3           |
    When the booking with the ID <bookingId> is tracked
    Then the tracked booking with the booking ID <bookingId> does not exist
    And the stored membership with ID <memberId> now has <balanceAfter> as balance
    Examples:
      | bookingId  | memberId  | membershipStatus | bookingStatus | bookingDate | balanceBefore | avoidFeeAmount | balanceAfter |
      | 91234      | 92990     | ACTIVATED        | INIT          | 2018-01-01  | 60.2          | -2.1           | 62.3         |
      | 91245      | 92991     | ACTIVATED        | REQUEST       | 2018-02-02  | -21.5         | -54.99         | 33.49        |
      | 91256      | 92992     | ACTIVATED        | HOLD          | 2018-03-03  | 40.3          | -10.3          | 50.6         |
      | 91267      | 92993     | ACTIVATED        | DIDNOTBUY     | 2018-04-04  | -10.8         | -2.1           | -8.7         |
      | 92055      | 92884     | DEACTIVATED      | PENDING       | 2018-05-15  | 60.2          | -2.1           | 62.3         |
      | 92056      | 92885     | DEACTIVATED      | RETAINED      | 2018-06-16  | 60.2          | -60.2          | 120.4        |
      | 92057      | 92886     | DEACTIVATED      | UNKNOWN       | 2018-06-17  | 40.3          | -10.3          | 50.6         |
      | 92058      | 92887     | DEACTIVATED      | FINAL_RET     | 2018-08-18  | -10.8         | -2.1           | -8.7         |
      | 93031      | 94040     | EXPIRED          | INIT          | 2018-01-21  | -5.5          | -3.0           | -2.5         |
      | 93032      | 94041     | EXPIRED          | REQUEST       | 2018-02-22  | 33.2          | 0.0            | 33.2         |
      | 93033      | 94042     | EXPIRED          | HOLD          | 2018-03-23  | 60.2          | -2.1           | 62.3         |
      | 93034      | 94043     | EXPIRED          | DIDNOTBUY     | 2018-04-24  | 10.2          | -60.2          | 70.4         |