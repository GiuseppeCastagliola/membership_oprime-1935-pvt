Feature: Free Trial Membership Eligibility

  Scenario Outline: Free Trial Membership eligibility check
    Given the user-api has the following user info
      | userId | email                 | brand | status | token |
      | 1000   | basicFree@edreams.com | ED    | ACTIVE | null  |
    And the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 100             | 1000   | JOSE      | GOMEZ     |
    And the next membership stored in db:
      | memberId | memberAccountId | autoRenewal   | membershipType   | sourceType   | monthsDuration   | status  | website | balance | activationDate | expirationDate |
      | 99000    | 100             | <autoRenewal> | <membershipType> | <sourceType> | <monthsDuration> | EXPIRED | ES      | 44.99   | 2018-05-04     | 2019-05-04     |

    When the following eligibleForFreeTrialRequest is made:
      | email   | basicFree@edreams.com |
      | website | ES                    |

    Then the returned eligibility for free prime should be <isEligible>

    Examples:
      | autoRenewal | membershipType | sourceType     | monthsDuration | isEligible |
      | ENABLED     | BASIC          | FUNNEL_BOOKING | 1              | true       |
      | DISABLED    | BUSINESS       | FUNNEL_BOOKING | 1              | true       |
      | DISABLED    | BASIC          | POST_BOOKING   | 1              | true       |
      | DISABLED    | BASIC          | FUNNEL_BOOKING | 12             | true       |
      | DISABLED    | BASIC          | FUNNEL_BOOKING | 1              | false      |


  Scenario: Free Trial Membership Eligibility call made from non Prime country
    Given the user-api has the following user info
      | userId | email                 | brand | status | token |
      | 1000   | basicFree@edreams.com | ED    | ACTIVE | null  |

    When the following eligibleForFreeTrialRequest is made:
      | email   | basicFree@edreams.com |
      | website | NL                    |

    Then the returned eligibility for free prime should be false


  Scenario: No user account found for Free Trial Membership Eligibility call
    Given the user-api has the following user info
      | userId | email                     | brand | status | token |
      | 777    | another-email@edreams.com | ED    | ACTIVE | null  |

    When the following eligibleForFreeTrialRequest is made:
      | email   | basicFree@edreams.com |
      | website | ES                    |

    Then the returned eligibility for free prime should be true
